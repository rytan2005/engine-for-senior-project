#include "Enemy.h"

Render* Renderer= DX9Render::GetRenderer();
Havok* EN_Havok = Havok::GetHavok();

SmallSpider::SmallSpider(D3DXVECTOR3 pos)
{
	GameComponent3DAnimated* comp = 0;
	Renderer->Load3DGameComponent( &comp, "small_spider.x" );

	SmallSpider* t = this;
	t = (SmallSpider*)comp;

	this->scale = D3DXVECTOR3( 1.0f, 1.0f, 1.0f );
	this->pos = pos;
	this->rot = rot;
	this->offset = offset;

	Entity* temp = (Entity*)comp;

	this->alive = true;
	this->dir = DIR_LEFT;
	this->health = SMALL_SPIDER_HEALTH;


	this->AIControlled = true;
	this->entityType = ENTITY_SMALL_SPIDER;

	this->SetStartPos( pos, patrolRadius, 10.0f, D3DXVECTOR3(3,0,0) );
	this->ChangeWeaponType(WEAPON_MELEE);
	this->rBody = EN_Havok->AddEnemy(this->pos);
}


SmallSpider::~SmallSpider(void)
{
	this->Shutdown();
}

Spider::Spider(D3DXVECTOR3 pos)
{
	GameComponent3DAnimated* comp = 0;
	Renderer->Load3DGameComponent( &comp, "boss_spider.x" );
	comp->scale = D3DXVECTOR3( 1.0f, 1.0f, 1.0f );
	comp->pos = pos;
	comp->rot = rot;
	comp->offset = offset;

	this->alive = true;
	this->dir = DIR_LEFT;
	this->health = BOSS_SPIDER_HEALTH;


	this->AIControlled = true;
	this->entityType = ENTITY_BOSS_SPIDER;

	this->SetStartPos( pos, patrolRadius, 10.0f, D3DXVECTOR3(3,0,0) );
	this->ChangeWeaponType(WEAPON_PROJECTILE);
	this->rBody = EN_Havok->AddEnemy(this->pos);

}


Spider::~Spider(void)
{
	this->Shutdown();
}
#include "Enemy.h"

Guard::Guard(D3DXVECTOR3 pos)
{
	GameComponent3DAnimated* comp = 0;
	Renderer->Load3DGameComponent( &comp, "soldier.x" );

	Guard* t = this;
	t->skinnedMesh = comp->skinnedMesh;
	t->useBoundingBox = comp->useBoundingBox;

	this->characterState = comp->characterState;

	this->patrolRadius = comp->patrolRadius;

	this->scale = D3DXVECTOR3( 1.0f, 1.0f, 1.0f );
	this->pos = pos;
	this->rot = D3DXVECTOR3( 0, 0, 0 );
	this->offset = D3DXVECTOR3( 0, -1, 0 );

	this->alive = true;
	this->dir = DIR_LEFT;
	this->health = GAURD_HEALTH;


	this->AIControlled = true;
	this->entityType = ENTITY_GUARD;

	this->SetStartPos( pos, patrolRadius, 10.0f, D3DXVECTOR3(3,0,0) );
	this->ChangeWeaponType(WEAPON_PROJECTILE);
	this->SetWeaponTo(ENEMYSHOTGUN);
	this->rBody = EN_Havok->AddEnemy(this->pos);

	this->setAttackRanges( 1, 2);

}


Guard::~Guard(void)
{
	this->Shutdown();
}

GuardCommander::GuardCommander(D3DXVECTOR3 pos)
{
	GameComponent3DAnimated* comp = 0;
	Renderer->Load3DGameComponent( &comp, "commander.x" );
	comp->scale = D3DXVECTOR3( 1.0f, 1.0f, 1.0f );
	comp->pos = pos;
	comp->rot = rot;
	comp->offset = offset;

	this->alive = true;
	this->dir = DIR_LEFT;
	this->health = COMMANDER_HEALTH;


	this->AIControlled = true;
	this->entityType = ENTITY_COMMANDER;

	this->SetStartPos( pos, patrolRadius, 10.0f, D3DXVECTOR3(3,0,0) );
	this->ChangeWeaponType(WEAPON_PROJECTILE);
	this->rBody = EN_Havok->AddEnemy(this->pos);

}


GuardCommander::~GuardCommander(void)
{
	this->Shutdown();
}
#include "Enemy.h"

MechWarrior::MechWarrior(D3DXVECTOR3 pos)
{
	this->pos = pos;
	GameComponent3DAnimated* comp = 0;
	Renderer->Load3DGameComponent( &comp, "mech_warrior.x" );
	this->scale = D3DXVECTOR3( 1.0f, 1.0f, 1.0f );
	this->pos = pos;
	this->rot = rot;
	this->offset = offset;

	this->alive = true;
	this->dir = DIR_LEFT;
	this->health = MECH_WARRIOR_HEALTH;


	this->AIControlled = true;
	this->entityType = ENTITY_SMALL_SPIDER;

	this->SetStartPos( pos, patrolRadius, 10.0f, D3DXVECTOR3(3,0,0) );
	this->ChangeWeaponType(WEAPON_MELEE);
	this->rBody = EN_Havok->AddEnemy(this->pos);

}

MechWarrior::~MechWarrior(void)
{
	this->Shutdown();
}

Juggernuat::Juggernuat(D3DXVECTOR3 pos)
{
	GameComponent3DAnimated* comp = 0;
	Renderer->Load3DGameComponent( &comp, "juggernuat.x" );
	this->scale = D3DXVECTOR3( 1.0f, 1.0f, 1.0f );
	this->pos = pos;
	this->rot = rot;
	this->offset = offset;

	this->alive = true;
	this->dir = DIR_LEFT;
	this->health = JUGGERNAUT_HEALTH;


	this->AIControlled = true;
	this->entityType = ENTITY_JUGGERNAUT;

	this->SetStartPos( pos, patrolRadius, 10.0f, D3DXVECTOR3(3,0,0) );
	this->ChangeWeaponType(WEAPON_PROJECTILE);
	this->rBody = EN_Havok->AddEnemy(this->pos);

}


Juggernuat::~Juggernuat(void)
{
	this->Shutdown();
}
