#pragma once

#include "WeaponStation.h"
#include <string.h>
#include "DX9Renderer.h"
#include "GameTimer.h"
#include "InputManager.h"
#include "SoundEffect.h"

#include <vector>

const unsigned short messageSize = 1024;

class WeaponStation;

using namespace std;
struct Button
{
	GameComponent2D* Sprite;
	
	Button();

	GAME_STATE DestinationState;
	bool IsClicked();
	bool isHighlighted();
	void SetPosition( int x, int y );
	D3DXVECTOR2 GetPosition();
	RECT GetBoundingBox();

	void Shutdown();
};

class UserInterface
{
private:
	static UserInterface* instance;
	UserInterface(void){}
	~UserInterface(void){}

	Render* p_Render;
	GAME_STATE GameState;
	InputManager* p_Input;
	GameTimer* p_Timer;
	WeaponStation* p_Station;

	GameComponent2D* StartMenuScreen;
	GameComponent2D* LoadingScreen;
	GameComponent2D* LoadingScreenHint;
	GameComponent2D* LoadingScreenControls;
	GameComponent2D* ControlScreen;
	GameComponent2D* CreditsScreen;
	GameComponent2D* PauseScreen;
	GameComponent2D* WinScreen;
	GameComponent2D* LoseScreen;
	GameComponent2D* SelectionIndicator;

	Button StartButton;

	Button MainMenuButton;
	Button OptionsButton;
	Button CreditsButton;
	Button ControlsButton;
	Button ExitButton;
	Button ResumeButton;
	Button ContinueButton;
	Button ContinueButton_inGame;
	Button ContinueButton_win;

	Button* SelectedButton;

	GameComponent2D* MouseCursor;

	char message[messageSize];

	float ScreenTime;

	short ScreenWidth, ScreenHeight;

	byte PlayerPaused;

	byte Selection;

	string levelFileName;
	LEVEL LevelState;
public:
	static UserInterface* UserInterface::GetInstance();
	static UserInterface* GetInstance( short screenWidth, short screenHeight );

	bool Init();
	GAME_STATE Update();
	GAME_STATE GetState();
	LEVEL GetLevelState();
	void SetState(GAME_STATE state);
	void SetLevel(LEVEL Level);

	void DisplayLoadingScreen();

	void RenderUI();
	void Shutdown();
};

