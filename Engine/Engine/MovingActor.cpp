#include "MovingActor.h"
#include "GameTimer.h"

MovingActor::MovingActor( char* filename, D3DXVECTOR3 position, Trigger* trigger,
			float TexU, float TexV,
			 D3DXVECTOR3 Scale, D3DXVECTOR3 rotation )
			 : Actor( filename, position, TexU, TexV, rotation, Scale )
{
	//Graphic = 0;
	OpenSpeed = 5.0f;

	//DX9Render::GetRenderer()->LoadUntexturedCube( &Graphic, width, height, depth );
	rBody = Havok::GetHavok()->AddBoxShape( Scale.x, Scale.y, Scale.z, position, 100, hkpMotion::MOTION_KEYFRAMED );

	StartPos = position;
	Destination = position + D3DXVECTOR3( 0.0f, 2.0f, 0.0f );
	//Graphic->pos = position;
	//Graphic->rot = rotation;
	//Graphic->scale = D3DXVECTOR3(Scale, Scale, Scale);
	this->trigger = trigger;
}

void MovingActor::Update()
{
	if( trigger->IsActive() )
	{
		if( D3DXVec3Length( &( Destination - Graphic->pos ) ) > 0.1f )
		{
			D3DXVECTOR3 vel;
			D3DXVec3Normalize( &vel, &(Destination-Graphic->pos) );

			Graphic->pos += ( (vel * OpenSpeed) * GameTimer::GetTimer()->GetDeltaTime() );
		}
	}else
	{
		if( D3DXVec3Length( &( StartPos - Graphic->pos ) ) > 0.1f )
		{
			D3DXVECTOR3 vel;
			D3DXVec3Normalize( &vel, &(StartPos - Graphic->pos) );

			Graphic->pos += ( (vel * OpenSpeed) * GameTimer::GetTimer()->GetDeltaTime() );
		}
	}

	::hkVector4 V;
	Havok::GetHavok()->D3DXVecToHkVec( Graphic->pos, V );

	rBody->setPosition( V );
}
