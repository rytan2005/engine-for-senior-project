#pragma once
#include "ParticleEffect.h"

class FreezeEffect : public ParticleEmitter
{
private:
	static FreezeEffect* instance;
	FreezeEffect(void){}
	FreezeEffect(const FreezeEffect& t){}
	void operator= (const FreezeEffect&t){}

public:
	static FreezeEffect* GetEffect();
	
	void InitParticle( Particle& out );
	
	~FreezeEffect(void){}
};

