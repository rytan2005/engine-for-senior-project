#pragma once

#include "DX9Renderer.h"
#include "GameTimer.h"

#include "Entity.h"

class AIHandler
{

public:
	bool WantToShoot;

	AIHandler(void){};
	~AIHandler(void){}	

	static AIHandler* GetAIHandler();

	bool Init();
	void Update( Entity* object, Entity* target, int EntityListID );

	bool CheckWantToShoot();

private:
	static AIHandler* instance;

	void DoGuardStuff( Entity* object, Entity* target, int EntityListID);

	void Shoot();
};

