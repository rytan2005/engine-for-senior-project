#include "AcidEffect.h"


AcidEffect* AcidEffect::instance = 0;
AcidEffect* AcidEffect::GetEffect()
{
	if( !instance )
	{
		instance = new AcidEffect();
		instance->filename = "assets/shaders/flamethrower.hlsl";
		instance->techname = "tech0";
		instance->texname = "assets/textures/particles/acid2.png";
	}
	return instance;
}

void AcidEffect::InitParticle( Particle& out )
{
	out.initialPos = origin;

	out.initialTime = mTime;
	out.lifeTime = GetRandomFloat( 0.2f, 0.4f );
	out.initialColor = D3DXCOLOR( 1.0f, 1.0f, 1.0f, 1.0f );
	out.initialSize = GetRandomFloat( 2.0f, 5.0f );
	out.mass = GetRandomFloat( 0.8f, 1.2f );

	out.initialVelocity.x = GetRandomFloat( -2.0f, 2.0f );
	out.initialVelocity.y = GetRandomFloat( -0.5f, 2.0f );
	out.initialVelocity.z = GetRandomFloat( -2.0f, 2.0f );
}

void AcidEffect::AddParticles( short direction, 
		PARTICLE_DETAIL_LEVEL detailLevel )
{
	this->direction = direction;

	int numParticlesToInit = 0;
	switch( detailLevel )
	{
	case PARTICLE_DETAIL_LOW:
		numParticlesToInit = 5;
		break;

	case PARTICLE_DETAIL_MEDIUM:
		numParticlesToInit = 15;
		break;

	case PARTICLE_DETAIL_HIGH:
		numParticlesToInit = 30;
		break;

	default:
		numParticlesToInit = 5;
		break;
	};

	for( short i = 0; i < numParticlesToInit; ++i )
	{
		AddParticle();
	}
}