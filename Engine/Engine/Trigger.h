#pragma once

#include "DX9Renderer.h"
#include "EntityManager.h"

const float collideDist = 1.0f;

class Trigger
{
private:
	bool Active;
	GameComponent3D* Graphic;
	GameComponent3D* Sphere;

public:
	Trigger( char* filename, D3DXVECTOR3 position, 
		D3DXVECTOR3 rotation = D3DXVECTOR3(0,0,0), float Scale = 1.0f );
	~Trigger();

	void Update();
	void Render( Light* light = 0 );
	bool IsActive();
};
