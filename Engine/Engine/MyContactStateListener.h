#pragma once

#include "Havok.h"
#include "EntityManager.h"

class Havok;

//custom contact listener that identifies which 2 bodies collides and takes custom actions on those objects (i.e. delete the bullet, hurt enemy etc.)
class MyContactStateListener : public hkReferencedObject, public hkpContactListener
{
private:
	Havok* p_Havok;
	EntityManager* p_EntityManager;
	unsigned int eventBody0ID;
	unsigned int eventBody1ID;
public:
	MyContactStateListener();

	// hkpContactListener interface.
	void contactPointCallback( const hkpContactPointEvent& event );
	void phantomEnterEvent( const hkpCollidable* collidableA, const hkpCollidable* collidableB, const hkpCollisionInput& env );
	/// Update the body (to be called once every frame).
	void update();
	unsigned int GetEventBody0ID();
	unsigned int GetEventBody1ID();
};

