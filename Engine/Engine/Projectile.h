#pragma once

#include "Utilities.h"
#include "DX9Renderer.h"
#include "GameTimer.h"
#include "Havok.h"
#include "Weapon.h"

class Projectile
{
public:
	Projectile(bool isPlayer, D3DXVECTOR3 bulletStart, Directions TheDir, ElementType element, short GunDamage, float GunRange);
	~Projectile(void);

	void Update();
	void Shutdown();

	short GetDamage(void);
	hkpRigidBody* GetRigidBody(void);
	GameComponent3D* GetGC3D(void);
	float GetDistTraveled(void);
	float GetRange(void);
	ElementType GetElement(void);

private:
	Directions Dir;
	ElementType element;
	//Graphical body and havok body
	GameComponent3D* Graphic;
	hkpRigidBody* rBody;
	
protected:
	short Damage;
	float Range;		//the range of the weapon this bullet was shot from
	float DistTraveled;	//the current distance this bullet has traveled
};

