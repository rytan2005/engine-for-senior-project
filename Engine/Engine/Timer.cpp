#include "Timer.h"


CTimer* CTimer::timerObj = 0;
CTimer* CTimer::GetTimer()
{
	if( !timerObj )
		timerObj = new CTimer();

	return timerObj;
}

CTimer::CTimer()
{
	timer = 0;
	prevTime= 0;
	timeDiff = 0;

	currTimeStamp = 0;
	prevTimeStamp = 0;
	deltaTime = 0;
	secsPerCnt = 0;

	fpsTimer = 0;
	fps = 0;
	frameCounter = 0;
}

void CTimer::StartTimer()
{
	_int64 cntsPerSec = 0;
	QueryPerformanceFrequency( (LARGE_INTEGER*)&cntsPerSec );
	secsPerCnt = 1.0f / (float)cntsPerSec;
	QueryPerformanceCounter( (LARGE_INTEGER*)&prevTimeStamp );
}

void CTimer::Update()
{
	QueryPerformanceCounter( (LARGE_INTEGER*)&currTimeStamp );
	deltaTime = ( currTimeStamp - prevTimeStamp ) * secsPerCnt;
	prevTimeStamp = currTimeStamp;

	prevTime = timer;
	timer = (short)timeGetTime();
	timeDiff = timer - prevTime;

	++frameCounter;
	fpsTimer += deltaTime;
	if( fpsTimer > 1.0f )
	{
		fps = frameCounter;
		frameCounter = 0;
		fpsTimer = 0.0f;
	}
}

float CTimer::GetDeltaTime()
{
	return deltaTime;
}

short CTimer::GetElapsedMilliseconds()
{
	return timeDiff;
}

short CTimer::GetFPS()
{
	return fps;
}

void CTimer::Shutdown()
{
	if( timerObj )
		delete timerObj;
	timerObj = 0;
}