#include "MyContactStateListener.h"
#include "BloodEffect.h"


//constructor for contact listener
MyContactStateListener::MyContactStateListener()
{
	p_Havok = Havok::GetHavok();
	p_EntityManager = EntityManager::GetEntityManager();
	eventBody0ID = -1;
	eventBody1ID = -1;
}

//custom response to collision
void MyContactStateListener::contactPointCallback( const hkpContactPointEvent& event )
{
	//determine what kind of objects are in this contact event
	//event.m_bodies[0] is the object that has the contactlistener attached that triggered this callback
	//event.m_bodies[1] is the object that the moving object collided with (possibly another moving object)
	int body0Info = event.m_bodies[0]->getCollisionFilterInfo();
	int body1Info = event.m_bodies[1]->getCollisionFilterInfo();

	if(CustomCollisionFilter::LAYER_PLAYER_BULLET == body0Info && CustomCollisionFilter::LAYER_ENEMY != body1Info)//player's bullet hits non-enemy
	{
		Projectile* bullet;
		bullet = p_EntityManager->GetBulletUsingID(event.m_bodies[0]->getUid());

		//upon contact, remove the bullet as soon as it is safe to do so
		p_Havok->GetPhysicsWorld()->removeEntity(bullet->GetRigidBody());
	}
	else if(CustomCollisionFilter::LAYER_PLAYER_BULLET == body0Info && CustomCollisionFilter::LAYER_ENEMY == body1Info)//player's bullet hits enemy
	{
		Projectile* bullet;
		bullet = p_EntityManager->GetBulletUsingID(event.m_bodies[0]->getUid());

		Entity* enemy;
		//get the correct enemy
		enemy = p_EntityManager->GetEnemyUsingID(event.m_bodies[1]->getUid());

		D3DXVECTOR3 bpos = bullet->GetGC3D()->pos;
		bpos.x = enemy->pos.x;
		BloodEffect::GetEffect()->SetOrigin( bpos );
		if( bullet->GetGC3D()->pos.x > enemy->pos.x )
		{
			BloodEffect::GetEffect()->AddParticles( 1, PARTICLE_DETAIL_LOW );
		}else
		{
			BloodEffect::GetEffect()->AddParticles( -1, PARTICLE_DETAIL_LOW );
		}

		//check if the enemy is in a special Status_Effect
		if(enemy->GetStatus() == POISONED && bullet->GetElement() == POISON)
		{
			//POISONED bullet does extra damage to poisoned enemies
			enemy->TakeDamage(bullet->GetDamage() * 1.5);
		}
		else //take normal damage
		{
			//now hurt the enemy with damage equal to this bullet's damage
			enemy->TakeDamage(bullet->GetDamage());
		}

		//set the enemies Status_Effect if this is an elemental bullet
		if(bullet->GetElement() == FREEZE)
		{
			//freeze the enemy for 3.0 seconds.
			enemy->SetStatus(FROZEN, 3.0f);
			p_Havok->LockMovement(false, enemy->GetRigidBody()->getUid(),3.0f);
			
		}
		else if(bullet->GetElement() == POISON && enemy->GetStatus() != FROZEN)//do not let the enemy end FROZEN state prematurely.
		{
			//poison the enemy for 5.0 seconds
			enemy->SetStatus(POISONED, 5.0f);
		}

		//now remove the bullet
		bullet->GetRigidBody()->getWorld()->removeEntity(bullet->GetRigidBody());
	}

	if(CustomCollisionFilter::LAYER_PLAYER == body0Info && CustomCollisionFilter::LAYER_ENEMY == body1Info)//player touches enemy
	{
		Entity* player;
		//upon touching an enemy, hurt the player
		//first get the correct player
		player = p_EntityManager->GetPlayerUsingID(event.m_bodies[0]->getUid());

		//now hurt the player
		//player->TakeDamage(TOUCHENEMYDMG);TURN THIS TOUCH DAMAGE BACK ON
		//SoundEffect* playerhurt = SoundLoader::GetInstance()->Load(false,false,"assets\\soundfx\\Player Hurt.mp3");
		//AudioManager::GetInstance()->PlaySFX(*playerhurt);
	}

	if(CustomCollisionFilter::LAYER_ENEMY_BULLET == body0Info && CustomCollisionFilter::LAYER_PLAYER == body1Info)//Enemy's bullet hits player
	{
		//get the correct bullet
		Projectile* bullet;
		bullet = p_EntityManager->GetBulletUsingID(event.m_bodies[0]->getUid());
				
		Entity* player;
		//get the correct player
		player = p_EntityManager->GetPlayerUsingID(event.m_bodies[1]->getUid());

		D3DXVECTOR3 bpos = bullet->GetGC3D()->pos;
		bpos.x = player->pos.x;
		BloodEffect::GetEffect()->SetOrigin( bpos );
		if( bullet->GetGC3D()->pos.x > player->pos.x )
			BloodEffect::GetEffect()->AddParticles( 1, PARTICLE_DETAIL_LOW );
		else
			BloodEffect::GetEffect()->AddParticles( -1, PARTICLE_DETAIL_LOW );

		//now hurt the player with damage equal to this bullet's damage
		player->TakeDamage(bullet->GetDamage());
		if(bullet->GetElement() == FREEZE)
		{
			//freeze the enemy for 3.0 seconds.
			player->SetStatus(FROZEN, 3.0f);
		}
		else if(bullet->GetElement() == POISON)
		{
			//poison the enemy for 5.0 seconds
			player->SetStatus(POISONED, 5.0f);
		}

		SoundEffect* playerhurt = SoundLoader::GetInstance()->Load(false,false,"assets\\soundfx\\Player Hurt.mp3");
		AudioManager::GetInstance()->PlaySFX(*playerhurt);

		//now remove the bullet
		bullet->GetRigidBody()->getWorld()->removeEntity(bullet->GetRigidBody());
	}
	else if(CustomCollisionFilter::LAYER_ENEMY_BULLET == body0Info && CustomCollisionFilter::LAYER_PLAYER != body1Info)//enemy bullet hits nonplayer
	{
		Projectile* bullet;
		bullet = p_EntityManager->GetBulletUsingID(event.m_bodies[0]->getUid());

		//upon contact, remove the bullet as soon as it is safe to do so
		p_Havok->GetPhysicsWorld()->removeEntity(bullet->GetRigidBody());
	}

	if(CustomCollisionFilter::LAYER_PLAYER_ATTACK_PHANTOM == body0Info)
	{
		int debug = 0;
	}
}

void MyContactStateListener::update()
{
	//Report any triangles we've entered. (example update task)
}

unsigned int MyContactStateListener::GetEventBody0ID()
{
	return eventBody0ID;
}

unsigned int MyContactStateListener::GetEventBody1ID()
{
	return eventBody1ID;
}
