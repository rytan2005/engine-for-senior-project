#include "AIHandler.h"

AIHandler* AIHandler::instance = 0;

AIHandler* AIHandler::GetAIHandler()
{
	if( !instance )
		instance = new AIHandler();

	return instance;
}

bool AIHandler::Init()
{
	if( !instance )
		return false;

	//init variables
	WantToShoot = false;

	return true;
}

void AIHandler::Update( Entity* object, Entity* target, int EntityListID )
{
	if(object->GetStatus() != FROZEN)
	{
		switch( object->entityType )
		{
		case ENTITY_GUARD:
			DoGuardStuff( object, target, EntityListID );
			break;

		default:
			break;
		}
	}
}

void AIHandler::DoGuardStuff( Entity* object, Entity* target, int EntityListID)
{


	// if this object is not AI controlled, then exit the function
	if( !object->AIControlled )
		return;

	// check to see if the target's y position is within 1 unit of this object's y position
	if( abs( target->pos.y - object->pos.y) < 1.0f )
	{
		// create a temporary variable to keep track of which side the target is on
		float tempVal = 0;
		float xval = (target->pos.x - object->pos.x);

		if( abs(xval) < 0.01f )
			tempVal = 0.0f;
		else 
			tempVal = xval / abs(xval);

		// set the velocity to always  point toward's the target, this will take care
		//	of properly orienting the character
		if( tempVal > 0.0f )
		{
			object->vel.x = abs(object->vel.x);
			object->dir = DIR_RIGHT;
		}else
		{
			object->vel.x = -abs(object->vel.x);
			object->dir = DIR_LEFT;
		}

		// keep the object from going outside of it's patrol bounds.
		if( abs( object->startPos.x - object->pos.x ) > object->patrolRadius )
			object->pos = object->prevPos;

		// check this object's weapon type, and set the correct behaviors
		switch( object->weaponType )
		{
		case WEAPON_MELEE:
			{
				if( abs( target->pos.x - object->pos.x ) < object->meleeAttackRange )
					object->ChangeCharaterState( STATE_ATTACK_MELEE );
				else
					object->ChangeCharaterState( STATE_SEEK );

				WantToShoot = false;
			}
			break;

		case WEAPON_PROJECTILE:
			{
				if( abs( target->pos.x - object->pos.x ) < object->projectileAttackRange )
					object->ChangeCharaterState( STATE_ATTACK_RANGED );
				else
				{
					WantToShoot = false;
					object->ChangeCharaterState( STATE_SEEK );
				}
			}
			break;

		default:
			object->ChangeCharaterState(STATE_ATTACK_MELEE);
			break;
		}
	}else
	{
		object->ChangeCharaterState( STATE_PATROL );
	}

	// properly orient the object
	if( object->vel.x > 0.0f )
		object->rot.y = -D3DX_PI / 2.0f;
	else
		object->rot.y = D3DX_PI / 2.0f;

	// run the behaviors based on the object's current state
	switch( object->characterState )
	{
	case STATE_IDLE:	// not used in this class
	case STATE_WALKING:	// not used in this class
	case STATE_RUNNING:	// not used in this class
	case STATE_PATROL:
		{

			D3DXVECTOR3 MAXDIST = object->startPos + (D3DXVECTOR3(1,1,1)*object->patrolRadius);
			D3DXVECTOR3 MINDIST = object->startPos - (D3DXVECTOR3(1,1,1)*object->patrolRadius);


			if( object->pos.x > MAXDIST.x )
			{
				object->vel.x = -abs( object->vel.x );
				object->vel.y = -abs( object->vel.y );
				object->vel.z = -abs( object->vel.z );
			}
			if( object->pos.x < MINDIST.x )
			{
				object->vel.x = abs( object->vel.x );
				object->vel.y = abs( object->vel.y );
				object->vel.z = abs( object->vel.z );
			}

			object->prevPos = object->pos;

			//Tell Havok the AI wants to move left or right
			if(object->vel.x > 0)
				Havok::GetHavok()->Move(false, EntityListID, .25f);//right
			else
				Havok::GetHavok()->Move(false, EntityListID, -.25f);//left
		}
		break;

	case STATE_SEEK:
		{
			// this block of code takes care of setting the proper velocity for seeking
			//	in the direction of the target, it's very ugly, but was the quickest way
			//	I could get it to run.
			D3DXVECTOR3 tempVel;

			float xval = (target->pos.x - object->pos.x);
			float yval = (target->pos.y - object->pos.y);
			float zval = (target->pos.z - object->pos.z);

			if( abs(xval) < 0.01f )
				tempVel.x = 0.0f;
			else 
				tempVel.x = xval / abs(xval);

			if( abs(yval) < 0.01f ) 
				tempVel.y = 0.0f;
			else
				tempVel.y = yval / abs(yval);

			if( abs(zval) < 0.01f )
				tempVel.z = 0.0f;
			else
				tempVel.z = zval / abs(zval);

			object->prevPos = object->pos;

			if( tempVel.x > 0.0f )
				object->vel.x = abs(object->vel.x);
			else
				object->vel.x = -abs(object->vel.x);

			if( tempVel.y > 0.0f )
				object->vel.y = abs(object->vel.y);
			else
				object->vel.y = -abs(object->vel.y);

			if( tempVel.z > 0.0f )
				object->vel.z = abs(object->vel.z);
			else
				object->vel.z = -abs(object->vel.z);

			//Tell Havok the AI wants to move left or right
			if(object->vel.x > 0)
				Havok::GetHavok()->Move(false, EntityListID, .5f);//right
			else
				Havok::GetHavok()->Move(false, EntityListID, -.5f);//left
		}
		break;

	case STATE_ATTACK_RANGED:
		{
			Shoot();
		}
		break;

	case STATE_ATTACK_MELEE:
		{
			//spawn a hitbox that does not travel anywhere but is large enough to hit the player
			//if the player is within this AI's "melee range"
		}
		break;

	default:
		break;
	};
}

bool AIHandler::CheckWantToShoot()
{
	return WantToShoot;
}

void AIHandler::Shoot()
{
	WantToShoot = true;
}
