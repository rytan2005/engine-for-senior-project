#pragma once

#include "Entity.h"
#include "Weapon.h"
#include "Projectile.h"
/*
	player shoots his firegun for a while.
	Player runs out of ammo(on a firegun its really his clip cause ammo is -1 and recharges the clip). flamethrower causes cooldown of Firegun::cooldown
	set coolDownRemaining to weapon::coolDown;
	constantly update coolDownRemaining until it reaches (falls below) 0.

	//COOL EFFECT REFILL RATE: during this update, recharge the clip continually so that it is full when coolDownRemaining is < 0.
	//However in the mean time, he could just shoot anyway and cancel this effect. and reset coolDownRemaining so it is not updating until gun is empty again
	//He would shoot until his clip is empty and then this would kick on again
	//Theoretically, you could do this same thing but in like 0.5s chunks to reload a slug into the shotgun more realistically

	//even better, the firegun starts recharging as long as you have not shot it within the last x seconds instead of waiting until its empty to recharge
*/

class Player
{
public:
	Player();
	~Player(void);
	
	void Update(float dt); //update the player's ammo count, cooldown remaining	
	void SetWeaponTo(GUNTYPE gt);
	void SwitchWeapon(void)		//swaps between the pistol and the large elemental gun
	{ IsPrimary = !IsPrimary; }
	Weapon* getActiveWeapon();
	Projectile* Shoot();	//checks if it can shoot, then spawns a Projectile* with correct stats and returns it (to be caught by entitymanager)
	short GetGunDamage();
	float GetGunRange();
	float GetGunCoolDownRemaining();
	bool CanShoot();

private:
	Weapon Primary;
	Weapon Secondary;		 
	bool IsPrimary;			 //when true, player is using their primary large elemental weapon. when false, player is using their secondary pistol
};
