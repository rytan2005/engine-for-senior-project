//////////////////////////////////////////////////////////////////////////
//					Character Animation with Direct3D					//
//						   Author: C. Granberg							//
//							   2008 - 2009								//
//////////////////////////////////////////////////////////////////////////

#ifndef SKINNED_MESH
#define SKINNED_MESH

#include <windows.h>
#include <d3dx9.h>
#include <string>
#include <vector>
#include <string>

using namespace std;

struct Bone: public D3DXFRAME
{
    D3DXMATRIX CombinedTransformationMatrix;
};

struct BoneMesh: public D3DXMESHCONTAINER
{
	ID3DXMesh* OriginalMesh;
	vector<D3DMATERIAL9> materials;
	vector<IDirect3DTexture9*> textures;

	DWORD NumAttributeGroups;
	D3DXATTRIBUTERANGE* attributeTable;

	D3DXMATRIX** boneMatrixPtrs;
	D3DXMATRIX* boneOffsetMatrices;
	D3DXMATRIX* currentBoneMatrices;
};

class SkinnedMesh
{
private:
	friend class DX9Render;

public:
	float headAngle;

	SkinnedMesh();
	~SkinnedMesh();
	void Load(char fileName[], IDirect3DDevice9* m_pD3DDevice);
	void Render(Bone *bone, ID3DXEffect* m_pAnimEffect, 
		ID3DXEffect* m_pStaticEffect, D3DXMATRIX &viewProj, IDirect3DDevice9* d3dDev );

	void SetPose(D3DXMATRIX world, float time);
	void SetAnimation(string name);
	void GetAnimations(vector<string> &animations);

	void GetBoneList( vector<string> &bones  );

	void Shutdown();

	void CopyMesh( SkinnedMesh** newMesh );

private:
	void UpdateMatrices(Bone* bone, D3DXMATRIX *parentMatrix);
	void SetupBoneMatrixPointers(Bone *bone);

	D3DXFRAME *m_pRootBone;
	ID3DXAnimationController *m_pAnimControl;

	bool cloned;

	void ReleaseMeshContainer( Bone* bone );

	vector<string> boneNames;
	void CreateBoneList( Bone* bone );

	friend struct GameComponent3DAnimated;
};

#endif