#pragma once

#include "Havok.h"

class Actor
{
	friend class PlatformManager;

protected:
	GameComponent3D* Graphic;
	hkpRigidBody* rBody;

	bool hasTexture;

public:
	Actor(char* filename, D3DXVECTOR3 position,
		float TexU, float TexV,
		D3DXVECTOR3 rotation = D3DXVECTOR3(0,0,0), 
		D3DXVECTOR3 scale = D3DXVECTOR3(1,1,1));
	~Actor(void);

	void Render( Light* light = 0 );
	void Shutdown();
};

