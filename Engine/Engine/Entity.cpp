#include "Entity.h"

Entity::Entity()
{
	IsPrimary = true;
	Status = NORMAL;
	StatusTimer = 0;
	PeriodicDmgTimer = 0;

	canTakeDamage = true;
}

void Entity::TakeDamage(short damage)
{
	if( !canTakeDamage )
		return;

	health -= damage;

	if(health < 1)
	{
		health = 0;

		//play the dying animation
		this->ChangeCharaterState( STATE_DYING );

		//it died, so let the manager know
		alive = false;
	}
}

void Entity::GainHealth(short amount)
{
	health += amount;
}

void Entity::SetHealth(short amount)
{
	health = amount;
}

short Entity::GetHealth()
{
	return health;
}

short Entity::GetMaxHealth()
{
	switch ( entityType )
	{
	case ENTITY_PLAYER:
		{
			return PLAYER_HEALTH;
		}
		break;
	case ENTITY_SMALL_SPIDER:
		{
			return SMALL_SPIDER_HEALTH;
		}
		break;
	case ENTITY_ROBOT:
		{
			return ROBOT_HEALTH;
		}
		break;
	case ENTITY_GUARD:
		{
			return GAURD_HEALTH;
		}
		break;
	case ENTITY_COMMANDER:
		{
			return COMMANDER_HEALTH;
		}
		break;
	case ENTITY_JUGGERNAUT:
		{
			return JUGGERNAUT_HEALTH;
		}
		break;
	case ENTITY_BOSS_SPIDER:
		{
			return BOSS_SPIDER_HEALTH;
		}
		break;
	case ENTITY_MECH_WARRIOR:
		{
			return MECH_WARRIOR_HEALTH;
		}
		break;
	default:
		break;
	}
}
//update the player's ammo count, cooldown remaining
void Entity::Update(float dt)
{
	GameComponent3DAnimated::Update(dt);
	Primary.Update(dt);
	Secondary.Update(dt);

	//Status_Effect update
	UpdateStatus(dt);
}

void Entity::UpdateStatus(float dt)
{
	//if the entity is in the frozen or poisoned states,
	switch (Status)
	{
		case NORMAL:
			{
				//
			}
			break;
		case FROZEN:
			{
				//
			}
			break;
		case POISONED:
			{
				//hurt the enemy every 1 second for ACIDTICKDMG damage because they are poisoned.
				if(PeriodicDmgTimer == 1.0f)
				{
					TakeDamage(ACIDTICKDMG);
				}
			}
			break;
		default:
			break;
	}
	//update the time remaining in a FROZEN or POISONED state
	if(StatusTimer > 0)
	{
		StatusTimer -= dt;

		//update the time remaining before the next periodic damage tick should apply
		if(PeriodicDmgTimer > 0)
		{
			PeriodicDmgTimer -= dt;
		}
		else
		{
			PeriodicDmgTimer = 1.0f;//reset the tick timer
		}
	}
	else//the duration of FROZEN OR POISONED has ended
	{
		//return to normal
		Status = NORMAL;
		PeriodicDmgTimer = 0;
	}
}

void Entity::SetStatus(Status_Effect newStatus, float duration)
{ 
	Status = newStatus;
	StatusTimer = duration;
	PeriodicDmgTimer = 1.0f;
}

//This method should be called by the player's choice at the weapon station
void Entity::SetWeaponTo(GUNTYPE gt)
{
	switch( gt ) //GUNTYPE PISTOL = 0, FIREGUN = 1, FREEZEGUN = 2, ACIDGUN = 3, CONFUSEGUN = 4, SHOTGUN = 5
		{
		case FIREGUN:
			{
				Secondary = FireGun();
			}
			break;

		case FREEZEGUN:
			{
				Secondary = FreezeGun();
			}
			break;

		case ACIDGUN:
			{
				Secondary = AcidGun();
			}
			break;

		case SHOTGUN:
			{
				Primary = Shotgun();
			}
			break;

		case ENEMYSHOTGUN:
			{
				Primary = EnemyShotgun();
			}
			break;

		case ASSAULT:
			
			Primary = Assault();
			
			break;

		default:
			{
				Primary = Pistol();
			}
			break;
		}
}

Weapon* Entity::getActiveWeapon()
{
	if(true == IsPrimary)
	{
		return &Primary; 
	}
	else
	{
		return &Secondary;
	}
}

GUNTYPE* Entity::getWeapons()
{
	GUNTYPE temp[2];

	temp[0] = Primary.GetGunType();
	temp[1] = Secondary.GetGunType();

	return temp;
}

Projectile* Entity::Shoot()//Remove this method after BETA. I plan to be able to shoot secondary with the Right Bumper on Controller without switching weapons.
{
	Projectile* bullet = 0;

	if(true == CanShoot())
	{
		//update the appropriate gun's ammo count
		if(true == IsPrimary)
			Primary.Shoot();
		else
			Secondary.Shoot();

		//create a projectile with the correct damage and range for this weapon
		//shoot a bullet in the player's current direction with the proper element
		if(entityType == ENTITY_PLAYER)
		{
			switch ( getActiveWeapon()->GetGunType() )
			{
				case FREEZEGUN:
					{
						bullet = new Projectile(true, pos, dir, FREEZE, GetGunDamage(),GetGunRange());
					}
				break;
				case ACIDGUN:
					{
						bullet = new Projectile(true, pos, dir, POISON, GetGunDamage(),GetGunRange());
					}
				break;
				case SHOTGUN:
					{
						//shoot a spreadpattern of 8 bullets
						bullet = new Projectile(true, pos, dir, NONE, GetGunDamage(),GetGunRange());
					}
					break;
				default:
					{
						bullet = new Projectile(true, pos, dir, NONE, GetGunDamage(),GetGunRange());
					}
				break;
			}
		}
		else
			bullet = new Projectile(false, pos, dir, NONE, GetGunDamage(),GetGunRange());

		//Play the correct gun sound
		SoundEffect* tempSound = getActiveWeapon()->weaponFire;
		AudioManager::GetInstance()->PlaySFX(*tempSound);

		return bullet;
	}

	return bullet;//(this = 0) because the player cannot shoot right now
}

Projectile* Entity::ShootPrimary()
{
	Projectile* bullet = 0;

	if(true == CanShoot())
	{
		Primary.Shoot();

		if(entityType == ENTITY_PLAYER)
			bullet = new Projectile(true, pos, dir, NONE, GetGunDamage(),GetGunRange());
		else
			bullet = new Projectile(false, pos, dir, NONE, GetGunDamage(),GetGunRange());

		//Play the correct gun sound
		SoundEffect* tempSound = getActiveWeapon()->weaponFire;
		AudioManager::GetInstance()->PlaySFX(*tempSound);

		return bullet;
	}
	return bullet;
}

Projectile* Entity::ShootSecondary()
{
	Projectile* bullet = 0;

	if(true == CanShoot())
	{
		Secondary.Shoot();

		//create a projectile with the correct damage and range for this weapon
		//shoot a bullet in the player's current direction with the proper element
		if(entityType == ENTITY_PLAYER)
		{
			switch ( Secondary.GetGunType() )
			{
				case FREEZEGUN:
					{
						bullet = new Projectile(true, pos, dir, FREEZE, GetGunDamage(),GetGunRange());
					}
				break;
				case ACIDGUN:
					{
						bullet = new Projectile(true, pos, dir, POISON, GetGunDamage(),GetGunRange());
					}
				break;
				default:
					{
						bullet = new Projectile(true, pos, dir, NONE, GetGunDamage(),GetGunRange());
					}
				break;
			}
		}
		else
			bullet = new Projectile(false, pos, dir, NONE, GetGunDamage(),GetGunRange());

		//Play the correct gun sound
		SoundEffect* tempSound = getActiveWeapon()->weaponFire;
		AudioManager::GetInstance()->PlaySFX(*tempSound);

		return bullet;
	}
	return bullet;//(this = 0) because the player cannot shoot right now
}

bool Entity::CanShoot()
{
	if( true == IsPrimary )
	{
		if( Primary.Clip > 0 )
			return Primary.CanShoot;

		return false;
	}else
	{
		if( Secondary.Clip > 0 )
			return Secondary.CanShoot;
	
		return false;
	}
}

short Entity::GetGunDamage()
{
	if(true == IsPrimary)
		return Primary.GetDamage();
	else
		return Secondary.GetDamage();
}
float Entity::GetGunRange()
{
	if(true == IsPrimary)
		return Primary.GetRange();
	else
		return Secondary.GetRange();
}

float Entity::GetGunCoolDownRemaining()
{
	if(true == IsPrimary)
		return Primary.GetCoolDownRemaining();
	else
		return Secondary.GetCoolDownRemaining();
}