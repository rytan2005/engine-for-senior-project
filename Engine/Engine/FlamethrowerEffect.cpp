#include "FlamethrowerEffect.h"
#include <ctime>


FlamethrowerEffect* FlamethrowerEffect::instance = 0;
FlamethrowerEffect* FlamethrowerEffect::GetEffect()
{
	if( !instance )
	{
		instance = new FlamethrowerEffect();
		instance->filename = "assets/shaders/flamethrower.hlsl";
		instance->techname = "tech0"; 
		instance->texname = "assets/textures/particles/fire2.png";
	}
	return instance;
}

void FlamethrowerEffect::InitParticle( Particle& out )
{
	if( direction > 0.0f )
		direction = 1.0f;
	if( direction < 0.0f )
		direction = -1.0f;

	out.initialPos = origin;
	out.initialPos.x += direction*0.7f;

	out.initialTime = mTime;
	out.lifeTime = GetRandomFloat( 0.1f, 0.3f );
	out.initialColor = D3DXCOLOR( 1.0f, 1.0f, 1.0f, 1.0f );
	out.initialSize = GetRandomFloat( 3.5f, 5.0f );
	out.mass = GetRandomFloat( 0.8f, 1.2f );

	out.initialVelocity.x = direction * GetRandomFloat( 13.0f, 17.0f );
	out.initialVelocity.y = GetRandomFloat( -5.0f, 3.5f );
	out.initialVelocity.z = GetRandomFloat( -2.0f, 2.0f );
}

void FlamethrowerEffect::AddParticles( short direction, 
		PARTICLE_DETAIL_LEVEL detailLevel )
{
	this->direction = direction;

	int numParticlesToInit = 0;
	switch( detailLevel )
	{
	case PARTICLE_DETAIL_LOW:
		numParticlesToInit = 5;
		break;

	case PARTICLE_DETAIL_MEDIUM:
		numParticlesToInit = 10;
		break;

	case PARTICLE_DETAIL_HIGH:
		numParticlesToInit = 15;
		break;

	default:
		numParticlesToInit = 5;
		break;
	};

	for( short i = 0; i < numParticlesToInit; ++i )
	{
		AddParticle();
	}
}