#pragma once
#include "Entity.h"


class  SmallSpider:Entity
{
public:
	SmallSpider(D3DXVECTOR3 pos);
	~SmallSpider(void); 
};

class Robot:Entity
{
public:
	Robot(D3DXVECTOR3 pos);
	~Robot(void);
};

class Guard:Entity
{
public:
	Guard(D3DXVECTOR3 pos);
	~Guard(void);
};

class GuardCommander:Entity
{
public:
	GuardCommander(D3DXVECTOR3 pos);
	~GuardCommander(void);
};
class Spider:Entity
{
	public:
	Spider(D3DXVECTOR3 pos);
	~Spider(void);
};
class MechWarrior:Entity
{
	public:
		MechWarrior(D3DXVECTOR3 pos);
		~MechWarrior(void);
};
class Juggernuat:Entity
{
public:
	Juggernuat(D3DXVECTOR3 pos);
	~Juggernuat(void);
};