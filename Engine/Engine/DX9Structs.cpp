#include "DX9Structs.h"

IDirect3DVertexDeclaration9* DefaultVertex::decl = 0;

//////////////////////////////////////////////////////////////////////////
/// METHODS FOR THE ANIMATION LIST
//////////////////////////////////////////////////////////////////////////
AnimationList::AnimationList()
{
	this->fileName = "";
	this->mesh = NULL;
	this->next = NULL;
}

//////////////////////////////////////////////////////////////////////////
///	METHODS FOR THE 2D, AND 3D GAME COMPONENT OBJECTS
//////////////////////////////////////////////////////////////////////////
AIComponent::AIComponent()
{
	texCoord = D3DXVECTOR2( 1.0f, 1.0f );
	this->animTime = 2.5f;
	canRemove = false;
	isJumping = false;
	jumpTime = 3.0f;
}

void AIComponent::EnableAI( bool enable )
{
	AIControlled = enable;
}

void AIComponent::ChangeCharaterState(CHARACTER_STATE state)
{
	this->characterState = state;
}

void GameComponent3DAnimated::ChangeCharaterState(CHARACTER_STATE state)
{
	if( !alive )
	{
		if( characterState == STATE_DYING )
			return;
		else
		{
			characterState = STATE_DYING;
			ChangeAnimationTrack( "DEATH_FALL_FORWARD" );
		}
	}

	if( !isJumping && alive )
	{
		if( characterState != state )
		{
			characterState = state;
			jumpTime = 1.0f;

			switch (characterState)
			{
			case STATE_IDLE:
				ChangeAnimationTrack("IDLE_COMBAT");
				break;

			case STATE_WALKING:
			case STATE_PATROL:
				ChangeAnimationTrack("WALKING");
				break;

			case STATE_RUNNING:
			case STATE_SEEK:
				ChangeAnimationTrack("RUN");
				break;

			case STATE_ATTACK_RANGED:
				// set ranged attack animation
				break;

			case STATE_ATTACK_MELEE:
				// set melee attack animation
				break;

			case STATE_DYING:
				ChangeAnimationTrack( "DEATH_FALL_FORWARD" );
				break;

			case STATE_JUMP:
				ChangeAnimationTrack( "JUMP" );
				break;

			default:
				characterState = STATE_IDLE;
				break;
			}

			skinnedMesh->m_pAnimControl->SetTrackSpeed( 0, 1.0f );
			skinnedMesh->m_pAnimControl->SetTrackPosition( 0, 0.0f );

			if( state == STATE_JUMP )
			{
				skinnedMesh->m_pAnimControl->SetTrackPosition( 0, 0.2f );
			}
		}
	}
}

void AIComponent::ChangeWeaponType(WEAPON_TYPE type)
{
	weaponType = type;
}

void AIComponent::SetStartPos( D3DXVECTOR3 position, float PatrolRadius, float PersuitRadius, D3DXVECTOR3 velocity )
{
	startPos = position;
	pos = position;
	prevPos = pos;
	patrolRadius = PatrolRadius;
	vel = velocity;

	persuitRadius = PersuitRadius;
}

void AIComponent::setAttackRanges( float meleeAttackDist, float projectileAttackDist )
{
	meleeAttackRange = meleeAttackDist;
	projectileAttackRange = projectileAttackDist;
}

void GameComponent2D::Shutdown()
{
	texture->Release();
	texture = 0;

	delete this;
}

void GameComponent3D::Shutdown()
{
	if( textures )
	{
		for( DWORD i = 0; i < numMaterials; ++i )
			SAFE_RELEASE( textures[i] );
		delete[] textures;
	}

	if( materials )
	{
		delete[] materials;
	}

	//SAFE_RELEASE( mesh );

	delete this;
}

void GameComponent3DAnimated::Update(float dt)
{
	UINT index = 0;

	if( !alive )
		ChangeCharaterState( STATE_DYING );

	switch (characterState)
	{
	case STATE_DYING:
		animTime -= dt;
		if( animTime < 0.0f )
		{
			animTime = 2.5f;
			canRemove = true;
			skinnedMesh->m_pAnimControl->SetTrackSpeed( 0, 0.0f );
		}else
		{
			canRemove = false;
		}
		break;

	case STATE_JUMP:
		jumpTime -= dt;
		if( jumpTime < 0.0f )
		{
			skinnedMesh->m_pAnimControl->SetTrackSpeed( 0, 0.0f );
		}else
		{
			skinnedMesh->m_pAnimControl->SetTrackSpeed( 0, 1.0f );
		}

		break;

	default:
		break;
	}

	D3DXMATRIX transmat, scalemat, rotmat;
	D3DXMatrixTranslation( &transmat, 
		pos.x + offset.x,
		pos.y + offset.y,
		pos.z + offset.z );
	D3DXMatrixScaling( &scalemat, scale.x, scale.y, scale.z );
	D3DXMatrixRotationYawPitchRoll( &rotmat, rot.y, rot.x, rot.z );
	D3DXMatrixMultiply( &scalemat, &scalemat, &transmat );
	D3DXMatrixMultiply( &worldMat, &rotmat, &scalemat );

	skinnedMesh->SetPose( worldMat, dt );
}

void GameComponent3DAnimated::Shutdown()
{
	skinnedMesh->Shutdown();
}

void GameComponent3DAnimated::ChangeAnimationTrack( string track )
{
	skinnedMesh->SetAnimation( track );
}



Light::Light()
{
	type = 0;
	range = 0;
	position[0] = position[1] = position[2] = 0.0f;
	direction[0] = direction[1] = direction[2] = 0.0f;
	attenuation[0] = attenuation[1] = attenuation[2] = 0.0f;
}

Light::Light( int Type, float Range, float Position[3],
	float Direction[3], float Attenuation[3], float Ambient[3] )
{
	type = Type;
	range = Range;
	position[0] = Position[0];
	position[1] = Position[1];
	position[2] = Position[2];
	direction[0] = Direction[0];
	direction[1] = Direction[1];
	direction[2] = Direction[2];
	attenuation[0] = Attenuation[0];
	attenuation[1] = Attenuation[1];
	attenuation[2] = Attenuation[2];
	ambient[0] = Ambient[0];
	ambient[1] = Ambient[1];
	ambient[2] = Ambient[2];
}