#pragma once

#include "ParticleEffect.h"

class BloodEffect : public ParticleEmitter
{
private:
	static BloodEffect* instance;
	BloodEffect(void){}
	BloodEffect(const BloodEffect& t){}
	void operator= (const BloodEffect& t ){}

public:
	static BloodEffect* GetEffect();

	void InitParticle( Particle& out );

	void AddParticles( short direction, 
		PARTICLE_DETAIL_LEVEL detailLevel );
	
	~BloodEffect(void){}
};

