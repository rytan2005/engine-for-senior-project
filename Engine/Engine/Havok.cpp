#include "Havok.h"
#include "MyContactStateListener.h"
#include "MyPhantomShape.h"
#include<ctime>

Havok* Havok::hkPhysicsCore = 0;
Havok::Havok(){}
Havok::~Havok(){}

/*
  Returns a pointer to the Havok Instance.
*/
Havok* Havok::GetHavok()
{
	if( !hkPhysicsCore )
	{
		hkPhysicsCore = new Havok;
		hkPhysicsCore->Initialized = false;
	}
	return hkPhysicsCore;
}

bool Havok::InitHavok(void)
{
	// Perfrom platform specific initialization for this demo - you should already have something similar in your own code.
	PlatformInit();

	// Need to have memory allocated for the solver. Allocate 1mb for it.
	MemoryRouter = hkMemoryInitUtil::initDefault( hkMallocAllocator::m_defaultMallocAllocator, hkMemorySystem::FrameInfo(1024 * 1024) );
	hkBaseSystem::init( MemoryRouter, errorReport );

	// Initialize the monitor stream so that we can send timing information to the Visual Debugger.
	const int monitorSize = 128 * 1024;
	hkMonitorStream& stream = hkMonitorStream::getInstance();
	stream.resize( monitorSize );
	stream.reset();

	//create the world info and set world size, gravity etc.
	hkpWorldCinfo info;
	info.setBroadPhaseWorldSize( 350.0f );  
	info.m_gravity.set(0, -9.8f, 0);
	info.m_contactPointGeneration = hkpWorldCinfo::CONTACT_POINT_ACCEPT_ALWAYS;	//used in rigidbody character demos - not used in proxy demos
	//info.m_collisionTolerance = 0.1f;											//used in character "proxy" demos - not used in rigidbody character demos

	//now create the world using the worldInfo we've provided
	World = new hkpWorld( info );

	// Register all collision agents
	// It's important to register collision agents before adding any entities to the world.
	hkpAgentRegisterUtil::registerAllAgents( World->getCollisionDispatcher() );

	//create a custom collision filter.
	hkpGroupFilter* filter = new hkpGroupFilter();

	FilterSetup = new CustomCollisionFilter();
	FilterSetup->setupGroupFilter(filter);

	World->setCollisionFilter(filter);

	p_Listener = new MyContactStateListener();
	World->addContactListener(p_Listener);

	Initialized = true;

	StandShape = NULL;
	CrouchShape = NULL;
	PreviousGround = NULL;
	numPlayers = 0;
	numEnemies = 0;

	//seed the randomizer used in bullet's accuracy
	std::srand((unsigned)std::time(0)); 

	return true;
}

void Havok::SetupHavokVisualDebugger(void)
{
	// Register all the physics viewers
	hkpPhysicsContext::registerAllPhysicsProcesses(); 

	// Set up a physics context containing the world for the use in the visual debugger
	Context = new hkpPhysicsContext;
	Context->addWorld(World); 

	// Set up the visual debugger
	{
		// Setup the visual debugger
		hkArray<hkProcessContext*> contexts;
		contexts.pushBack(Context);

		VisualDebugger = new hkVisualDebugger(contexts);
		VisualDebugger->serve();
	}
}

/*create a boxshape rigidbody by passing in the x,y,z coords, position and mass of the box as well as its hkpMotionType
  then add it to the list of rigidbodies in the world object
*/
hkpRigidBody* Havok::AddBoxShape(float halfWidth, float halfHeight, float halfDepth, D3DXVECTOR3 startPos, float mass, short hkpMotionType)
{
	/*hkpMotionType::enum
	0	=	MOTION_INVALID,

	/// A fully-simulated, movable rigid body. At construction time the engine checks
	/// the input inertia and selects MOTION_SPHERE_INERTIA or MOTION_BOX_INERTIA as
	/// appropriate.
	1	=	MOTION_DYNAMIC,

	2	=	MOTION_SPHERE_INERTIA,
	3	=	MOTION_BOX_INERTIA,

	/// Simulation is not performed as a normal rigid body. During a simulation step,
	/// the velocity of the rigid body is used to calculate the new position of the
	/// rigid body, however the velocity is NOT updated. The user can keyframe a rigid
	/// body by setting the velocity of the rigid body to produce the desired keyframe
	/// positions. The hkpKeyFrameUtility class can be used to simply apply keyframes
	/// in this way. The velocity of a keyframed rigid body is NOT changed by the
	/// application of impulses or forces. The keyframed rigid body has an infinite
	/// mass when viewed by the rest of the system.
	4	=	MOTION_KEYFRAMED,

	/// This motion type is used for the static elements of a game scene, e.g., the
	/// landscape. Fixed rigid bodies are treated in a special way by the system. They
	/// have the same effect as a rigid body with a motion of type MOTION_KEYFRAMED
	/// and velocity 0, however they are much faster to use, incurring no simulation
	/// overhead, except in collision with moving bodies.
	5	=	MOTION_FIXED,

	6	=	MOTION_THIN_BOX_INERTIA,
	/// A specialized motion used for character controllers
	7	=	MOTION_CHARACTER

	You will only ever need to use 4 and 5 because 1 is the default and that autoselects 2 or 3.
	*/
	hkVector4 halfExtents;
	halfExtents.set(halfWidth, halfHeight, halfDepth);
	hkpBoxShape* boxShape = new hkpBoxShape(halfExtents);

	hkpRigidBodyCinfo boxInfo;
	boxInfo.m_shape = boxShape;

	if(hkpMotionType == 1)
		boxInfo.m_motionType = hkpMotion::MOTION_DYNAMIC;
	else if(hkpMotionType == 4)
		boxInfo.m_motionType = hkpMotion::MOTION_KEYFRAMED;
	else if(hkpMotionType == 5)
		boxInfo.m_motionType = hkpMotion::MOTION_FIXED;
	boxInfo.m_position(0) = startPos[0];
	boxInfo.m_position(1) = startPos[1];
	boxInfo.m_position(2) = startPos[2];

	if(hkpMotionType != 5)//MOTION_FIXED does not need to calculate mass since its a unmoving platform/terrain
	{
		//calculate the mass properties for the shape
		const hkReal boxMass = mass;
		hkMassProperties massProperties;
		hkpInertiaTensorComputer::computeShapeVolumeMassProperties(boxShape, boxMass, massProperties);

		boxInfo.setMassProperties(massProperties);
	}

	//create the rigidbody
	hkpRigidBody* RigidBody = new hkpRigidBody(boxInfo);

	if(hkpMotionType == 4)//if keyframed, setCollisionFilterInfo as LAYER_KEYFRAME
	{
		RigidBody->setCollisionFilterInfo( hkpGroupFilter::calcFilterInfo(hkpGroupFilterSetup::LAYER_KEYFRAME));
	}
	else if(hkpMotionType == 5)//if FIXED, setCollisionFilterInfo as LAYER_STATIC so it doesn't collide with keyframed stuff
	{
		RigidBody->setCollisionFilterInfo( hkpGroupFilter::calcFilterInfo(hkpGroupFilterSetup::LAYER_STATIC));
	}

	//add the rigidBody to the world
	World->addEntity(RigidBody);

	//The world now owns the rigid body so removeReference
	RigidBody->removeReference();

	//No longer need the reference to the boxshape
	boxShape->removeReference();

	return RigidBody;
}

hkpRigidBody* Havok::CreatePlayerRigidBody( short i, float position[3] )
{
	PlayerInputs[i].m_wantJump = false;

	// create a character rigid body object
	// contruct a capsule shape
	hkVector4 vertexA( 0, 0.6f, 0 );
	hkVector4 vertexB( 0, -0.6f, 0 );

	// create a capsule to represent the character standing
	if( StandShape == 0 )
		StandShape = new hkpCapsuleShape( vertexA, vertexB, 0.3f );

	// create a capsule to represent the chracter crouching
	if( CrouchShape == 0)
	{
		vertexA.setZero4();
		CrouchShape = new hkpCapsuleShape( vertexA, vertexB, 0.3f );
	}

	// construct a character rigid body
	hkpCharacterRigidBodyCinfo info;
	info.m_mass = 100.0f;
	info.m_shape = StandShape;

	info.m_maxForce = 1000.0f;
	info.m_up = UP;
	info.m_position.set( position[0], position[1], position[2] );
	info.m_maxSlope = 70.0f * HK_REAL_DEG_TO_RAD;
	info.m_collisionFilterInfo = CustomCollisionFilter::LAYER_PLAYER; // player id = 3;

	hkpCharacterRigidBody* RigidBody = new hkpCharacterRigidBody( info );
	Players[i].p_CharacterBody = RigidBody;
	{
		hkpCharacterRigidBodyListener* listener = new hkpCharacterRigidBodyListener();
		Players[i].p_CharacterBody->setListener( listener );
		listener->removeReference();
	}
	World->addEntity( Players[i].p_CharacterBody->getRigidBody() );
	
	// create the characer state machine and context
	{
		hkpCharacterState* state;
		hkpCharacterStateManager* manager = new hkpCharacterStateManager();

		state = new hkpCharacterStateOnGround();
		manager->registerState( state, HK_CHARACTER_ON_GROUND );
		state->removeReference();

		state = new hkpCharacterStateInAir();
		manager->registerState( state,	HK_CHARACTER_IN_AIR);
		state->removeReference();

		state = new hkpCharacterStateJumping();
		manager->registerState( state,	HK_CHARACTER_JUMPING);
		state->removeReference();

		state = new hkpCharacterStateClimbing();
		manager->registerState( state,	HK_CHARACTER_CLIMBING);
		state->removeReference();

		Players[i].p_CharacterContext = new hkpCharacterContext( manager, HK_CHARACTER_ON_GROUND );
		manager->removeReference();

		// Set character type
		Players[i].p_CharacterContext->setCharacterType(hkpCharacterContext::HK_CHARACTER_RIGIDBODY);
	}

	if( PreviousGround == 0 )
		PreviousGround = new hkpSurfaceInfo();

	Players[i].XAccel = 0;
	Players[i].YAccel = 0;
	Players[i].lockMovement = false;
	Players[i].movePhantom = false;

	numPlayers = i+1;

	return RigidBody->getRigidBody();
}

hkpRigidBody* Havok::AddEnemy(D3DXVECTOR3 startPos)
{
	EnemyInputs[numEnemies].m_wantJump = false;

	//create a rigidbody for the AI enemy
	{
		// contruct a capsule shape
		hkVector4 vertexA( 0, 0.6f, 0 );
		hkVector4 vertexB( 0, -0.6f, 0 );

		// create a capsule to represent the character standing
		if( StandShape == 0 )
			StandShape = new hkpCapsuleShape( vertexA, vertexB, 0.3f );

		// create a capsule to represent the chracter crouching
		if( CrouchShape == 0)
		{
			vertexA.setZero4();
			CrouchShape = new hkpCapsuleShape( vertexA, vertexB, 0.3f );
		}

		// construct a character rigid body
		hkpCharacterRigidBodyCinfo info;
		info.m_mass = 100.0f;
		info.m_shape = StandShape;

		info.m_up = UP;
		info.m_position.set( startPos.x, startPos.y, startPos.z);
		info.m_maxSlope = 70.0f * HK_REAL_DEG_TO_RAD;
		info.m_collisionFilterInfo = CustomCollisionFilter::LAYER_ENEMY; // enemy id = 4;

		Enemies[numEnemies].p_CharacterBody = new hkpCharacterRigidBody( info );
		{
			//hkpCharacterRigidBodyListener* listener = new hkpCharacterRigidBodyListener();
			//Enemies[numEnemies].p_CharacterBody->setListener( listener );
			//listener->removeReference();
		}

		World->addEntity( Enemies[numEnemies].p_CharacterBody->getRigidBody() );
	}

	// create the characer state machine and context
	{
		hkpCharacterState* state;
		hkpCharacterStateManager* manager = new hkpCharacterStateManager();

		state = new hkpCharacterStateOnGround();
		manager->registerState( state, HK_CHARACTER_ON_GROUND );
		state->removeReference();

		state = new hkpCharacterStateInAir();
		manager->registerState( state,	HK_CHARACTER_IN_AIR);
		state->removeReference();

		state = new hkpCharacterStateJumping();
		manager->registerState( state,	HK_CHARACTER_JUMPING);
		state->removeReference();

		state = new hkpCharacterStateClimbing();
		manager->registerState( state,	HK_CHARACTER_CLIMBING);
		state->removeReference();

		Enemies[numEnemies].p_CharacterContext = new hkpCharacterContext( manager, HK_CHARACTER_ON_GROUND );
		manager->removeReference();

		// Set character type
		Enemies[numEnemies].p_CharacterContext->setCharacterType(hkpCharacterContext::HK_CHARACTER_RIGIDBODY);
	}

	if( PreviousGround == 0 )
		PreviousGround = new hkpSurfaceInfo();

	Enemies[numEnemies].XAccel = 0;
	Enemies[numEnemies].YAccel = 0;
	numEnemies++; //notify that an enemy has been added

	return Enemies[numEnemies - 1].p_CharacterBody->getRigidBody();
}

hkpRigidBody* Havok::AddBullet(bool isPlayer, D3DXVECTOR3 startPos, D3DXVECTOR3 bulletDir)
{
		hkVector4 hkBulletDir;
		//convert the D3DXVECTOR3 into an hkVector4
		hkBulletDir(0) = bulletDir[0];
		hkBulletDir(1) = bulletDir[1];
		hkBulletDir(2) = bulletDir[2];
	
		//simulating imperfect accuracy
		float y = static_cast<float>(std::rand()) / RAND_MAX * 0.05f - 0.025f;//* max - min

		//randomize the up and down a bit to make the bullets' travel appear less uniform 
		hkBulletDir(1) += y;

		hkpRigidBodyCinfo ci;
		ci.m_shape = new hkpSphereShape( 0.025f);
		ci.m_mass = 0.1f;
		ci.m_qualityType = HK_COLLIDABLE_QUALITY_BULLET;
		ci.m_restitution = 0.2f;
		ci.m_friction = 0.3f;
		ci.m_position.set( 0.0f, 1.0f, 0.0f );
		ci.m_motionType = hkpMotion::MOTION_SPHERE_INERTIA;
		hkMatrix3Util::_setDiagonal( 100.0f, 100.0f, 100.0f, ci.m_inertiaTensor );
		ci.m_linearVelocity.setMul4( 25.0f, hkBulletDir );
		ci.m_gravityFactor = 0;//make gravity not affect the bullets

		//convert the D3DXVECTOR3 into an hkVector4
		ci.m_position(0) = startPos[0];
		ci.m_position(1) = startPos[1];
		ci.m_position(2) = startPos[2];

		////////////////
		//Player bullet/
		////////////////
		if(true == isPlayer)
		{
			ci.m_collisionFilterInfo = hkpGroupFilter::calcFilterInfo(CustomCollisionFilter::LAYER_PLAYER_BULLET);//6 is LAYER_PLAYER_BULLET
		}
		////////////////////
		else //enemy bullet/
		////////////////////
		{
			ci.m_collisionFilterInfo = hkpGroupFilter::calcFilterInfo(CustomCollisionFilter::LAYER_ENEMY_BULLET);//7 is LAYER_ENEMY_BULLET
		}

		hkpRigidBody* bullet = new hkpRigidBody(ci);
		ci.m_shape->removeReference();
	
		World->addEntity(bullet);

		return bullet;
}

/*Create an AABB that will act as a trigger if the  AI enemies enter the volume
  Havok Defines the AABB with a minimum vertex position and a maximum vertex position
  Think of them as two corners of the box where the difference between their x, y, z defines the width, height, length, of the box
  These two corners do represent the box's location in world space
  REMEMBER: the minv CANNOT have a x, y, or z value greater than the maxv's x, y, or z.
  If the minv is higher in any of these, it will throw an error exception
*/
void Havok::CreateFireGunPhantomAABB(short thePlayerNumber)
{
	hkReal halfwidth = 0.05f;//1.75f;
	hkReal halfheight = 0.75f, halfdepth = 1.0f; //MAKE SURE THIS halfwidth MATCHES THE RANGE OF THE FIREGUN in weapon.h


	hkpRigidBodyCinfo boxInfo;
	hkVector4 boxSize( halfwidth, halfheight, halfdepth );
	hkpShape* boxShape = new hkpBoxShape( boxSize , 0 );
	boxInfo.m_motionType = hkpMotion::MOTION_FIXED;

	boxInfo.m_position.set(-100,-100,0);//set it at -100,-100 so it wont collide with anything until we move it later

	// MyPhantomShape is the demo implementation of the hkpPhantomCallbackShape.
	MyPhantomShape* myPhantomShape = new MyPhantomShape();
	hkpBvShape* bvShape = new hkpBvShape( boxShape, myPhantomShape );
	boxShape->removeReference();
	myPhantomShape->removeReference();

	boxInfo.m_shape = bvShape;
	boxInfo.m_collisionFilterInfo = CustomCollisionFilter::LAYER_PLAYER_ATTACK_PHANTOM;
	hkpRigidBody* rigidBody = new hkpRigidBody( boxInfo );
	bvShape->removeReference();

	FiregunPhantom[thePlayerNumber] = rigidBody;
	World->addEntity(FiregunPhantom[thePlayerNumber]);
}

/*Call this when the player shoots. It will reposition the singleton phantom created for that player.
*/
void Havok::ShootFireGun(FireGun* thePlayersGun, short thePlayerNumber)
{
	if(0 == thePlayersGun->Clip)
		return;//do nothing because the firegun is empty and needs to recharge
	if(thePlayersGun->CoolDownRemaining > 0)
		return;//do nothing because the firegun is still recharging
	//if(thePlayersGun->ROFTimer > 0)
		//return;//you cant shoot faster than rate of fire
	else//ROFTimer <= 0 aka the gun is allowed to shoot now
	{
		D3DXVECTOR3 playerPos = EntityManager::GetEntityManager()->EntityList[thePlayerNumber]->pos;
		hkVector4 pos;
		pos(0) = playerPos.x; pos(1) = playerPos.y; pos(2) = playerPos.z;

		if(EntityManager::GetEntityManager()->EntityList[thePlayerNumber]->dir == DIR_LEFT)
		{
			//move by a halfwidth left
			pos(0) -= 0.3f;//1.75f;
		}
		else //DIR == RIGHT
		{
			//move by a halfwidth right
			pos(0) += 0.3f;//1.75f;
		}
		FiregunPhantom[thePlayerNumber]->setPosition(pos);
		Players[thePlayerNumber].movePhantom = true;
		thePlayersGun->Shoot();
	}
}

void Havok::UpdateFireGuns()
{
	EntityManager* p_EMan = EntityManager::GetEntityManager();
	hkVector4 pos;

	for(int i = 0; i < numPlayers; ++i)
	{
		Weapon* playerGun = p_EMan->EntityList[i]->getActiveWeapon();

		if(playerGun->GunType == FIREGUN)
		{
			if(Players[i].movePhantom == false)//the fire phantom has reached the max range of the firegun
			{
				if(playerGun->Clip > 0 && playerGun->ROFTimer <= 0 && Players[i].lockMovement == true)//if ROF says its time to shoot, keep unloading the flamethrower until it empties its entire clip
				{
					ShootFireGun((FireGun*)playerGun,i);
				}
				if (FiregunPhantom != NULL)
				{
					if(Players[i].lockMovement == false && FiregunPhantom[i]->getPosition()(1) != -100)
					{
						pos(0) = -100; pos(1) = -100; pos(2) = 0; pos(3) = 0;//place the phantoms in some arbitrary location when they're not being used.
						FiregunPhantom[i]->setPosition(pos);
					}
				}
			}
			else//if(Players[i].movePhantom == true)
			{
				float playerPos = p_EMan->EntityList[i]->pos.x;
				float phantomPos = FiregunPhantom[i]->getPosition()(0);

				if(abs( phantomPos - playerPos) < 3.5f)//make sure this 3.5f is equal to the value stored as FIREGUN's Range
				{
					//the phantom has not yet traveled to the max range of the firegun so keep traveling
					pos = FiregunPhantom[i]->getPosition();

					if(EntityManager::GetEntityManager()->EntityList[i]->dir == DIR_LEFT)
					{
						//move left
						pos(0) -= 0.20f;
					}
					else //DIR == RIGHT
					{
						//move right
						pos(0) += 0.20f;
					}
				
					FiregunPhantom[i]->setPosition(pos);
				}
				else
				{
					//the phantom has traveled as far as the firegun's range can shoot so stop it from moving any more
					Players[i].movePhantom = false;
				}
			}
		}
	}
}

D3DXVECTOR3 Havok::GetCharacterPosition(bool isPlayer, unsigned short HavokID)
{
	D3DXVECTOR3 pos;

	if(isPlayer == true)
		HkVecToD3DXVec( Players[HavokID].p_CharacterBody->getPosition(), pos );
	else
		{	//not a player then its an enemy.
			//search for the id number
			for( int j = 0; j < numEnemies; ++j)
			{
				if(Enemies[j].p_CharacterBody->getRigidBody()->getUid() == HavokID)
				{
					HkVecToD3DXVec( Enemies[j].p_CharacterBody->getPosition(), pos);
					break;
				}
			}
		}

	return pos;
}

void Havok::Jump(bool isPlayer, unsigned short HavokID )
{
	if(isPlayer == true)
	{
		if(Players[HavokID].lockMovement == false)
			PlayerInputs[HavokID].m_wantJump = true;
	}
	else
	{	//not a player then its an enemy
			
		//search for the id number
		for( int j = 0; j < numEnemies; ++j)
		{
			if(Enemies[j].p_CharacterBody->getRigidBody()->getUid() == HavokID)
			{
				EnemyInputs[j].m_wantJump = true;
				break;
			}
		}
	}
}
void Havok::Move(bool isPlayer, unsigned short HavokID, float speed)
{
	if(isPlayer == true)
	{
		if(Players[HavokID].lockMovement == false)
			Players[HavokID].XAccel = -1 * speed;
	}
	else//not a player then its an enemy
	{
		//search for the id number
		for( int j = 0; j < numEnemies; ++j)
		{
			if(Enemies[j].p_CharacterBody->getRigidBody()->getUid() == HavokID)
			{
				Enemies[j].XAccel = -1 * speed;
				break;
			}
		}
		Enemies[HavokID].XAccel = -1*speed;
	}
}

void Havok::StopMovingHorizontal(bool isPlayer, unsigned short HavokID )
{
	if(isPlayer == true)
		Players[HavokID].XAccel = 0;
	else
	{	//not a player then its an enemy.
		for( int j = 0; j < numEnemies; ++j)
		{
			if(Enemies[j].p_CharacterBody->getRigidBody()->getUid() == HavokID)
			{
				Enemies[j].XAccel = 0;
				break;
			}
		}
	}
}

void Havok::LockMovement(bool isPlayer, unsigned short HavokID, float secsLocked)
{
	StopMovingHorizontal(isPlayer, HavokID);

	if(isPlayer == true)
	{		
		Players[HavokID].lockMovement = true;
		Players[HavokID].lockTimer = secsLocked;
	}
	else
	{
		for( int j = 0; j < numEnemies; ++j)
		{
			if(Enemies[j].p_CharacterBody->getRigidBody()->getUid() == HavokID)
			{
				Enemies[j].lockMovement = true;
				Enemies[j].lockTimer = secsLocked;
				break;
			}
		}
	}
}

void Havok::setPosition(unsigned short i, D3DXVECTOR3 position)
{
	hkVector4 pos;
	pos(0) = position.x;
	pos(1) = position.y;
	pos(2) = position.z;
	Players[i].p_CharacterBody->getRigidBody()->setPosition(pos);
}

void Havok::Update(float dT)
{
	UpdatePlayers(dT);

	UpdateEnemies(dT);

	UpdateFireGuns();

	// Do a simulation step
	World->lock();
	World->stepDeltaTime(dT);
	World->unlock();
	// Step the debugger
	//VisualDebugger->step();
}

void Havok::UpdatePlayers(float dT)
{
	//update the player(s)
	for( byte i = 0; i < numPlayers; ++i )
	{
		hkpCharacterOutput output;

		PlayerInputs[i].m_inputLR = Players[i].XAccel;
		PlayerInputs[i].m_inputUD = Players[i].YAccel;

		PlayerInputs[i].m_atLadder = false;

		PlayerInputs[i].m_up = UP;
		PlayerInputs[i].m_forward.set(1,0,0);

		CurrentAngle = HK_REAL_PI * 0.5f;	

		hkRotation currentRot;
		currentRot.setAxisAngle(UP, CurrentAngle);
		PlayerInputs[i].m_forward.setRotatedDir( currentRot, PlayerInputs[i].m_forward );

		hkStepInfo stepInfo;
		stepInfo.m_deltaTime = dT;
		stepInfo.m_invDeltaTime = 1.0f/dT;

		PlayerInputs[i].m_stepInfo = stepInfo;

		PlayerInputs[i].m_characterGravity.set(0,-16, 0);
		PlayerInputs[i].m_velocity = Players[i].p_CharacterBody->getRigidBody()->getLinearVelocity();
		PlayerInputs[i].m_position = Players[i].p_CharacterBody->getRigidBody()->getPosition();

		Players[i].p_CharacterBody->checkSupport(stepInfo, PlayerInputs[i].m_surfaceInfo);

		// Apply the character state machine
		Players[i].p_CharacterContext->update( PlayerInputs[i], output );

		//Apply the player character controller
		//lock player's z coordinate to 2D movement
		output.m_velocity(2) = 0;
		// Set output velocity from state machine into character rigid body
		Players[i].p_CharacterBody->setLinearVelocity(output.m_velocity, dT);

		if(PlayerInputs[i].m_wantJump == true)
		{
			//reset jump bool to false
			PlayerInputs[i].m_wantJump = false;
		}
	

		//update this player's lockedmovement timer
		if(Players[i].lockTimer > 0)
		{
			Players[i].lockTimer -= dT;
		}
		else//lockTimer has ended
		{
			if(Players[i].lockMovement == true)
			{
				//FlamethrowerEffect::GetEffect()->EnableEffect(false);
				Players[i].lockMovement = false;
			}
		}
	}
}

bool Havok::CheckPlayerSupported( short i, float dt )
{
	hkStepInfo stepInfo;
	stepInfo.m_deltaTime = dt;
	stepInfo.m_invDeltaTime = 1.0f/dt;
	Players[i].p_CharacterBody->checkSupport(stepInfo, PlayerInputs[i].m_surfaceInfo);
	if( PlayerInputs[i].m_surfaceInfo.m_supportedState == hkpSurfaceInfo::UNSUPPORTED )
		return false;
	else
		return true;
}

void Havok::UpdateEnemies(float dT)
{
	//update the enemies' movement info
	for( byte i = 0; i < numEnemies; ++i )
	{
		hkpCharacterOutput output;

		EnemyInputs[i].m_inputLR = Enemies[i].XAccel;
		EnemyInputs[i].m_inputUD = Enemies[i].YAccel;

		EnemyInputs[i].m_atLadder = false;

		EnemyInputs[i].m_up = UP;
		EnemyInputs[i].m_forward.set(1,0,0);

		CurrentAngle = HK_REAL_PI * 0.5f;	

		hkRotation currentRot;
		currentRot.setAxisAngle(UP, CurrentAngle);
		EnemyInputs[i].m_forward.setRotatedDir( currentRot, EnemyInputs[i].m_forward );

		hkStepInfo stepInfo;
		stepInfo.m_deltaTime = dT;
		stepInfo.m_invDeltaTime = 1.0f/dT;

		EnemyInputs[i].m_stepInfo = stepInfo;

		EnemyInputs[i].m_characterGravity.set(0,-16, 0);
		EnemyInputs[i].m_velocity = Enemies[i].p_CharacterBody->getRigidBody()->getLinearVelocity();
		EnemyInputs[i].m_position = Enemies[i].p_CharacterBody->getRigidBody()->getPosition();

		Enemies[i].p_CharacterBody->checkSupport(stepInfo, EnemyInputs[i].m_surfaceInfo);

		// Apply the character state machine
		Enemies[i].p_CharacterContext->update( EnemyInputs[i], output );

		//Apply the player character controller
		//lock player's z coordinate to 2D movement
		output.m_velocity(2) = 0;
		// Set output velocity from state machine into character rigid body
		Enemies[i].p_CharacterBody->setLinearVelocity(output.m_velocity, dT);

		if(EnemyInputs[i].m_wantJump == true)
		{
			//reset jump bool to false
			EnemyInputs[i].m_wantJump = false;
		}
	}
}

hkpWorld* Havok::GetPhysicsWorld(void)
{
	return World;
}

MyContactStateListener* Havok::GetListener(void)
{
	return p_Listener;
}

CharacterData* Havok::GetCharacterData(bool isPlayer, unsigned short i)
{
	if(true == isPlayer)
	{
		return &Players[i];
	}
	else
	{
		return &Enemies[i];
	}
}

void Havok::SetGravity(float g)
{
	World->setGravity(hkVector4(0.0f, g, 0.0f));
}

void Havok::HkVecToD3DXVec(const hkVector4& input, D3DXVECTOR3& output)
{
	output.x = input(0);
	output.y = input(1);
	output.z = input(2);
}

void Havok::D3DXVecToHkVec(const D3DXVECTOR3& input, hkVector4& output)
{
	output(0) = input.x;
	output(1) = input.y;
	output(2) = input.z;
}

void Havok::ShutdownHavok(void)
{
	// Release the visual debugger
	//VisualDebugger->removeReference();

	//remove all ref in world
	World->removeAll();

	p_Listener->removeReference();

	//remove all ref to rigidbodies in vectors
	for(int i=0; i < numPlayers; i++)
	{
		Players[i].p_CharacterBody->removeReference();
		Players[i].p_CharacterBody = HK_NULL;
		Players[i].p_CharacterContext->removeReference();
		Players[i].p_CharacterContext = HK_NULL;
	}

	for(int j=0; j < numEnemies; j++)//MAX_NUM_ENEMIES = 12
	{
		Enemies[j].p_CharacterBody->removeReference();
		Enemies[j].p_CharacterBody = HK_NULL;
		Enemies[j].p_CharacterContext->removeReference();
		Enemies[j].p_CharacterContext = HK_NULL;
	}

	//release the phantoms used by the player
	FiregunPhantom[0]->removeReference();
	FiregunPhantom[1]->removeReference();

	// Release the reference on the world
	World->removeReference();
	World = HK_NULL;

	// Physics Contexts are not reference counted at the base class level by the VDB as
	// they are just interfaces really. So only delete the context after you have
	// finished using the VDB.
	Context->removeReference();

	hkBaseSystem::quit();
	hkMemoryInitUtil::quit();

}
