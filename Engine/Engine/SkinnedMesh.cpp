#include "skinnedMesh.h"
#include <fstream>
#include "DX9Structs.h"

#pragma warning(disable:4996)

#pragma region Bone Hierarchy Loader

class BoneHierarchyLoader: public ID3DXAllocateHierarchy
{
	public:
		STDMETHOD(CreateFrame)(THIS_ LPCSTR Name, LPD3DXFRAME *ppNewFrame);
		STDMETHOD(CreateMeshContainer)(THIS_ LPCTSTR Name, CONST D3DXMESHDATA * pMeshData, CONST D3DXMATERIAL * pMaterials, CONST D3DXEFFECTINSTANCE * pEffectInstances, DWORD NumMaterials, CONST DWORD * pAdjacency, LPD3DXSKININFO pSkinInfo, LPD3DXMESHCONTAINER * ppNewMeshContainer);
		STDMETHOD(DestroyFrame)(THIS_ LPD3DXFRAME pFrameToFree);
		STDMETHOD(DestroyMeshContainer)(THIS_ LPD3DXMESHCONTAINER pMeshContainerBase);
};

HRESULT BoneHierarchyLoader::CreateFrame(LPCSTR Name, LPD3DXFRAME *ppNewFrame)
{
	Bone *newBone = new Bone;
	memset(newBone, 0, sizeof(Bone));

	//Copy name
	if(Name != NULL)
	{
		newBone->Name = new char[strlen(Name)+1];
		strcpy(newBone->Name, Name);
	}else
	{
		newBone->Name = 0;
	}

	//Set the transformation matrices
	D3DXMatrixIdentity(&newBone->TransformationMatrix);
	D3DXMatrixIdentity(&newBone->CombinedTransformationMatrix);

	//Return the new bone...
	*ppNewFrame = (D3DXFRAME*)newBone;

	return S_OK;
}

HRESULT BoneHierarchyLoader::CreateMeshContainer(LPCSTR Name,
											CONST D3DXMESHDATA *pMeshData,
											CONST D3DXMATERIAL *pMaterials,
											CONST D3DXEFFECTINSTANCE *pEffectInstances,
											DWORD NumMaterials,
											CONST DWORD *pAdjacency,
											LPD3DXSKININFO pSkinInfo,
											LPD3DXMESHCONTAINER *ppNewMeshContainer)
{
	//Create new Bone Mesh
	BoneMesh *boneMesh = new BoneMesh;
	memset(boneMesh, 0, sizeof(BoneMesh));

	//Get mesh data
	boneMesh->OriginalMesh = pMeshData->pMesh;
	boneMesh->MeshData.pMesh = pMeshData->pMesh;
	boneMesh->MeshData.Type = pMeshData->Type;
	pMeshData->pMesh->AddRef();		//Add Reference so that the mesh isnt deallocated
	IDirect3DDevice9 *g_pDevice = NULL;	
	pMeshData->pMesh->GetDevice(&g_pDevice);	//Get g_pDevice ptr from mesh

	D3DXComputeNormals( pMeshData->pMesh, 0 );

	//Copy materials and load textures (just like with a static mesh)
	for(int i=0;i<(int)NumMaterials;i++)
	{
		D3DXMATERIAL mtrl;
		memcpy(&mtrl, &pMaterials[i], sizeof(D3DXMATERIAL));
		boneMesh->materials.push_back(mtrl.MatD3D);
		IDirect3DTexture9* newTexture = NULL;

		if(mtrl.pTextureFilename != NULL)
		{
			char textureFname[200];
			strcpy(textureFname, "assets/textures/");
			strcat(textureFname, mtrl.pTextureFilename);

			//Load texture
			if( FAILED( D3DXCreateTextureFromFile(g_pDevice, textureFname, &newTexture) ) )
			{
				char errorMessage[200];
				strcpy( errorMessage, "Error, Could not file asset file: " );
				strcat( errorMessage, textureFname );
				MessageBox( 0, errorMessage, "Error Loading Asset", MB_OK );
				PostQuitMessage(0);
				return E_FAIL;
			}
		}

		boneMesh->textures.push_back(newTexture);
	}

	if(pSkinInfo != NULL)
	{
		//Get Skin Info
		boneMesh->pSkinInfo = pSkinInfo;
		pSkinInfo->AddRef();	//Add reference so that the SkinInfo isnt deallocated

		DWORD maxVertInfluences = 0;
		DWORD numBoneComboEntries = 0;
		ID3DXBuffer* boneComboTable = 0;

		pSkinInfo->ConvertToIndexedBlendedMesh(pMeshData->pMesh, 
												D3DXMESH_MANAGED | D3DXMESH_WRITEONLY,  
												30, 
												0, // ignore adjacency in
												0, // ignore adjacency out
												0, // ignore face remap
												0, // ignore vertex remap
												&maxVertInfluences,
												&numBoneComboEntries, 
												&boneComboTable,
												&boneMesh->MeshData.pMesh);
		pMeshData->pMesh->Release();

		if(boneComboTable != NULL)
			boneComboTable->Release();

		//Get Attribute Table
		boneMesh->MeshData.pMesh->GetAttributeTable(NULL, &boneMesh->NumAttributeGroups);
		boneMesh->attributeTable = new D3DXATTRIBUTERANGE[boneMesh->NumAttributeGroups];
		boneMesh->MeshData.pMesh->GetAttributeTable(boneMesh->attributeTable, NULL);

		//Create bone offset and current matrices
		int NumBones = pSkinInfo->GetNumBones();
		boneMesh->boneOffsetMatrices = new D3DXMATRIX[NumBones];		
		boneMesh->currentBoneMatrices = new D3DXMATRIX[NumBones];

		//Get bone offset matrices
		for(int i=0;i < NumBones;i++)
			boneMesh->boneOffsetMatrices[i] = *(boneMesh->pSkinInfo->GetBoneOffsetMatrix(i));
	}

	//Set ppNewMeshContainer to the newly created boneMesh container
	*ppNewMeshContainer = boneMesh;

	return S_OK;
}

HRESULT BoneHierarchyLoader::DestroyFrame(LPD3DXFRAME pFrameToFree) 
{
	if(pFrameToFree)
	{
		//Free name
		if(pFrameToFree->Name != NULL)
			delete [] pFrameToFree->Name;

		//Free bone
		delete pFrameToFree;
	}
	pFrameToFree = NULL;

    return S_OK; 
}

HRESULT BoneHierarchyLoader::DestroyMeshContainer(LPD3DXMESHCONTAINER pMeshContainerBase)
{
	BoneMesh *boneMesh = (BoneMesh*)pMeshContainerBase;

	//Release textures
	int numTextures = (int)boneMesh->textures.size();

	for(int i=0;i < numTextures;i++)
		if(boneMesh->textures[i] != NULL)
			boneMesh->textures[i]->Release();

	delete[] boneMesh->boneMatrixPtrs;

	if( boneMesh->pSkinInfo )
	{
		delete[] boneMesh->boneOffsetMatrices;
		delete[] boneMesh->currentBoneMatrices;
		delete[] boneMesh->attributeTable;
	}

	//Release mesh data
	if(boneMesh->MeshData.pMesh)boneMesh->MeshData.pMesh->Release();
	if(boneMesh->pSkinInfo)boneMesh->pSkinInfo->Release();
	//if(boneMesh->OriginalMesh)boneMesh->OriginalMesh->Release();
	delete boneMesh;

    return S_OK;
}

#pragma endregion

//////////////////////////////////////////////////////////////////////////////////////////////////
//									SKINNED MESH												//
//////////////////////////////////////////////////////////////////////////////////////////////////

SkinnedMesh::SkinnedMesh()
{
	m_pRootBone = NULL;
	m_pAnimControl = NULL;
	headAngle = 0;
}

SkinnedMesh::~SkinnedMesh()
{
}

void SkinnedMesh::Load(char fileName[], IDirect3DDevice9* m_pD3DDevice )
{
	BoneHierarchyLoader boneHierarchy;

	char prefix[200];
	strcpy(prefix, "assets/models/");
	strcat(prefix, fileName);

	if( FAILED( D3DXLoadMeshHierarchyFromX(prefix, D3DXMESH_MANAGED, 
							   m_pD3DDevice, &boneHierarchy,
							   NULL, &m_pRootBone, &m_pAnimControl) ) )
	{
		char errorMessage[200];
		strcpy( errorMessage, "Failed to load asset: ");
		strcat( errorMessage, prefix );
		MessageBox( 0, errorMessage, "Error loading asset", MB_OK );
		PostQuitMessage(0);
		return;
	}

	

	cloned = false;

	SetupBoneMatrixPointers((Bone*)m_pRootBone);

	//Update all the bones
	D3DXMATRIX i;
	D3DXMatrixIdentity(&i);
	UpdateMatrices((Bone*)m_pRootBone, &i);



	CreateBoneList( (Bone*)m_pRootBone );
}

void SkinnedMesh::UpdateMatrices(Bone* bone, D3DXMATRIX *parentMatrix)
{
	if(bone == NULL)return;

	D3DXMatrixMultiply(&bone->CombinedTransformationMatrix,
					   &bone->TransformationMatrix,
					   parentMatrix);

	if(bone->pFrameSibling)UpdateMatrices((Bone*)bone->pFrameSibling, parentMatrix);
	if(bone->pFrameFirstChild)UpdateMatrices((Bone*)bone->pFrameFirstChild, &bone->CombinedTransformationMatrix);
}

void SkinnedMesh::Render(Bone *bone, ID3DXEffect* m_pAnimEffect, 
						 ID3DXEffect* m_pStaticEffect, D3DXMATRIX &viewProj, IDirect3DDevice9* d3dDev)
{
	if(bone == NULL)
	{
		bone = (Bone*)m_pRootBone;
	}

	//If there is a mesh to render...
	if(bone->pMeshContainer != NULL)
	{
		BoneMesh *boneMesh = (BoneMesh*)bone->pMeshContainer;

		if (boneMesh->pSkinInfo != NULL)
		{

			// set up bone transforms
			int numBones = boneMesh->pSkinInfo->GetNumBones();
			for(int i=0;i < numBones;i++)
			{
				D3DXMatrixMultiply(&boneMesh->currentBoneMatrices[i],
								   &boneMesh->boneOffsetMatrices[i], 
								   boneMesh->boneMatrixPtrs[i]);
			}

			m_pAnimEffect->SetMatrixArray("gFinalXForms", boneMesh->currentBoneMatrices, boneMesh->pSkinInfo->GetNumBones());
			D3DXMATRIX identity;
			D3DXMatrixIdentity( &identity );
			D3DXMATRIX witmat;
			D3DXMatrixInverse( &witmat, 0, &identity );
			D3DXMatrixTranspose( &witmat, &witmat );
			m_pAnimEffect->SetMatrix("worldMat", &identity);
			m_pAnimEffect->SetMatrix("worldInverseTransposeMat", &witmat );
			//m_pAnimEffect->SetMatrix("worldViewProjMat", &(identity*viewProj) );
			DWORD vertInflueces = 0;
			boneMesh->pSkinInfo->GetMaxVertexInfluences(&vertInflueces);
			m_pAnimEffect->SetInt( "NumVertInfluences", vertInflueces );
			//Render the mesh
			for(int i=0;i < (int)boneMesh->NumAttributeGroups;i++)
			{
				int mtrlIndex = boneMesh->attributeTable[i].AttribId;

				m_pAnimEffect->SetFloatArray("ambientMaterial", (float*)&boneMesh->materials[mtrlIndex].Ambient, 4);
				m_pAnimEffect->SetFloatArray("diffuseMaterial", (float*)&boneMesh->materials[mtrlIndex].Diffuse, 4);
				m_pAnimEffect->SetFloatArray("specularMaterial", (float*)&boneMesh->materials[mtrlIndex].Specular, 4);
				m_pAnimEffect->SetFloat("specularPower", boneMesh->materials[mtrlIndex].Power );

				m_pAnimEffect->SetTexture("tex", boneMesh->textures[mtrlIndex]);

				//m_pAnimEffect->SetTechnique("tech0");
				UINT numPasses = 0;
				m_pAnimEffect->Begin(&numPasses, NULL);
				for( short i = 0; i < (short)numPasses; ++i )
				{
					m_pAnimEffect->BeginPass(i);
					m_pAnimEffect->CommitChanges();

					boneMesh->MeshData.pMesh->DrawSubset(mtrlIndex);

					m_pAnimEffect->EndPass();
				}
				m_pAnimEffect->End();
			}
		}
		else
		{
			d3dDev->SetVertexDeclaration( DefaultVertex::decl );

			//Normal Static Mesh
			D3DXMATRIX witmat;
			D3DXMatrixInverse( &witmat, 0, &bone->CombinedTransformationMatrix );
			D3DXMatrixTranspose( &witmat, &witmat );
			m_pStaticEffect->SetMatrix("worldMat", &bone->CombinedTransformationMatrix);
			m_pStaticEffect->SetMatrix("WITmat", &witmat);
			m_pStaticEffect->SetMatrix("WVPmat", &(bone->CombinedTransformationMatrix * viewProj) );

			//m_pStaticEffect->SetTechnique("unlit");

			//Render the mesh
			int numMaterials = (int)boneMesh->materials.size();

			for(int i=0;i < numMaterials;i++)
			{
				m_pStaticEffect->SetFloatArray("ambient", (float*)&boneMesh->materials[i].Ambient, 4);
				m_pStaticEffect->SetFloatArray("diffuse", (float*)&boneMesh->materials[i].Diffuse, 4);
				m_pStaticEffect->SetFloatArray("specular", (float*)&boneMesh->materials[i].Specular, 4);
				m_pStaticEffect->SetFloat( "specularPower", boneMesh->materials[i].Power );

				m_pStaticEffect->SetTexture("tex", boneMesh->textures[i]);

				UINT numPasses = 0;
				m_pStaticEffect->Begin(&numPasses, NULL);
				for( short i = 0; i < (short)numPasses; ++i )
				{
					m_pStaticEffect->BeginPass(i);
					m_pStaticEffect->CommitChanges();

					boneMesh->OriginalMesh->DrawSubset(i);

					m_pStaticEffect->EndPass();
				}
				m_pStaticEffect->End();
			}
		}
	}

	if(bone->pFrameSibling != NULL)Render((Bone*)bone->pFrameSibling, m_pAnimEffect, m_pStaticEffect, viewProj, d3dDev);
	if(bone->pFrameFirstChild != NULL)Render((Bone*)bone->pFrameFirstChild, m_pAnimEffect, m_pStaticEffect, viewProj, d3dDev);
}


void SkinnedMesh::SetupBoneMatrixPointers(Bone *bone)
{
	if(bone->pMeshContainer != NULL)
	{
		BoneMesh *boneMesh = (BoneMesh*)bone->pMeshContainer;

		if(boneMesh->pSkinInfo != NULL)
		{
			int NumBones = boneMesh->pSkinInfo->GetNumBones();
			boneMesh->boneMatrixPtrs = new D3DXMATRIX*[NumBones];

			for(int i=0;i < NumBones;i++)
			{
				Bone *b = (Bone*)D3DXFrameFind(m_pRootBone, boneMesh->pSkinInfo->GetBoneName(i));
				if(b != NULL)boneMesh->boneMatrixPtrs[i] = &b->CombinedTransformationMatrix;
				else boneMesh->boneMatrixPtrs[i] = NULL;
			}
		}
	}

	if(bone->pFrameSibling != NULL)SetupBoneMatrixPointers((Bone*)bone->pFrameSibling);
	if(bone->pFrameFirstChild != NULL)SetupBoneMatrixPointers((Bone*)bone->pFrameFirstChild);
}

void SkinnedMesh::SetPose(D3DXMATRIX world, float time)
{
	//m_pAnimControl->AdvanceTime(time, NULL);

	/*if( GetAsyncKeyState( VK_UP ) )
	headAngle -= 5.0f*time;
	if( GetAsyncKeyState( VK_DOWN ) )
	headAngle += 5.0f*time;*/

	//D3DXMATRIX w;
	//D3DXMatrixRotationY( &w, headAngle );
	//LPD3DXFRAME b = D3DXFrameFind( m_pRootBone, "Bip01_Head" );
	//D3DXMatrixMultiply( &b->TransformationMatrix, &w, &b->TransformationMatrix );

	//UpdateMatrices((Bone*)m_pRootBone, &world);
}

void SkinnedMesh::SetAnimation(string name)
{
	ID3DXAnimationSet *anim = NULL;

	int numAnims = (int)m_pAnimControl->GetMaxNumAnimationSets();

	for(int i=0;i<numAnims;i++)
	{
		anim = NULL;
		m_pAnimControl->GetAnimationSet(i, &anim);

		if(anim != NULL)
		{
			if(strcmp(name.c_str(), anim->GetName()) == 0)
				m_pAnimControl->SetTrackAnimationSet(0, anim);
			anim->Release();
		}
	}
}

void SkinnedMesh::GetAnimations(vector<string> &animations)
{
	ID3DXAnimationSet *anim = NULL;

	for(int i=0;i<(int)m_pAnimControl->GetMaxNumAnimationSets();i++)
	{
		anim = NULL;
		m_pAnimControl->GetAnimationSet(i, &anim);

		if(anim != NULL)
		{
			animations.push_back(anim->GetName());
			anim->Release();
		}
	}
}

void SkinnedMesh::Shutdown()
{
	if( !cloned )
	{
		BoneHierarchyLoader boneHierarchy;
		BoneMesh* boneMesh = (BoneMesh*)m_pRootBone->pMeshContainer;
		ReleaseMeshContainer( (Bone*)m_pRootBone );
		m_pRootBone = 0;
	}
	if(m_pAnimControl)m_pAnimControl->Release();
	m_pAnimControl = 0;

	delete this;
}

void SkinnedMesh::ReleaseMeshContainer( Bone* bone )
{
	if(bone == NULL)
	{
		bone = (Bone*)m_pRootBone;
	}

	BoneHierarchyLoader boneLoader;

	//If there is a mesh to render...
	if(bone->pMeshContainer != NULL)
	{
		BoneMesh *boneMesh = (BoneMesh*)bone->pMeshContainer;
		boneLoader.DestroyMeshContainer( boneMesh );
	}

	if(bone->pFrameSibling != NULL)ReleaseMeshContainer((Bone*)bone->pFrameSibling);
	if(bone->pFrameFirstChild != NULL)ReleaseMeshContainer((Bone*)bone->pFrameFirstChild);
	boneLoader.DestroyFrame( bone );
}

void SkinnedMesh::CreateBoneList( Bone* bone )
{
	if( !bone ) return;

	if( bone->Name )
		if( strlen(bone->Name) > 1 )
			boneNames.push_back(bone->Name);

	CreateBoneList( (Bone*)bone->pFrameSibling );
	CreateBoneList( (Bone*)bone->pFrameFirstChild );
}

void SkinnedMesh::GetBoneList( vector<string> &bones )
{
	for( short i = 0; i < (short)boneNames.size(); ++i )
		bones.push_back(boneNames.at(i));
}

void SkinnedMesh::CopyMesh( SkinnedMesh** newMesh )
{
	if( (*newMesh) )
		return;

	SkinnedMesh* tMesh = new SkinnedMesh;
	tMesh->m_pRootBone = this->m_pRootBone;

	if( this->m_pAnimControl )
	{
		this->m_pAnimControl->CloneAnimationController(
			this->m_pAnimControl->GetMaxNumAnimationOutputs(),
			this->m_pAnimControl->GetMaxNumAnimationSets(),
			this->m_pAnimControl->GetMaxNumTracks(),
			this->m_pAnimControl->GetMaxNumEvents(),
			&tMesh->m_pAnimControl );
	}

	tMesh->headAngle = 0.0f;
	tMesh->cloned = true;

	(*newMesh) = tMesh;
}