#pragma once

#include <Windows.h>
#pragma comment (lib, "winmm.lib")

class GameTimer
{
private:
	static GameTimer* timerObj;
	GameTimer();
	GameTimer(const GameTimer& t){}
	void operator= (const GameTimer& t){}

	short timer, prevTime, timeDiff;

	__int64 currTimeStamp, prevTimeStamp;
	float deltaTime, secsPerCnt;

	float fpsTimer;
	short fps, frameCounter;

public:
	static GameTimer* GetTimer();

	void StartTimer();

	void Update();
	float GetDeltaTime();
	short GetElapsedMilliseconds();
	short GetFPS();

	void Shutdown();

	~GameTimer(void){}
};

