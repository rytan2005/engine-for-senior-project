#include "WeaponStation.h"

WeaponStation* WeaponStation::instance = NULL;
WeaponStation* WeaponStation::GetInstance()
{
	if( !instance )
	{
		return NULL;
	}

	return instance;
}
WeaponStation* WeaponStation::GetInstance(short screenWidth, short screenHeight)
{
	if( !instance )
	{
		instance = new WeaponStation();
		instance->screenW = screenWidth;
		instance->screenH = screenHeight;
		instance->Init();
	}

	return instance;
}

bool WeaponStation::Init()
{
	p_Renderer = DX9Render::GetRenderer();
	p_Input = InputManager::GetInputManager();

	float defWeaponPosVal[2] = {(float)(screenW/2), (float)(screenH/2)};

	WS_BG = 0;
	p_Renderer->Load2DGameComponent( &WS_BG, "Weapon_Switch/WS_BG.jpg" );
	WS_BG->position.x = defWeaponPosVal[0];
	WS_BG->position.y = defWeaponPosVal[1];

	for(short i=0; i<14; ++i)
	{
		weaponSwitchNormal[i] = 0;
		weaponSwitchSelected[i] = 0;
		weaponSwitchHighlighted[i] = 0;
	}

	for(short i=0; i<4; ++i)
	{
		weaponSwitchPlayerOne[i] = 0;
		weaponSwitchPlayerTwo[i] = 0;
	}

	for(short i=0; i<2; ++i)
	{
		weaponSwitchReadyNormal[i] = 0;
		weaponSwitchReadySelected[i] = 0;
	}
	// Weapon Switch - Normal State Initialization
	p_Renderer->Load2DGameComponent( &weaponSwitchNormal[0], "Weapon_Switch/Normal/WS_PlayerOne_Pistol.png" );
	p_Renderer->Load2DGameComponent( &weaponSwitchNormal[1], "Weapon_Switch/Normal/WS_PlayerOne_Assault.png" );
	p_Renderer->Load2DGameComponent( &weaponSwitchNormal[2], "Weapon_Switch/Normal/WS_PlayerOne_Shotgun.png" );
	p_Renderer->Load2DGameComponent( &weaponSwitchNormal[3], "Weapon_Switch/Normal/WS_PlayerOne_Fire.png" );
	p_Renderer->Load2DGameComponent( &weaponSwitchNormal[4], "Weapon_Switch/Normal/WS_PlayerOne_Freeze.png" );
	p_Renderer->Load2DGameComponent( &weaponSwitchNormal[5], "Weapon_Switch/Normal/WS_PlayerOne_Acid.png" );
	p_Renderer->Load2DGameComponent( &weaponSwitchNormal[6], "Weapon_Switch/WS_PlayerOne_Blackhole.png" );
	p_Renderer->Load2DGameComponent( &weaponSwitchNormal[7], "Weapon_Switch/Normal/WS_PlayerTwo_Pistol.png" );
	p_Renderer->Load2DGameComponent( &weaponSwitchNormal[8], "Weapon_Switch/Normal/WS_PlayerTwo_Assault.png" );
	p_Renderer->Load2DGameComponent( &weaponSwitchNormal[9], "Weapon_Switch/Normal/WS_PlayerTwo_Shotgun.png" );
	p_Renderer->Load2DGameComponent( &weaponSwitchNormal[10], "Weapon_Switch/Normal/WS_PlayerTwo_Fire.png" );
	p_Renderer->Load2DGameComponent( &weaponSwitchNormal[11], "Weapon_Switch/Normal/WS_PlayerTwo_Freeze.png" );
	p_Renderer->Load2DGameComponent( &weaponSwitchNormal[12], "Weapon_Switch/Normal/WS_PlayerTwo_Acid.png" );
	p_Renderer->Load2DGameComponent( &weaponSwitchNormal[13], "Weapon_Switch/WS_PlayerTwo_Blackhole.png" );

	// Weapon Switch Graphics - Selected State Initialization
	p_Renderer->Load2DGameComponent( &weaponSwitchSelected[0], "Weapon_Switch/Selected/WS_PlayerOne_Pistol_Selected.png" );
	p_Renderer->Load2DGameComponent( &weaponSwitchSelected[1], "Weapon_Switch/Selected/WS_PlayerOne_Assault_Selected.png" );
	p_Renderer->Load2DGameComponent( &weaponSwitchSelected[2], "Weapon_Switch/Selected/WS_PlayerOne_Shotgun_Selected.png" );
	p_Renderer->Load2DGameComponent( &weaponSwitchSelected[3], "Weapon_Switch/Selected/WS_PlayerOne_Fire_Selected.png" );
	p_Renderer->Load2DGameComponent( &weaponSwitchSelected[4], "Weapon_Switch/Selected/WS_PlayerOne_Freeze_Selected.png" );
	p_Renderer->Load2DGameComponent( &weaponSwitchSelected[5], "Weapon_Switch/Selected/WS_PlayerOne_Acid_Selected.png" );
	p_Renderer->Load2DGameComponent( &weaponSwitchSelected[6], "Weapon_Switch/WS_PlayerOne_Blackhole.png" );
	p_Renderer->Load2DGameComponent( &weaponSwitchSelected[7], "Weapon_Switch/Selected/WS_PlayerTwo_Pistol_Selected.png" );
	p_Renderer->Load2DGameComponent( &weaponSwitchSelected[8], "Weapon_Switch/Selected/WS_PlayerTwo_Assault_Selected.png" );
	p_Renderer->Load2DGameComponent( &weaponSwitchSelected[9], "Weapon_Switch/Selected/WS_PlayerTwo_Shotgun_Selected.png" );
	p_Renderer->Load2DGameComponent( &weaponSwitchSelected[10], "Weapon_Switch/Selected/WS_PlayerTwo_Fire_Selected.png" );
	p_Renderer->Load2DGameComponent( &weaponSwitchSelected[11], "Weapon_Switch/Selected/WS_PlayerTwo_Freeze_Selected.png" );
	p_Renderer->Load2DGameComponent( &weaponSwitchSelected[12], "Weapon_Switch/Selected/WS_PlayerTwo_Acid_Selected.png" );
	p_Renderer->Load2DGameComponent( &weaponSwitchSelected[13], "Weapon_Switch/WS_PlayerTwo_Blackhole.png" );

	// Weapon Switch - Highlighted State Initialization
	p_Renderer->Load2DGameComponent( &weaponSwitchHighlighted[0], "Weapon_Switch/Highlighted/WS_PlayerOne_Pistol_Highlighted.png" );
	p_Renderer->Load2DGameComponent( &weaponSwitchHighlighted[1], "Weapon_Switch/Highlighted/WS_PlayerOne_Assault_Highlighted.png" );
	p_Renderer->Load2DGameComponent( &weaponSwitchHighlighted[2], "Weapon_Switch/Highlighted/WS_PlayerOne_Shotgun_Highlighted.png" );
	p_Renderer->Load2DGameComponent( &weaponSwitchHighlighted[3], "Weapon_Switch/Highlighted/WS_PlayerOne_Fire_Highlighted.png" );
	p_Renderer->Load2DGameComponent( &weaponSwitchHighlighted[4], "Weapon_Switch/Highlighted/WS_PlayerOne_Freeze_Highlighted.png" );
	p_Renderer->Load2DGameComponent( &weaponSwitchHighlighted[5], "Weapon_Switch/Highlighted/WS_PlayerOne_Acid_Highlighted.png" );
	p_Renderer->Load2DGameComponent( &weaponSwitchHighlighted[6], "Weapon_Switch/Highlighted/WS_PlayerOne_Blackhole_Highlighted.png" );
	p_Renderer->Load2DGameComponent( &weaponSwitchHighlighted[7], "Weapon_Switch/Highlighted/WS_PlayerTwo_Pistol_Highlighted.png" );
	p_Renderer->Load2DGameComponent( &weaponSwitchHighlighted[8], "Weapon_Switch/Highlighted/WS_PlayerTwo_Assault_Highlighted.png" );
	p_Renderer->Load2DGameComponent( &weaponSwitchHighlighted[9], "Weapon_Switch/Highlighted/WS_PlayerTwo_Shotgun_Highlighted.png" );
	p_Renderer->Load2DGameComponent( &weaponSwitchHighlighted[10], "Weapon_Switch/Highlighted/WS_PlayerTwo_Fire_Highlighted.png" );
	p_Renderer->Load2DGameComponent( &weaponSwitchHighlighted[11], "Weapon_Switch/Highlighted/WS_PlayerTwo_Freeze_Highlighted.png" );
	p_Renderer->Load2DGameComponent( &weaponSwitchHighlighted[12], "Weapon_Switch/Highlighted/WS_PlayerTwo_Acid_Highlighted.png" );
	p_Renderer->Load2DGameComponent( &weaponSwitchHighlighted[13], "Weapon_Switch/Highlighted/WS_PlayerTwo_Blackhole_Highlighted.png" );

	p_Renderer->Load2DGameComponent( &weaponSwitchReadyNormal[0], "Weapon_Switch/Normal/WS_PlayerOne_Ready.png" );
	p_Renderer->Load2DGameComponent( &weaponSwitchReadyNormal[1], "Weapon_Switch/Normal/WS_PlayerTwo_Ready.png" );
	p_Renderer->Load2DGameComponent( &weaponSwitchReadySelected[0], "Weapon_Switch/Selected/WS_PlayerOne_Ready_Selected.png" );
	p_Renderer->Load2DGameComponent( &weaponSwitchReadySelected[1], "Weapon_Switch/Selected/WS_PlayerTwo_Ready_Selected.png" );


	for(short i = 0; i < 14; ++i)
	{
		weaponSwitchNormal[i]->position.x = defWeaponPosVal[0];
		weaponSwitchNormal[i]->position.y = defWeaponPosVal[1];

		weaponSwitchSelected[i]->position.x = defWeaponPosVal[0];
		weaponSwitchSelected[i]->position.y = defWeaponPosVal[1];

		weaponSwitchHighlighted[i]->position.x = defWeaponPosVal[0];
		weaponSwitchHighlighted[i]->position.y = defWeaponPosVal[1];
	}

	for(short i = 0; i < 2; ++i)
	{
		weaponSwitchReadyNormal[i]->position.x = defWeaponPosVal[0];
		weaponSwitchReadyNormal[i]->position.y = defWeaponPosVal[1];

		weaponSwitchReadySelected[i]->position.x = defWeaponPosVal[0];
		weaponSwitchReadySelected[i]->position.y = defWeaponPosVal[1];
	}



	weaponSwitchPlayerOne[2] = weaponSwitchHighlighted[0];
	weaponSwitchPlayerTwo[2] = weaponSwitchHighlighted[7];
	weaponSwitchPlayerOne[3] = weaponSwitchReadyNormal[0];
	weaponSwitchPlayerTwo[3] = weaponSwitchReadyNormal[1];

	errorSound = SoundLoader::GetInstance()->Load(false,false,"assets\\soundfx\\Error.mp3");

	isReady[0] = false;
	isReady[1] = false;

	return true;
}

void WeaponStation::Update()
{
	if(weaponSwitchPlayerOne[3] == weaponSwitchReadySelected[0] && weaponSwitchPlayerTwo[3] == weaponSwitchReadySelected[1])
		{
			UserInterface::GetInstance()->SetState(IN_GAME);
			weaponSwitchPlayerOne[3] = weaponSwitchReadyNormal[0];
			weaponSwitchPlayerTwo[3] = weaponSwitchReadyNormal[1];

			isReady[0] = false;
			isReady[1] = false;
		}

		SetWeaponStatuses();
		DoKeyboardInput();
		DoXboxInput();
}

void WeaponStation::RenderStation()
{
 	p_Renderer->Render2DComponent( &WS_BG );

	for(short i = 0; i < 14; ++i)
		p_Renderer->Render2DComponent( &weaponSwitchNormal[i] );
	for(short i = 2; i < 4; ++i)
	{
		p_Renderer->Render2DComponent( &weaponSwitchPlayerOne[i] );
		p_Renderer->Render2DComponent( &weaponSwitchPlayerTwo[i] );
	}

	p_Renderer->Render2DComponent( &weaponSwitchPlayerOne[0] );
	p_Renderer->Render2DComponent( &weaponSwitchPlayerTwo[0] );

	p_Renderer->Render2DComponent( &weaponSwitchPlayerOne[1] );
	p_Renderer->Render2DComponent( &weaponSwitchPlayerTwo[1] );
}

void WeaponStation::Shutdown()
{
	if(instance)
	{
	WS_BG->Shutdown();
	WS_BG = 0;

	for(short i = 0; i < 14; ++i)
	{
		weaponSwitchNormal[i]->Shutdown();
		weaponSwitchNormal[i] = 0;

		weaponSwitchSelected[i]->Shutdown();
		weaponSwitchSelected[i] = 0;

		weaponSwitchHighlighted[i]->Shutdown();
		weaponSwitchHighlighted[i] = 0;
	}

	for(short i = 0; i < 2; ++i)
	{
		weaponSwitchReadyNormal[i]->Shutdown();
		weaponSwitchReadyNormal[i] = 0;

		weaponSwitchReadySelected[i]->Shutdown();
		weaponSwitchReadySelected[i] = 0;
	}

	delete instance;
	}

	instance = NULL;
}

void WeaponStation::SetWeaponStatuses()
{
	GUNTYPE temp[2] = {PISTOL, FIREGUN};
	for(short i=0; i<2; i++)
	{
		for(short j=0; j<2; j++)
		{
			if(i == 0 )
				temp[j] = EntityManager::GetEntityManager()->EntityList[0]->getWeapons()[j];
			else
				temp[j] = EntityManager::GetEntityManager()->EntityList[1]->getWeapons()[j];

			switch(temp[j])
			{
			case PISTOL:
				if(i == 0)
					weaponSwitchPlayerOne[j] = weaponSwitchSelected[0];
				else
					weaponSwitchPlayerTwo[j] = weaponSwitchSelected[7];
				break;
			case ASSAULT:
				if(i == 0)
					weaponSwitchPlayerOne[j] = weaponSwitchSelected[1];
				else
					weaponSwitchPlayerTwo[j] = weaponSwitchSelected[8];
				break;
			case SHOTGUN:
				if(i == 0)
					weaponSwitchPlayerOne[j] = weaponSwitchSelected[2];
				else
					weaponSwitchPlayerTwo[j] = weaponSwitchSelected[9];
				break;
			case FIREGUN:
				if(i == 0)
					weaponSwitchPlayerOne[j] = weaponSwitchSelected[3];
				else
					weaponSwitchPlayerTwo[j] = weaponSwitchSelected[10];
				break;
			case FREEZEGUN:
				if(i == 0)
					weaponSwitchPlayerOne[j] = weaponSwitchSelected[4];
				else
					weaponSwitchPlayerTwo[j] = weaponSwitchSelected[11];
				break;
			case ACIDGUN:
				if(i == 0)
					weaponSwitchPlayerOne[j] = weaponSwitchSelected[5];
				else
					weaponSwitchPlayerTwo[j] = weaponSwitchSelected[12];
				break;
			}
		}
	}
}

void WeaponStation::DoKeyboardInput()
{
	GameComponent2D* temp = 0;
	short tempValue = 0;

	if(p_Input->CheckKeyboardBufferedKey(DIK_Q))
	{
		if(weaponSwitchPlayerOne[3] == weaponSwitchReadyNormal[0])
		{
			weaponSwitchPlayerOne[3] = weaponSwitchReadySelected[0];
			isReady[0] = true;
		}
		else
		{
			weaponSwitchPlayerOne[3] = weaponSwitchReadyNormal[0];
			isReady[0] = false;
		}
	}

	if(!isReady[0])
	{
		if(p_Input->CheckKeyboardBufferedKey(DIK_S))
		{
			if(weaponSwitchPlayerOne[2] == weaponSwitchHighlighted[0])
				temp = weaponSwitchHighlighted[1];
			else if(weaponSwitchPlayerOne[2] == weaponSwitchHighlighted[1])
				temp = weaponSwitchHighlighted[2];
			else if(weaponSwitchPlayerOne[2] == weaponSwitchHighlighted[2])
				temp = weaponSwitchHighlighted[0];
			else if(weaponSwitchPlayerOne[2] == weaponSwitchHighlighted[3])
				temp = weaponSwitchHighlighted[4];
			else if(weaponSwitchPlayerOne[2] == weaponSwitchHighlighted[4])
				temp = weaponSwitchHighlighted[5];
			else if(weaponSwitchPlayerOne[2] == weaponSwitchHighlighted[5])
				temp = weaponSwitchHighlighted[6];
			else if(weaponSwitchPlayerOne[2] == weaponSwitchHighlighted[6])
				temp = weaponSwitchHighlighted[3];

			weaponSwitchPlayerOne[2] = temp;
		}

		if(p_Input->CheckKeyboardBufferedKey(DIK_W))
		{
			if(weaponSwitchPlayerOne[2] == weaponSwitchHighlighted[0])
				temp = weaponSwitchHighlighted[2];
			else if(weaponSwitchPlayerOne[2] == weaponSwitchHighlighted[1])
				temp = weaponSwitchHighlighted[0];
			else if(weaponSwitchPlayerOne[2] == weaponSwitchHighlighted[2])
				temp = weaponSwitchHighlighted[1];
			else if(weaponSwitchPlayerOne[2] == weaponSwitchHighlighted[3])
				temp = weaponSwitchHighlighted[6];
			else if(weaponSwitchPlayerOne[2] == weaponSwitchHighlighted[4])
				temp = weaponSwitchHighlighted[3];
			else if(weaponSwitchPlayerOne[2] == weaponSwitchHighlighted[5])
				temp = weaponSwitchHighlighted[4];
			else if(weaponSwitchPlayerOne[2] == weaponSwitchHighlighted[6])
				temp = weaponSwitchHighlighted[5];

			weaponSwitchPlayerOne[2] = temp;
		}

		if(p_Input->CheckKeyboardBufferedKey(DIK_A) || p_Input->CheckKeyboardBufferedKey(DIK_D))
		{
			if(weaponSwitchPlayerOne[2] == weaponSwitchHighlighted[0])
				temp = weaponSwitchHighlighted[3];
			else if(weaponSwitchPlayerOne[2] == weaponSwitchHighlighted[1])
				temp = weaponSwitchHighlighted[4];
			else if(weaponSwitchPlayerOne[2] == weaponSwitchHighlighted[2])
				temp = weaponSwitchHighlighted[5];
			else if(weaponSwitchPlayerOne[2] == weaponSwitchHighlighted[3])
				temp = weaponSwitchHighlighted[0];
			else if(weaponSwitchPlayerOne[2] == weaponSwitchHighlighted[4])
				temp = weaponSwitchHighlighted[1];
			else if(weaponSwitchPlayerOne[2] == weaponSwitchHighlighted[5])
				temp = weaponSwitchHighlighted[2];
			else if(weaponSwitchPlayerOne[2] == weaponSwitchHighlighted[6])
				temp = weaponSwitchHighlighted[2];

			weaponSwitchPlayerOne[2] = temp;
		}

		if(p_Input->CheckKeyboardBufferedKey(DIK_SPACE))
		{
			for(short i=0; i<7; ++i)
			{
				if(weaponSwitchPlayerOne[2] == weaponSwitchHighlighted[i])
				{
					temp = weaponSwitchSelected[i];
					tempValue = i;
				}
			}


			if(temp == weaponSwitchSelected[0]
			|| temp == weaponSwitchSelected[1]
			|| temp == weaponSwitchSelected[2])
				weaponSwitchPlayerOne[0] = temp;
			else if(temp == weaponSwitchSelected[3]
			|| temp == weaponSwitchSelected[4]
			|| temp == weaponSwitchSelected[5])
				weaponSwitchPlayerOne[1] = temp;
			else if(temp == weaponSwitchSelected[6])
				AudioManager::GetInstance()->PlaySFX(*errorSound);

			if(temp != weaponSwitchSelected[6])
				EntityManager::GetEntityManager()->EntityList[0]->SetWeaponTo((GUNTYPE)tempValue);
		}
	}


	// Player Two Input
	if(p_Input->CheckKeyboardBufferedKey(DIK_DECIMAL))
	{

		if(weaponSwitchPlayerTwo[3] == weaponSwitchReadyNormal[1])
		{
			weaponSwitchPlayerTwo[3] = weaponSwitchReadySelected[1];
			isReady[1] = true;
		}
		else
		{
			weaponSwitchPlayerTwo[3] = weaponSwitchReadyNormal[1];
			isReady[1] = false;
		}
	}

	if(!isReady[1])
	{
		if(p_Input->CheckKeyboardBufferedKey(DIK_DOWNARROW))
		{
			if(weaponSwitchPlayerTwo[2] == weaponSwitchHighlighted[7])
				temp = weaponSwitchHighlighted[8];
			else if(weaponSwitchPlayerTwo[2] == weaponSwitchHighlighted[8])
				temp = weaponSwitchHighlighted[9];
			else if(weaponSwitchPlayerTwo[2] == weaponSwitchHighlighted[9])
				temp = weaponSwitchHighlighted[7];
			else if(weaponSwitchPlayerTwo[2] == weaponSwitchHighlighted[10])
				temp = weaponSwitchHighlighted[11];
			else if(weaponSwitchPlayerTwo[2] == weaponSwitchHighlighted[11])
				temp = weaponSwitchHighlighted[12];
			else if(weaponSwitchPlayerTwo[2] == weaponSwitchHighlighted[12])
				temp = weaponSwitchHighlighted[13];
			else if(weaponSwitchPlayerTwo[2] == weaponSwitchHighlighted[13])
				temp = weaponSwitchHighlighted[10];

			weaponSwitchPlayerTwo[2] = temp;
		}

		if(p_Input->CheckKeyboardBufferedKey(DIK_UPARROW))
		{
			if(weaponSwitchPlayerTwo[2] == weaponSwitchHighlighted[7])
				temp = weaponSwitchHighlighted[9];
			else if(weaponSwitchPlayerTwo[2] == weaponSwitchHighlighted[8])
				temp = weaponSwitchHighlighted[7];
			else if(weaponSwitchPlayerTwo[2] == weaponSwitchHighlighted[9])
				temp = weaponSwitchHighlighted[8];
			else if(weaponSwitchPlayerTwo[2] == weaponSwitchHighlighted[10])
				temp = weaponSwitchHighlighted[13];
			else if(weaponSwitchPlayerTwo[2] == weaponSwitchHighlighted[11])
				temp = weaponSwitchHighlighted[10];
			else if(weaponSwitchPlayerTwo[2] == weaponSwitchHighlighted[12])
				temp = weaponSwitchHighlighted[11];
			else if(weaponSwitchPlayerTwo[2] == weaponSwitchHighlighted[13])
				temp = weaponSwitchHighlighted[12];

			weaponSwitchPlayerTwo[2] = temp;
		}

		if(p_Input->CheckKeyboardBufferedKey(DIK_LEFTARROW) || p_Input->CheckKeyboardBufferedKey(DIK_RIGHTARROW))
		{
			if(weaponSwitchPlayerTwo[2] == weaponSwitchHighlighted[7])
				temp = weaponSwitchHighlighted[10];
			else if(weaponSwitchPlayerTwo[2] == weaponSwitchHighlighted[8])
				temp = weaponSwitchHighlighted[11];
			else if(weaponSwitchPlayerTwo[2] == weaponSwitchHighlighted[9])
				temp = weaponSwitchHighlighted[12];
			else if(weaponSwitchPlayerTwo[2] == weaponSwitchHighlighted[10])
				temp = weaponSwitchHighlighted[7];
			else if(weaponSwitchPlayerTwo[2] == weaponSwitchHighlighted[11])
				temp = weaponSwitchHighlighted[8];
			else if(weaponSwitchPlayerTwo[2] == weaponSwitchHighlighted[12])
				temp = weaponSwitchHighlighted[9];
			else if(weaponSwitchPlayerTwo[2] == weaponSwitchHighlighted[13])
				temp = weaponSwitchHighlighted[9];

			weaponSwitchPlayerTwo[2] = temp;
		}

		if(p_Input->CheckKeyboardBufferedKey(DIK_NUMPAD0))
		{
			for(short i=7; i<14; ++i)
			{
				if(weaponSwitchPlayerTwo[2] == weaponSwitchHighlighted[i])
				{
					temp = weaponSwitchSelected[i];
					tempValue = i;
				}
			}


			if(temp == weaponSwitchSelected[7]
			|| temp == weaponSwitchSelected[8]
			|| temp == weaponSwitchSelected[9])
				weaponSwitchPlayerTwo[0] = temp;
			else if(temp == weaponSwitchSelected[10]
			|| temp == weaponSwitchSelected[11]
			|| temp == weaponSwitchSelected[12])
				weaponSwitchPlayerTwo[1] = temp;
			else if(temp == weaponSwitchSelected[13])
				AudioManager::GetInstance()->PlaySFX(*errorSound);

			if(temp != weaponSwitchSelected[13])
				EntityManager::GetEntityManager()->EntityList[1]->SetWeaponTo((GUNTYPE)(tempValue % 7));
		}
	}
}

void WeaponStation::DoXboxInput()
{
	GameComponent2D* temp = 0;

	short leftStickX[2];
	short leftStickY[2];

	for(short i=0; i<2; ++i)
	{
		leftStickX[i] = InputManager::GetInputManager()->CheckXboxBufferedLStickX(i);
		leftStickY[i] = InputManager::GetInputManager()->CheckXboxBufferedLStickY(i);

		if(p_Input->CheckXboxBufferedButton((BYTE)i, XBUTTON_START))
		{
			if(i == 0)
			{
				if(weaponSwitchPlayerOne[3] == weaponSwitchReadyNormal[0])
				{
					weaponSwitchPlayerOne[3] = weaponSwitchReadySelected[0];
					isReady[0] = true;
				}
				else
				{
					weaponSwitchPlayerOne[3] = weaponSwitchReadyNormal[0];
					isReady[0] = false;
				}
			}
			else
			{
				if(weaponSwitchPlayerTwo[3] == weaponSwitchReadyNormal[1])
				{
					weaponSwitchPlayerTwo[3] = weaponSwitchReadySelected[1];
					isReady[1] = true;
				}
				else
				{
					weaponSwitchPlayerTwo[3] = weaponSwitchReadyNormal[1];
					isReady[1] = false;
				}
			}
		}
		if(!isReady[i])
		{
			//if(p_Input->CheckXboxBufferedDpad((BYTE)i,XDPAD_DOWN))
			if(leftStickY[i] == -1)
			{
				if(i == 0)
				{
					if(weaponSwitchPlayerOne[2] == weaponSwitchHighlighted[0])
						temp = weaponSwitchHighlighted[1];
					else if(weaponSwitchPlayerOne[2] == weaponSwitchHighlighted[1])
						temp = weaponSwitchHighlighted[2];
					else if(weaponSwitchPlayerOne[2] == weaponSwitchHighlighted[2])
						temp = weaponSwitchHighlighted[0];
					else if(weaponSwitchPlayerOne[2] == weaponSwitchHighlighted[3])
						temp = weaponSwitchHighlighted[4];
					else if(weaponSwitchPlayerOne[2] == weaponSwitchHighlighted[4])
						temp = weaponSwitchHighlighted[5];
					else if(weaponSwitchPlayerOne[2] == weaponSwitchHighlighted[5])
						temp = weaponSwitchHighlighted[6];
					else if(weaponSwitchPlayerOne[2] == weaponSwitchHighlighted[6])
						temp = weaponSwitchHighlighted[3];

					weaponSwitchPlayerOne[2] = temp;
				}
				else
				{
					if(weaponSwitchPlayerTwo[2] == weaponSwitchHighlighted[7])
						temp = weaponSwitchHighlighted[8];
					else if(weaponSwitchPlayerTwo[2] == weaponSwitchHighlighted[8])
						temp = weaponSwitchHighlighted[9];
					else if(weaponSwitchPlayerTwo[2] == weaponSwitchHighlighted[9])
						temp = weaponSwitchHighlighted[7];
					else if(weaponSwitchPlayerTwo[2] == weaponSwitchHighlighted[10])
						temp = weaponSwitchHighlighted[11];
					else if(weaponSwitchPlayerTwo[2] == weaponSwitchHighlighted[11])
						temp = weaponSwitchHighlighted[12];
					else if(weaponSwitchPlayerTwo[2] == weaponSwitchHighlighted[12])
						temp = weaponSwitchHighlighted[13];
					else if(weaponSwitchPlayerTwo[2] == weaponSwitchHighlighted[13])
						temp = weaponSwitchHighlighted[10];

					weaponSwitchPlayerTwo[2] = temp;
				}
			}

			//if(p_Input->CheckXboxBufferedDpad((BYTE)i,XDPAD_UP))
			if(leftStickY[i] == 1)
			{
				if(i == 0)
				{
					if(weaponSwitchPlayerOne[2] == weaponSwitchHighlighted[0])
						temp = weaponSwitchHighlighted[2];
					else if(weaponSwitchPlayerOne[2] == weaponSwitchHighlighted[1])
						temp = weaponSwitchHighlighted[0];
					else if(weaponSwitchPlayerOne[2] == weaponSwitchHighlighted[2])
						temp = weaponSwitchHighlighted[1];
					else if(weaponSwitchPlayerOne[2] == weaponSwitchHighlighted[3])
						temp = weaponSwitchHighlighted[6];
					else if(weaponSwitchPlayerOne[2] == weaponSwitchHighlighted[4])
						temp = weaponSwitchHighlighted[3];
					else if(weaponSwitchPlayerOne[2] == weaponSwitchHighlighted[5])
						temp = weaponSwitchHighlighted[4];
					else if(weaponSwitchPlayerOne[2] == weaponSwitchHighlighted[6])
						temp = weaponSwitchHighlighted[5];

					weaponSwitchPlayerOne[2] = temp;
				}
				else
				{
					if(weaponSwitchPlayerTwo[2] == weaponSwitchHighlighted[7])
						temp = weaponSwitchHighlighted[9];
					else if(weaponSwitchPlayerTwo[2] == weaponSwitchHighlighted[8])
						temp = weaponSwitchHighlighted[7];
					else if(weaponSwitchPlayerTwo[2] == weaponSwitchHighlighted[9])
						temp = weaponSwitchHighlighted[8];
					else if(weaponSwitchPlayerTwo[2] == weaponSwitchHighlighted[10])
						temp = weaponSwitchHighlighted[13];
					else if(weaponSwitchPlayerTwo[2] == weaponSwitchHighlighted[11])
						temp = weaponSwitchHighlighted[10];
					else if(weaponSwitchPlayerTwo[2] == weaponSwitchHighlighted[12])
						temp = weaponSwitchHighlighted[11];
					else if(weaponSwitchPlayerTwo[2] == weaponSwitchHighlighted[13])
						temp = weaponSwitchHighlighted[12];

					weaponSwitchPlayerTwo[2] = temp;
				}
			}

			//if(p_Input->CheckXboxBufferedDpad((BYTE)i,XDPAD_LEFT) || p_Input->CheckXboxBufferedDpad((BYTE)i,XDPAD_RIGHT))
			if(leftStickX[i] == -1 || leftStickX[i] == 1)
			{
				if(i == 0)
				{
					if(weaponSwitchPlayerOne[2] == weaponSwitchHighlighted[0])
						temp = weaponSwitchHighlighted[3];
					else if(weaponSwitchPlayerOne[2] == weaponSwitchHighlighted[1])
						temp = weaponSwitchHighlighted[4];
					else if(weaponSwitchPlayerOne[2] == weaponSwitchHighlighted[2])
						temp = weaponSwitchHighlighted[5];
					else if(weaponSwitchPlayerOne[2] == weaponSwitchHighlighted[3])
						temp = weaponSwitchHighlighted[0];
					else if(weaponSwitchPlayerOne[2] == weaponSwitchHighlighted[4])
						temp = weaponSwitchHighlighted[1];
					else if(weaponSwitchPlayerOne[2] == weaponSwitchHighlighted[5])
						temp = weaponSwitchHighlighted[2];
					else if(weaponSwitchPlayerOne[2] == weaponSwitchHighlighted[6])
						temp = weaponSwitchHighlighted[2];

					weaponSwitchPlayerOne[2] = temp;
				}
				else
				{
					if(weaponSwitchPlayerTwo[2] == weaponSwitchHighlighted[7])
						temp = weaponSwitchHighlighted[10];
					else if(weaponSwitchPlayerTwo[2] == weaponSwitchHighlighted[8])
						temp = weaponSwitchHighlighted[11];
					else if(weaponSwitchPlayerTwo[2] == weaponSwitchHighlighted[9])
						temp = weaponSwitchHighlighted[12];
					else if(weaponSwitchPlayerTwo[2] == weaponSwitchHighlighted[10])
						temp = weaponSwitchHighlighted[7];
					else if(weaponSwitchPlayerTwo[2] == weaponSwitchHighlighted[11])
						temp = weaponSwitchHighlighted[8];
					else if(weaponSwitchPlayerTwo[2] == weaponSwitchHighlighted[12])
						temp = weaponSwitchHighlighted[9];
					else if(weaponSwitchPlayerTwo[2] == weaponSwitchHighlighted[13])
						temp = weaponSwitchHighlighted[9];

					weaponSwitchPlayerTwo[2] = temp;
				}
			}

			if(p_Input->CheckXboxBufferedButton((BYTE)i,XBUTTON_A))
			{
				short tempValue;
				if(i == 0)
				{
					for(short j=0; j<7; ++j)
					{
						if(weaponSwitchPlayerOne[2] == weaponSwitchHighlighted[j])
						{
							temp = weaponSwitchSelected[j];
							tempValue = j;
						}
					}


					if(temp == weaponSwitchSelected[0]
					|| temp == weaponSwitchSelected[1]
					|| temp == weaponSwitchSelected[2])
						weaponSwitchPlayerOne[0] = temp;
					else if(temp == weaponSwitchSelected[3]
					|| temp == weaponSwitchSelected[4]
					|| temp == weaponSwitchSelected[5])
						weaponSwitchPlayerOne[1] = temp;
					else if(temp == weaponSwitchSelected[6])
						AudioManager::GetInstance()->PlaySFX(*errorSound);

					if(temp != weaponSwitchSelected[6])
						EntityManager::GetEntityManager()->EntityList[0]->SetWeaponTo((GUNTYPE)tempValue);
				}
				else
				{
					for(short j=7; j<14; ++j)
					{
						if(weaponSwitchPlayerTwo[2] == weaponSwitchHighlighted[j])
						{
							temp = weaponSwitchSelected[j];
							tempValue = j;
						}
					}


					if(temp == weaponSwitchSelected[7]
					|| temp == weaponSwitchSelected[8]
					|| temp == weaponSwitchSelected[9])
						weaponSwitchPlayerTwo[0] = temp;
					else if(temp == weaponSwitchSelected[10]
					|| temp == weaponSwitchSelected[11]
					|| temp == weaponSwitchSelected[12])
						weaponSwitchPlayerTwo[1] = temp;
					else if(temp == weaponSwitchSelected[13])
						AudioManager::GetInstance()->PlaySFX(*errorSound);

					if(temp != weaponSwitchSelected[13])
						EntityManager::GetEntityManager()->EntityList[1]->SetWeaponTo((GUNTYPE)(tempValue % 7));
				}
			}
		}
	}
}