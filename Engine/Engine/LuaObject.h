#pragma once

#include "luainc.h"		// Contains all lua includes for the project so installation on machine is not required

#include "EntityManager.h"
#include "GameObject.h"
#include "Renderer.h"

#include "Player.h"
#include "Enemy.h"
#include "Projectile.h"

#include "PlatformManager.h"

#include <string>

class CGameObject;

class LuaObject
{
private:
	
	static LuaObject* luaObjectInstance;
	
	lua_State* LState;
protected:
	// None

public:
	// Constructors/Destructor
private:
	LuaObject(void);

public:
	~LuaObject(void);

	static LuaObject* GetLua();

	// Methods
	void LoadLuaFile(char* filename);
	void ShutdownLua(void);
	void ShutdownClass(void);

	EntityType cycleThroughTypes(short typeNum);

	// Lua Functions
	// Level Rendering
	static int AddPlatform(lua_State *luaState);

	// Trigger and Door Initialization
	static int AddDoorTrigger(lua_State* luaState);

	// Entity Managing
	static int AddEntity(lua_State* LState);

	// CheckPoint Initialization
	static int SetCheckPoint(lua_State* LState);

	// EndPoint Initialization
	static int SetEndPoint(lua_State* LState);
};

