#include "UserInterface.h"
#include "EntityManager.h"
#include "GameObject.h"
#include "LuaObject.h"

UserInterface* UserInterface::instance = NULL;
UserInterface* UserInterface::GetInstance()
{
	if( !instance )
	{
		return NULL;
	}

	return instance;
}
UserInterface* UserInterface::GetInstance( short screenWidth, short screenHeight )
{
	if( !instance )
	{
		instance = new UserInterface();
		instance->ScreenWidth = screenWidth;
		instance->ScreenHeight = screenHeight;
		instance->Init();
	}

	return instance;
}

bool UserInterface::Init()
{
	GameState = MAIN_MENU;
	ScreenTime = 4.0f;

	p_Render = DX9Render::GetRenderer();
	p_Timer = GameTimer::GetTimer();
	p_Input = InputManager::GetInputManager();

	StartMenuScreen = 0;
	p_Render->Load2DGameComponent( &StartMenuScreen, "Screens/Main_Menu.jpg" );
	StartMenuScreen->position.x = (float)(ScreenWidth/2);
	StartMenuScreen->position.y = (float)(ScreenHeight/2);
	
	LoadingScreen = 0;
	p_Render->Load2DGameComponent( &LoadingScreen, "Screens/Loading_Screen.jpg" );
	LoadingScreen->position.x = (float)(ScreenWidth/2);
	LoadingScreen->position.y = (float)(ScreenHeight/2);

	LoadingScreenHint = 0;
	p_Render->Load2DGameComponent( &LoadingScreenHint, "Screens/Loading_Screen_Hint.jpg" );
	LoadingScreenHint->position.x = (float)(ScreenWidth/2);
	LoadingScreenHint->position.y = (float)(ScreenHeight/2);

	LoadingScreenControls = 0;
	p_Render->Load2DGameComponent( &LoadingScreenControls, "Screens/Loading_Screen_Controls.jpg" );
	LoadingScreenControls->position.x = (float)(ScreenWidth/2);
	LoadingScreenControls->position.y = (float)(ScreenHeight/2);

	WinScreen = 0;
	p_Render->Load2DGameComponent( &WinScreen, "Screens/Win_Screen.jpg" );
	WinScreen->position.x = (float)(ScreenWidth/2);
	WinScreen->position.y = (float)(ScreenHeight/2);

	LoseScreen = 0;
	p_Render->Load2DGameComponent( &LoseScreen, "Screens/Lose_Screen.jpg" );
	LoseScreen->position.x = (float)(ScreenWidth/2);
	LoseScreen->position.y = (float)(ScreenHeight/2);

	PauseScreen = 0;
	p_Render->Load2DGameComponent( &PauseScreen, "Screens/Pause_Screen.jpg" );
	PauseScreen->position.x = (float)(ScreenWidth/2);
	PauseScreen->position.y = (float)(ScreenHeight/2);

	ControlScreen = 0;
	p_Render->Load2DGameComponent( &ControlScreen, "Screens/Controls.jpg" );
	ControlScreen->position.x = (float)(ScreenWidth/2);
	ControlScreen->position.y = (float)(ScreenHeight/2);

	CreditsScreen = 0;
	p_Render->Load2DGameComponent( &CreditsScreen, "Screens/Credits_Screen.jpg" );
	CreditsScreen->position.x = (float)(ScreenWidth/2);
	CreditsScreen->position.y = (float)(ScreenHeight/2);

	DisplayLoadingScreen();

	SelectionIndicator = 0;
	p_Render->Load2DGameComponent( &SelectionIndicator, "Selector.png" );

	p_Render->Load2DGameComponent(
		(GameComponent2D**)&MainMenuButton.Sprite, "Buttons/Menu_Button.png" );
	MainMenuButton.DestinationState = MAIN_MENU;
	MainMenuButton.SetPosition( ScreenWidth/2, ScreenHeight/2 );

	p_Render->Load2DGameComponent( 
		(GameComponent2D**)(&StartButton.Sprite), "Buttons/Start_Button.png" );
	StartButton.DestinationState = RESET_GAME;
	StartButton.Sprite->position.x = (float)(ScreenWidth/2) + 575.0f;
	StartButton.Sprite->position.y = (float)(ScreenHeight/2) + 250.0f;

	p_Render->Load2DGameComponent( 
		(GameComponent2D**)&OptionsButton.Sprite, "Buttons/Extras_Button.png" );
	OptionsButton.DestinationState = OPTIONS;
	OptionsButton.Sprite->position = StartButton.GetPosition() + D3DXVECTOR2( 0.0f, 50.0f );

	p_Render->Load2DGameComponent(
		(GameComponent2D**)&ExitButton.Sprite, "Buttons/Exit_Button.png" );
	ExitButton.DestinationState = EXIT_GAME;
	ExitButton.Sprite->position = D3DXVECTOR2( ScreenWidth/2 + 575.0f, ScreenHeight/2 + 350.0f );

	p_Render->Load2DGameComponent(
		(GameComponent2D**)&ResumeButton.Sprite, "Buttons/Resume_Button.png" );
	ResumeButton.DestinationState = IN_GAME;
	ResumeButton.SetPosition( ExitButton.GetPosition().x,
		ExitButton.GetPosition().y - 50.0 );

	p_Render->Load2DGameComponent(
		(GameComponent2D**)&MainMenuButton.Sprite, "Buttons/Menu_Button.png" );
	MainMenuButton.DestinationState = MAIN_MENU;
	MainMenuButton.Sprite->position = StartButton.GetPosition();

	p_Render->Load2DGameComponent( 
		(GameComponent2D**)&ControlsButton.Sprite, "Buttons/Controls_Button.png" );
	ControlsButton.DestinationState = CONTROLS;
	ControlsButton.Sprite->position = OptionsButton.GetPosition();

	p_Render->Load2DGameComponent( 
		(GameComponent2D**)&CreditsButton.Sprite, "Buttons/Credits_Button.png" );
	CreditsButton.DestinationState = CREDITS;
	CreditsButton.Sprite->position = ExitButton.GetPosition();

	p_Render->Load2DGameComponent( 
		(GameComponent2D**)&ContinueButton.Sprite, "Buttons/Continue_Button.png" );
	ContinueButton.DestinationState = RESET_GAME;
	ContinueButton.Sprite->position = ResumeButton.GetPosition();

	ContinueButton_inGame = ContinueButton;
	ContinueButton_inGame.DestinationState = IN_GAME;

	ContinueButton_win = ContinueButton;
	ContinueButton_win.DestinationState = MAIN_MENU;

	MouseCursor = 0;
	p_Render->Load2DGameComponent( &MouseCursor, "curser.png" );

	sprintf_s( message, messageSize,
		"Escape: exit\n F: enable/disable full screen\n ");

	SelectedButton = 0;
	Selection = 0;
	PlayerPaused = 0;

	//////////////////////////////////////////////////////////////////////////
	// START LOADING USER INTERFACE STUFF HERE

	//////////////////////////////////////////////////////////////////////////
	// START LOADING WEAPON SWITCH STATION ASSETS HERE
	//////////////////////////////////////////////////////////////////////////
	p_Station = WeaponStation::GetInstance(ScreenWidth, ScreenHeight);
	//p_Station->Init();

	return true;
}

GAME_STATE UserInterface::Update()
{
	bool p = false;
	MouseCursor->position.x = InputManager::GetInputManager()->GetMousePosX();
	MouseCursor->position.y = InputManager::GetInputManager()->GetMousePosY();


	SelectedButton = 0;
	byte NumSelections = 0;

	short leftStickY = 0;
	if( GameState != WEAPON_SWITCH )
		leftStickY = InputManager::GetInputManager()->CheckXboxBufferedLStickY(PlayerPaused);

	switch( GameState )
	{
	case LOADING:

		ScreenTime -= p_Timer->GetDeltaTime();

		if( ScreenTime < 0.0 )
		{
			ScreenTime = 0.0f;
			GameState = IN_GAME;
		}

		break;

	case MAIN_MENU:

		NumSelections = 3;
		//if( p_Input->CheckXboxBufferedDpad( PlayerPaused, XDPAD_UP ) )
		if(leftStickY == 1)
			if( Selection > 0 ) Selection--;
		//if( p_Input->CheckXboxBufferedDpad( PlayerPaused, XDPAD_DOWN ) )
		if(leftStickY == -1)
			if( Selection < NumSelections-1 ) Selection++;

		if( Selection < 0 ) Selection = 0;
		if( Selection > NumSelections-1 ) Selection = NumSelections-1;

		switch( Selection )
		{
		case 0:
			SelectedButton = &StartButton;
			break;
		case 1:
			SelectedButton = &OptionsButton;
			break;
		case 2:
			SelectedButton = &ExitButton;
			break;
		}

		if( StartButton.isHighlighted() ) 
		{
			SelectedButton = &StartButton;
			Selection = 0;
		}
		if( OptionsButton.isHighlighted() ) 
		{
			SelectedButton = &OptionsButton;
			Selection = 1;
		}
		if( ExitButton.isHighlighted() )
		{
			SelectedButton = &ExitButton;
			Selection = 2;
		}

		break;

	case PAUSED:

		NumSelections = 2;
		//if( p_Input->CheckXboxBufferedDpad( PlayerPaused, XDPAD_UP ) )
		if(leftStickY == 1)
			if( Selection > 0 ) Selection--;
		//if( p_Input->CheckXboxBufferedDpad( PlayerPaused, XDPAD_DOWN ) )
		if(leftStickY == -1)
			if( Selection < NumSelections-1 ) Selection++;

		if( Selection < 0 ) Selection = 0;
		if( Selection > NumSelections-1 ) Selection = NumSelections-1;

		switch( Selection )
		{
		case 0:
			SelectedButton = &ResumeButton;
			break;
		case 1:
			SelectedButton = &ExitButton;
		}

		if( ResumeButton.isHighlighted() )
		{
			SelectedButton = &ResumeButton;
			Selection = 0;
		}
		if( ExitButton.isHighlighted() )
		{
			SelectedButton = &ExitButton;
			Selection = 1;
		}

		break;

	case OPTIONS:

		NumSelections = 3;
		//if( p_Input->CheckXboxBufferedDpad( PlayerPaused, XDPAD_UP ) )
		if(leftStickY == 1)
			if( Selection > 0 ) Selection--;
		//if( p_Input->CheckXboxBufferedDpad( PlayerPaused, XDPAD_DOWN ) )
		if(leftStickY == -1)
			if( Selection < NumSelections-1 ) Selection++;

		if( Selection < 0 ) Selection = 0;
		if( Selection > NumSelections-1 ) Selection = NumSelections-1;

		switch( Selection )
		{
		case 0:
			SelectedButton = &MainMenuButton;
			break;
		case 1:
			SelectedButton = &ControlsButton;
			break;
		case 2:
			SelectedButton = &CreditsButton;
			break;
		}

		if( MainMenuButton.isHighlighted() )
		{
			SelectedButton = &MainMenuButton;
			Selection = 0;
		}
		else if( ControlsButton.isHighlighted() )
		{
			SelectedButton = &ControlsButton;
			Selection = 1;
		}
		else if( CreditsButton.isHighlighted() )
		{
			SelectedButton = &CreditsButton;
			Selection = 2;
		}


		break;

	case WEAPON_SWITCH:
		p_Station->Update();
		break;

	case CREDITS:
		if( p_Input->CheckKeyboardBufferedKey(DIK_RETURN) )
		{
			GameState = OPTIONS;
		}

		for(byte i = 0; i<4; ++i)
		{
			if( p_Input->CheckXboxBufferedButton(i, XBUTTON_A) )
			{
				GameState = OPTIONS;
			}
		}
		break;

	case GAME_OVER:
		NumSelections = 2;
		//if( p_Input->CheckXboxBufferedDpad( PlayerPaused, XDPAD_UP ) )
		if(leftStickY == 1)
			if( Selection > 0 ) Selection--;
		//if( p_Input->CheckXboxBufferedDpad( PlayerPaused, XDPAD_DOWN ) )
		if(leftStickY == -1)
			if( Selection < NumSelections-1 ) Selection++;

		if( Selection < 0 ) Selection = 0;
		if( Selection > NumSelections-1 ) Selection = NumSelections-1;

		switch( Selection )
		{
		case 0:
			SelectedButton = &ContinueButton;
			break;
		case 1:
			SelectedButton = &ExitButton;
		}

		if( ContinueButton.isHighlighted() )
		{
			SelectedButton = &ContinueButton;
			Selection = 0;
		}
		if( ExitButton.isHighlighted() )
		{
			SelectedButton = &ExitButton;
			Selection = 1;
		}
		break;

	case GAME_OVER_CONTINUE:
		NumSelections = 2;
		//if( p_Input->CheckXboxBufferedDpad( PlayerPaused, XDPAD_UP ) )
		if(leftStickY == 1)
			if( Selection > 0 ) Selection--;
		//if( p_Input->CheckXboxBufferedDpad( PlayerPaused, XDPAD_DOWN ) )
		if(leftStickY == -1)
			if( Selection < NumSelections-1 ) Selection++;

		if( Selection < 0 ) Selection = 0;
		if( Selection > NumSelections-1 ) Selection = NumSelections-1;

		switch( Selection )
		{
		case 0:
			SelectedButton = &ContinueButton_inGame;
			break;
		case 1:
			SelectedButton = &ExitButton;
		}

		if( ContinueButton_inGame.isHighlighted() )
		{
			SelectedButton = &ContinueButton_inGame;
			Selection = 0;
		}
		if( ExitButton.isHighlighted() )
		{
			SelectedButton = &ExitButton;
			Selection = 1;
		}
		break;

	case GAME_WIN:
		NumSelections = 2;
		//if( p_Input->CheckXboxBufferedDpad( PlayerPaused, XDPAD_UP ) )
		if(leftStickY == 1)
			if( Selection > 0 ) Selection--;
		//if( p_Input->CheckXboxBufferedDpad( PlayerPaused, XDPAD_DOWN ) )
		if(leftStickY == -1)
			if( Selection < NumSelections-1 ) Selection++;

		if( Selection < 0 ) Selection = 0;
		if( Selection > NumSelections-1 ) Selection = NumSelections-1;

		switch( Selection )
		{
		case 0:
			SelectedButton = &ContinueButton_win;
			break;
		case 1:
			SelectedButton = &ExitButton;
		}

		if( ContinueButton_win.isHighlighted() )
		{
			SelectedButton = &ContinueButton_win;
			Selection = 0;
		}
		if( ExitButton.isHighlighted() )
		{
			SelectedButton = &ExitButton;
			Selection = 1;
		}
		break;

	case CONTROLS:

		if( p_Input->CheckKeyboardBufferedKey(DIK_RETURN) )
		{
			GameState = OPTIONS;
		}

		for(byte i = 0; i<4; ++i)
		{
			if( p_Input->CheckXboxBufferedButton(i, XBUTTON_A) )
			{
				GameState = OPTIONS;
			}
		}

		break;

	case IN_GAME:

		for( byte i = 0; i < 4; ++i )
		{
			if( p_Input->CheckXboxBufferedButton(i, XBUTTON_START) )
			{
				GameState = PAUSED;
				PlayerPaused = i;
				break;
			}
		}
		if( p_Input->CheckKeyboardBufferedKey(DIK_RETURN) )
		{
			GameState = PAUSED;
			PlayerPaused = 5;
		}

		break;
	case RESET_GAME:
		{
			EntityManager::GetEntityManager()->ClearList();
			PlatformManager::GetManager()->Shutdown();
			//AudioManager::GetInstance()->Shutdown();
			//Camera* tempCam  = EntityManager::GetEntityManager()->GetActiveCamera();
			
			EntityManager::GetEntityManager()->ClearBullets();
			EntityManager::GetEntityManager()->Init();
			
			EntityManager* t = EntityManager::GetEntityManager();
			float p[] = {0.0f, 0.0f, 0.0f};
			float d[] = {1.0f, -1.0f, 0.0f};
			float a[] = {1.0f, 0.1f, 0.10f};
			float amb[] = {0.05f, 0.05f, 0.05f};
			Light* light = new Light( 0, 2.0f, p, d, a, amb );
			EntityManager::GetEntityManager()->light = light;
			//AudioManager::GetInstance()->Initialize();
			LuaObject* luaClass = LuaObject::GetLua();
			luaClass->LoadLuaFile("Assets\\Scripts\\LevelOne.lvl");
			PlatformManager::GetManager();

			EntityManager::GetEntityManager()->SetActiveCamera(DX9Render::GetRenderer()->GetActiveCamera());

			GameState = WEAPON_SWITCH;

		}

		break; 


	case EXIT_GAME:

		PostQuitMessage(0);

		break;

	default:
		GameState = IN_GAME;
		break;
	}

	if( SelectedButton )
	{
		if( SelectedButton->IsClicked() || p_Input->CheckXboxBufferedButton( PlayerPaused, XBUTTON_A ) )
			GameState = SelectedButton->DestinationState;
		SelectionIndicator->position = SelectedButton->GetPosition();
	}
	return GameState;
}

GAME_STATE UserInterface::GetState()
{
	return GameState;
}

LEVEL UserInterface::GetLevelState()
{
	return LevelState;
}
void UserInterface::SetState(GAME_STATE state)
{
	GameState = state;
}

void UserInterface::SetLevel(LEVEL Level)
{
	LevelState = Level;
	switch (Level)
	{
	case LEVEL_TUTORIAL:
		//TODO: chage this file names
		levelFileName = "Assets\\Scripts\\Tutorial.lvl";
		break;
	case LEVEL_ONE:
		levelFileName = "Assets\\Scripts\\LevelOne.lvl";
		break;
	case LEVEL_TWO:
		levelFileName = "Assets\\Scripts\\LevelOne.lvl";
		break;
	case LEVEL_THREE:
		levelFileName = "Assets\\Scripts\\LevelOne.lvl";
		break;
	}
}

void UserInterface::DisplayLoadingScreen()
{
	if( p_Render->BeginRender() )
	{
		p_Render->Begin2DRender();

		p_Render->Render2DComponent( &LoadingScreen );

		p_Render->End2DRender();
		p_Render->EndRender();
	}
}

void UserInterface::RenderUI()
{
	p_Render = DX9Render::GetRenderer();
	p_Render->Begin2DRender();

	{
		switch (GameState)
		{
		case LOADING:
			break;

		case MAIN_MENU:
			p_Render->Render2DComponent( &StartMenuScreen );
			p_Render->Render2DComponent( &SelectionIndicator );
			p_Render->Render2DComponent( (GameComponent2D**)&StartButton.Sprite );
			p_Render->Render2DComponent( (GameComponent2D**)&OptionsButton.Sprite );
			p_Render->Render2DComponent( (GameComponent2D**)&ExitButton.Sprite );

			p_Render->Render2DComponent( &MouseCursor );
			break;

		case PAUSED:

			p_Render->Render2DComponent( &PauseScreen );
			p_Render->Render2DComponent( &SelectionIndicator );
			p_Render->Render2DComponent( (GameComponent2D**)&ResumeButton.Sprite );
			p_Render->Render2DComponent( (GameComponent2D**)&ExitButton.Sprite );
			p_Render->Render2DComponent( &MouseCursor );
			break;

		case OPTIONS:

			p_Render->Render2DComponent( &StartMenuScreen );
			p_Render->RenderText( "OPTIONS", (float)(ScreenWidth/2) - 30, (float)(ScreenHeight/2) - 100 );
			p_Render->Render2DComponent( &SelectionIndicator );
			p_Render->Render2DComponent( (GameComponent2D**)&MainMenuButton.Sprite );
			p_Render->Render2DComponent( (GameComponent2D**)&ControlsButton.Sprite );
			p_Render->Render2DComponent( (GameComponent2D**)&CreditsButton.Sprite );
			p_Render->Render2DComponent( &MouseCursor );

			break;

		case CONTROLS:
			p_Render->Render2DComponent( &ControlScreen );

			break;

		case CREDITS:
			p_Render->Render2DComponent( &CreditsScreen );
			break;

		case GAME_OVER:
			p_Render->Render2DComponent( &LoseScreen );
			p_Render->Render2DComponent( &SelectionIndicator );
			p_Render->Render2DComponent( (GameComponent2D**)&ContinueButton.Sprite );
			p_Render->Render2DComponent( (GameComponent2D**)&ExitButton.Sprite );
			p_Render->Render2DComponent( &MouseCursor );
			break;

		case GAME_OVER_CONTINUE:
			p_Render->Render2DComponent( &LoseScreen );
			p_Render->Render2DComponent( &SelectionIndicator );
			p_Render->Render2DComponent( (GameComponent2D**)&ContinueButton_inGame.Sprite );
			p_Render->Render2DComponent( (GameComponent2D**)&ExitButton.Sprite );
			p_Render->Render2DComponent( &MouseCursor );
			break;

		case GAME_WIN:
			p_Render->Render2DComponent( &WinScreen );
			p_Render->Render2DComponent( &SelectionIndicator );
			p_Render->Render2DComponent( (GameComponent2D**)&ContinueButton_win.Sprite );
			p_Render->Render2DComponent( (GameComponent2D**)&ExitButton.Sprite );
			p_Render->Render2DComponent( &MouseCursor );
			break;

		case WEAPON_SWITCH:
			p_Station->RenderStation();
			break;

		case IN_GAME:

			p_Render->RenderText( message, 0.0f, 0.0f );

			break;

		default:
			break;
		}

		float leftstick = p_Input->CheckXboxLStickX(0);
		char result[100];
		sprintf_s( result, 100, "Left Stick: %f", leftstick );
		p_Render->RenderText( result, 100, 100 );
	}

	p_Render->End2DRender();
}

void UserInterface::Shutdown()
{
	if( instance )
	{
		StartMenuScreen->Shutdown();
		StartMenuScreen = 0;
		LoadingScreen->Shutdown();
		LoadingScreen = 0;
		LoadingScreenHint->Shutdown();
		LoadingScreenHint = 0;
		LoadingScreenControls->Shutdown();
		LoadingScreenControls = 0;
		CreditsScreen->Shutdown();
		CreditsScreen = 0;
		ControlScreen->Shutdown();
		ControlScreen = 0;
		PauseScreen->Shutdown();
		PauseScreen = 0;
		WinScreen->Shutdown();
		WinScreen = 0;
		LoseScreen->Shutdown();
		LoseScreen = 0;
		SelectionIndicator->Shutdown();
		SelectionIndicator = 0;


		StartButton.Shutdown();
		OptionsButton.Shutdown();
		CreditsButton.Shutdown();
		ControlsButton.Shutdown();
		ExitButton.Shutdown();
		ResumeButton.Shutdown();
		MainMenuButton.Shutdown();

		MouseCursor->Shutdown();
		MouseCursor = 0;

		p_Station->Shutdown();

		delete instance;
	}
	instance = NULL;
}




///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///	BUTTON CLASSES

Button::Button()
{
	Sprite = 0;
}

bool Button::IsClicked()
{
	if( InputManager::GetInputManager()->CheckMouseButton(0) )
	{
		D3DXVECTOR2 MousePos = D3DXVECTOR2(
			InputManager::GetInputManager()->GetMousePosX(),
			InputManager::GetInputManager()->GetMousePosY() );

		RECT r = GetBoundingBox();

		if( MousePos.x < r.left )	return false;
		if( MousePos.x > r.right )	return false;
		if( MousePos.y < r.top )	return false;
		if( MousePos.y > r.bottom )	return false;

		return true;
	}

	return false;
}

bool Button::isHighlighted()
{
	D3DXVECTOR2 MousePos = D3DXVECTOR2(
		InputManager::GetInputManager()->GetMousePosX(),
		InputManager::GetInputManager()->GetMousePosY() );

	RECT r = GetBoundingBox();

	if( MousePos.x < r.left )	return false;
	if( MousePos.x > r.right )	return false;
	if( MousePos.y < r.top )	return false;
	if( MousePos.y > r.bottom )	return false;

	return true;
}

void Button::SetPosition( int x, int y )
{
	Sprite->position = D3DXVECTOR2( x, y );
}

D3DXVECTOR2 Button::GetPosition()
{
	return Sprite->position;
}

RECT Button::GetBoundingBox()
{
	RECT r;

	r.left		= Sprite->position.x - Sprite->imageInfo.Width/2;
	r.right		= Sprite->position.x + Sprite->imageInfo.Width/2;
	r.top		= Sprite->position.y - Sprite->imageInfo.Height/2;
	r.bottom	= Sprite->position.y + Sprite->imageInfo.Height/2;

	return r;
}

void Button::Shutdown()
{
	Sprite->Shutdown();
	Sprite = 0;
}