// Global variables - Passed in from application
struct Light
{
	int type;
	float range;
	float3 position;
	float3 direction;
	float3 attenuation;
	float3 ambient;
};
Light light;

float4x4 viewProj;
float4x4 worldViewProjMat;			// Transforms positions to view space
float4x4 worldInverseTransposeMat;	// Transforms normals to world space
float4x4 worldMat;					// World matrix
float3 eyePos;						// Eye position (camera position)

// Material
float4 ambientMaterial;				// Ambient Material Color
float4 diffuseMaterial;				// Diffuse Material Color (ambient material, scaled down version of diffuse)
float4 specularMaterial;			// Specular Material Color
float  specularPower;				// Specular Power

uniform extern float4x4 gFinalXForms[35];
extern int NumVertInfluences = 2;

// Texture
texture tex;						// Image texture to be applied to object
sampler texSamp = sampler_state		// Texture sampler state
{
	Texture = <tex>;
	// Trilinear Filtering
	MinFilter = LINEAR;
	MagFilter = LINEAR;
	MipFilter = LINEAR;
	AddressU = WRAP;
	AddressV = WRAP;
};

struct VS_OUT
{
	float4 posH : POSITION0;		// Vertex position in homgenous clip space
	float3 normW : TEXCOORD0;		// Vertex normal in world space
	float3 posW : TEXCOORD1;		// Vertex position in world space
	float2 tex0 : TEXCOORD2;		// Vertex uv texture coordinate 0
};

// Vertex Shader
VS_OUT VS(float4 pos : POSITION0, float3 norm : NORMAL0, float2 tex0: TEXCOORD0,
			float4 weights : BLENDWEIGHT0, int4 boneIndex : BLENDINDICES0)
{
	VS_OUT vsOut = (VS_OUT) 0;
	float4x4 wvpmat = mul( worldMat, viewProj );
	
	int n = NumVertInfluences - 1;
	float lastWeight = 0;
	float4 p = float4( 0, 0, 0, 0 );
	float3 normal = float3( 0, 0, 0 );
	norm = normalize( norm );
	
	for( int i = 0; i < n; ++i )
	{
		lastWeight += weights[i];
		p += weights[i]*mul(pos, gFinalXForms[boneIndex[i]]);
		normal += weights[i]*mul(float4(norm, 1), gFinalXForms[boneIndex[i]]);
	}
	lastWeight = 1.0f - lastWeight;
	p += lastWeight*mul(pos, gFinalXForms[boneIndex[n]]);
	normal += lastWeight*mul(float4(norm, 1), gFinalXForms[boneIndex[n]]);
	p.w = 1.0f;

	// Transform vertex position to homogenous clip space (pos * WVPMat)
	//vsOut.posH = mul( p, worldViewProjMat);
	
	vsOut.posH = mul( p, viewProj );
	
	// Transform vertex position to world space
	vsOut.posW = mul( p, worldMat).xyz;
	
	// Transform vertex normal to world space (norm * WITMat)
	//vsOut.normW = mul( normal, worldInverseTransposeMat).xyz;
	vsOut.normW = mul( normal, worldMat ).xyz;
	
	// Normalize the normals to unit length
	//vsOut.normW = normalize(vsOut.normW);
	
	// Pass on texture coordinate to pixel shader
	vsOut.tex0 = tex0;

	return vsOut;	
}

// Pixel Shader
float4 PS(VS_OUT vsOut): COLOR
{

	vsOut.normW = normalize( vsOut.normW );
	float4 texCol = tex2D( texSamp, vsOut.tex0 );
	float3 toEye = normalize( eyePos - vsOut.posW );
	float3 lightDir = float3( 0.0f, 0.0f, 0.0f );
	float i = 0.0f;
	float d = 0.0f;
	float A = 0.0f;
	float4 ambient = float4( 0.0, 0.0, 0.0, 0.0 );
	float4 color = float4( 0.0, 0.0, 0.0, 0.0 );
	
	float distratio = 0.0f;

	d = distance( light.position, vsOut.posW );
	
	distratio = light.range / d;

	if( distratio > 0.9f )
	{
		color = texCol * float4(1.0, 1.0, 1.0, 1.0);
	}else if( distratio > 0.5f )
	{
		color = texCol * float4(0.7, 0.7, 0.7, 1.0);
	}else if( distratio > 0.3f )
	{
		color = texCol * float4(0.3, 0.3, 0.3, 1.0);
	}else
	{
		color = texCol * float4( light.ambient, 1.0f );
	}
	
	return color;

	/*
	// Get texel from texture map taht gets mapped to this pixel
	float3 texColor = tex2D(texSamp, vsOut.tex0).rgb;
	
	// Interpolated normals can "unnormalize", so normalize
	vsOut.normW = normalize(vsOut.normW);
	
	// Compute vector from the vertex to the eye position (V)
	float3 toEye = normalize(eyePos - vsOut.posW);
	
	// Light Direction Vector (L)
	//float3 lightDir = normalize(light.position - vsOut.posW);
	float3 lightDir = normalize( vsOut.posW - light.position );
	
	// Determine how much specular (if any), makes it to the eye
	
	//////////////////////////////////////////////////////////////////////////
	// Phong specular calculation
	//////////////////////////////////////////////////////////////////////////
	// Compute the reflection vector
	//float3 R = reflect(-lightDir, vsOut.normW);
	//float T = pow(max(dot(R, toEye), 0.0f), specularPower);
	
	//////////////////////////////////////////////////////////////////////////
	// Blinn-Phong specular calculation
	//////////////////////////////////////////////////////////////////////////
	// Half vector used in Blinn-Phong shading (faster than Phong)
	float3 H = normalize(lightDir + normalize(toEye));
	float T = pow(max(dot(H, vsOut.normW), 0.0f), specularPower);
	// Determine the diffuse light intensit that strikes the vertex
	float S = max(dot(lightDir, vsOut.normW), 0.0f);
	// Compute Ambient, Diffuse, Specular terms seperately
	
	float4 specularLight = float4( 0, 0, 0, 1 );
	float4 diffuseLight = float4( 1, 1, 1, 1 );
	
	float3 spec = T * (specularMaterial * specularLight).rgb;
	float3 diffuse = S * (diffuseMaterial * texColor * diffuseLight).rgb;
	float3 ambient = (ambientMaterial * float4(light.ambient, 1));
	
	// Attenuation
	float d = distance(light.position, vsOut.posW);
	float A = light.attenuation[0] + light.attenuation[1] * d +
				light.attenuation[2] * d * d;
	
	// Sum all of the terms together and copy diffuse alpha 
	return float4( ambient + (diffuse + spec) / A, diffuseMaterial.a);
	*/
}

float4 PSunlit(VS_OUT vsOut): COLOR
{
	float4 colortorender = tex2D( texSamp, vsOut.tex0 );
	return colortorender;
}

// Technique and Passes
technique tech0
{
	pass p0
	{
		// Specify vertex and pixel shader associated with this pass
		vertexShader = compile vs_3_0 VS();	// Vertex Shader 2.0, compiles VS()
		pixelShader = compile ps_3_0 PS();	// Pixel Shader 2.0, compiles PS()
		
		// Render states associated with this pass
		//Fillmode = Wireframe;
		
		// Uncomment these states below, to use diffuse material's 
		// alpha component to render translucent geometry.
		
		// AlphaBlendEnable = true;
		// SrcBlend = SrcAlpha;
		// DestBlend = InvSrcAlpha;
		// BlendOp = Add;
	}
}

technique unlit
{
	pass p0
	{
		VertexShader = compile vs_3_0 VS();
		PixelShader = compile ps_3_0 PSunlit();
	}
}