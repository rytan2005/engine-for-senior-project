extern float4x4 wvpMat;
extern texture tex;

sampler texSamp = sampler_state
{
	Texture = <tex>;
	MinFilter = LINEAR;
	MagFilter = LINEAR;
	MipFilter = LINEAR;
	AddressU = WRAP;
	AddressV = WRAP;
};

void vs( float3 posL : POSITION0, out float4 oPosH : POSITION0,
			out float3 oEnvTex : TEXCOORD0 )
{
	// setting z = w means z/w = 1 (i.e. skydome is always on far plane
	oPosH = mul( float4( posL, 1.0f ), wvpMat ).xyww;
	
	// set the vertex position as the tex coord for the cube mesh
	oEnvTex = posL;
}

float4 ps( float3 envTex : TEXCOORD0 ) : COLOR
{
	return texCUBE( texSamp, envTex );
}

technique tech0
{
	pass p0
	{
		VertexShader = compile vs_2_0 vs();
		PixelShader = compile ps_2_0 ps();
		CullMode = None;
		ZFunc = Always;	// always write to the depth Buffer
		StencilEnable = true;
		StencilFunc = Always;
		StencilPass = Replace;
		StencilRef = 0; // clear to zero
	}
}