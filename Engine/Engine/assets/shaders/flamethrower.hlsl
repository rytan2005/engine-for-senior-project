uniform extern float4x4	gWVP;
uniform extern texture	gTex;
uniform extern float3	gEyePosL;
uniform extern float3	gAccel;
uniform extern float	gTime;
uniform extern int		gViewportHeight;

sampler TexS = sampler_state
{
	Texture = <gTex>;
	MinFilter = LINEAR;
	MagFilter = LINEAR;
	MipFilter = POINT;
	AddressU = CLAMP;
	AddressV = CLAMP;
};

struct OutputVS
{
	float4 posH	:POSITION0;
	float2 tex0	:TEXCOORD0;
	float size	: PSIZE;
};

OutputVS VS(	float3 posL		: POSITION0,
				float3 vel		: TEXCOORD0,
				float size		: TEXCOORD1,
				float time		: TEXCOORD2,
				float lifeTime	: TEXCOORD3,
				float mass		: TEXCOORD4,
				float4 color	: COLOR0 )
{
	OutputVS outVS = (OutputVS)0;
	
	float t = gTime - time;
	posL = posL + vel*t + 0.5f*gAccel*t*t;
	
	outVS.posH = mul( float4(posL, 1), gWVP );
	
	float d = distance( posL, gEyePosL );
	outVS.size = gViewportHeight*size/(1.0f + 8.0f*d);
	
	return outVS;
}

float4 PS( float2 tex0 : TEXCOORD0 ) : COLOR
{
	float4 color = tex2D( TexS, tex0 );
	clip( color.a - 0.5f );
	return color;
}

technique tech0
{
	pass p0
	{
		VertexShader = compile vs_3_0 VS();
		PixelShader = compile  ps_3_0 PS();
		
		PointSpriteEnable = true;
		AlphaBlendEnable = true;
		SrcBlend = One;
		DestBlend = One;
		
		ZWriteEnable = true;
	}
}

technique tech1
{
	pass p0
	{
		VertexShader = compile vs_3_0 VS();
		PixelShader = compile ps_3_0 PS();
		
		PointSpriteEnable = true;
		ZWriteEnable = true;
	}
}
































