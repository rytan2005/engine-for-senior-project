#pragma once

//include DirectX
#include <d3d9.h>
#include <d3dx9.h>
#pragma comment (lib, "d3d9.lib")
#pragma comment (lib, "d3dx9.lib")

//begin including Havok stuff
#include <Common/Base/hkBase.h>
#include <Common/Base/Memory/System/Util/hkMemoryInitUtil.h>
#include <Common/Base/Memory/Allocator/Malloc/hkMallocAllocator.h>
#include <Common/Base/Fwd/hkcstdio.h>

// Physics
#include <Physics/Dynamics/World/hkpWorld.h>
#include <Physics/Collide/Dispatch/hkpAgentRegisterUtil.h>
#include <Physics/Dynamics/Entity/hkpRigidBody.h>
#include <Physics/Collide/Shape/Convex/Box/hkpBoxShape.h>
#include <Physics/Collide/Shape/Convex/Sphere/hkpSphereShape.h>
#include <Physics/Utilities/Dynamics/Inertia/hkpInertiaTensorComputer.h>
#include <Physics/Dynamics/Collide/ContactListener/hkpContactListener.h>
#include <Physics/Collide/Shape/Misc/PhantomCallback/hkpPhantomCallbackShape.h>
#include <Physics/Collide/Shape/Misc/Bv/hkpBvShape.h>

// Collision Groups (used when specifying setCollisionFilterInfo as LAYER_KEYFRAME etc.)
#include <Physics/Collide/Filter/Group/hkpGroupFilterSetup.h>
#include <Physics/Collide/Filter/hkpCollidableCollidableFilter.h>
//used during bullet calculations and others
#include <Common/Base/Math/Matrix/hkMatrix3Util.h>

// For the phantom triggers
#include <Physics/Dynamics/Phantom/hkpAabbPhantom.h>

// Visual Debugger includes
#include <Common/Visualize/hkVisualDebugger.h>
#include <Physics/Utilities/VisualDebugger/hkpPhysicsContext.h>

// Platform specific initialization
#include <Common/Base/System/Init/PlatformInit.cxx>

// Used for character rigid body
#include <Physics/Utilities/CharacterControl/CharacterRigidBody/hkpCharacterRigidBody.h>
#include <Physics/Utilities/CharacterControl/CharacterRigidBody/hkpCharacterRigidBodyListener.h>
// character proxy
#include <Physics/Utilities/CharacterControl/CharacterProxy/hkpCharacterProxy.h>
// state machine
#include <Physics/Utilities/CharacterControl/StateMachine/hkpDefaultCharacterStates.h>
// Capsule shape
#include <Physics/Collide/Shape/Convex/Capsule/hkpCapsuleShape.h>
//end including Havok stuff

#include "CustomCollisionFilter.h"

#include "DX9Renderer.h"
#include "Weapon.h"

static const hkVector4 UP (0.0f,1.0f,0.0f);	//up vector for the camera and other calculations

static void HK_CALL errorReport(const char* msg, void* userContext)
{
	using namespace std;
	printf("%s", msg);
}

class MyContactStateListener;

struct CharacterData
{
	hkpCharacterRigidBody* p_CharacterBody;
	hkpCharacterContext* p_CharacterContext;
	hkReal XAccel;		//horizontal player acceleration
	hkReal YAccel;		//forward/backward player acceleration (into the z buffer)
	bool lockMovement;
	bool movePhantom;
	float lockTimer;
};

static hkpCharacterInput PlayerInputs[2];
static hkpCharacterInput EnemyInputs[15];//MAX_NUM_ENEMIES from EntityManager.h

class Havok
{
private:
	static Havok* hkPhysicsCore;
	bool	Initialized;					//has singleton been initialized
	
	hkMemoryRouter* MemoryRouter;
	hkpWorld* World;
	MyContactStateListener* p_Listener;		//custom collision contact listener that does user-defined responses to collision events
	CustomCollisionFilter* FilterSetup;		//custom collision filter that fixed bug with trigger volume triggering on walls/doors and player bullets hitting player
	hkVisualDebugger* VisualDebugger;
	
	//player's capsule shape for standing and crouching
	hkpShape* StandShape;
	hkpShape* CrouchShape;
	hkpRigidBody* FiregunPhantom[2];		//The phantom used by the player's flamethrower to hurt multiple enemies at once
	CharacterData Players[2];				//store movement info and character state (on ground/in air etc.) see struct above
	CharacterData Enemies[15];				//store movement info and character state (on ground/in air etc.) for up to 10 enemies at a time
	byte numPlayers;
	byte numEnemies;
	
	hkReal CurrentAngle;					//angle used to determine player's orientation
	hkpSurfaceInfo*	PreviousGround;			//the demo crashed when shutting down without this hkpSurfaceInfo*
	hkpPhysicsContext* Context;				// Set up a physics context containing the world for use in the Havok visual debugger

public:

	Havok(void);
	~Havok(void);
	static Havok* GetHavok();

	/*
	Initialize Havok by allocating 1mb of memory for Havok. Also sets up the physics world info, registers all collision agents, and sets up collision group filters
	*/
	bool InitHavok(void);

	/* Initailize the Havok Visual Debugger.
	   This should not be included with GOLD release version.
	*/
	void SetupHavokVisualDebugger(void);

	/*create a boxshape rigidbody by passing in the x,y,z coords, position and mass of the box as well as its hkpMotionType
	  then add it to the list of rigidbodies in the world object
	*/
	hkpRigidBody* AddBoxShape(float halfWidth, float halfHeight, float halfDepth, D3DXVECTOR3 startPos, float mass, short hkpMotionType);

	/* Creates a capsule rigidbody to represent the player.
	   Then automatically creates the character's state machine with the following states:
	   HK_CHARACTER_ON_GROUND, HK_CHARACTER_IN_AIR, HK_CHARACTER_JUMPING, HK_CHARACTER_CLIMBING
	*/
	hkpRigidBody* CreatePlayerRigidBody( short i, float position[3] );

	/*
	  Create a capsule rigidbody to represent the AI enemy
	*/
	hkpRigidBody* AddEnemy(D3DXVECTOR3 startPos);

	/*Create a tiny sphere rigidbody that will act as a bullet.
	  Simply pass in the start position and bullet direction as D3DXVECTOR3s
	*/
	hkpRigidBody* AddBullet(bool isPlayer, D3DXVECTOR3 startPos, D3DXVECTOR3 bulletDir);

	/*Create a FireGunPhantom for each the players.
	*/
	void CreateFireGunPhantomAABB(short thePlayerNumber);

	/*Call this when the player shoots. It will reposition the singleton phantom created for that player.
	*/
	void ShootFireGun(FireGun* thePlayersGun, short thePlayerNumber);

	void UpdateFireGuns();
	/*
	  Updates all of Havok's relevant info. This includes updating the physics world, the player(s), all contact listeners, and the visual debugger
	*/
	void Update(float dT);
	void UpdatePlayers(float dT);
	void UpdateEnemies(float dT);

	void ResetPlayer( short i );

	CharacterData* GetCharacterData(bool isPlayer, unsigned short i);

	/*
	  Returns a pointer to the physics world
	*/
	hkpWorld* GetPhysicsWorld(void);

	/*
	  Returns a pointer to the contactListener.
	*/
	MyContactStateListener* GetListener(void);

	/*
	  Set gravity to the passed float value. Only needed if the default 9.8f gravity is not wanted
	*/
	void SetGravity(float g);

	bool CheckPlayerSupported( short i, float dt );

	/*
	  Returns a D3DXVECTOR3 representing the player's current position in Havok
	*/
	D3DXVECTOR3 GetCharacterPosition(bool isPlayer, unsigned short i );
	/*
	  Tells Havok this character wants to jump. if isPlayer == false, then I is assumed to be EntityList[i].
	*/
	void Jump(bool isPlayer, unsigned short HavokID );
	/*
	  Tells Havok this character wants to move horizontally. if isPlayer == false, then I is assumed to be EntityList[i].
	*/
	void Move(bool isPlayer, unsigned short HavokID, float speed );

	void setPosition(unsigned short i, D3DXVECTOR3 position);
	/*
	  Tells Havok this character should stop moving. Speed is expected to be ranging from (0.0f = 0%speed) to (1.0f = 100% speed).
	*/
	void StopMovingHorizontal(bool isPlayer, unsigned short HavokID );
	
	void LockMovement(bool isPlayer, unsigned short HavokID, float secsLocked);
	/*
	  Converts the passed in hkVector4 input into the passed in D3DXVECTOR3 output
	*/
	void HkVecToD3DXVec(const hkVector4& input, D3DXVECTOR3& output);
	/*
	  Converts the passed in D3DXVECTOR3 input into the passed in hkVector4 output
	*/
	void D3DXVecToHkVec(const D3DXVECTOR3& input, hkVector4& output);

	void ShutdownHavok(void);

	void SetNumEnemies( short n ){ numEnemies = n; }
};

/*

the following block of code fixes linker errors with Havok by telling it which features to disable
oddly enough, I had to move this code to winmain (which I think means move this to the first place run in the project) to fix the linker

// Keycode
#include <Common/Base/keycode.cxx>

// Productfeatures
// We're using only physics - we undef products even if the keycode is present so
// that we don't get the usual initialization for these products.
#undef HK_FEATURE_PRODUCT_AI
#undef HK_FEATURE_PRODUCT_ANIMATION
#undef HK_FEATURE_PRODUCT_CLOTH
#undef HK_FEATURE_PRODUCT_DESTRUCTION
#undef HK_FEATURE_PRODUCT_BEHAVIOR
#undef HK_FEATURE_PRODUCT_MILSIM
#undef HK_FEATURE_PRODUCT_NEW_PHYSICS

#define HK_EXCLUDE_FEATURE_SerializeDeprecatedPre700
#define HK_EXCLUDE_FEATURE_RegisterVersionPatches
// Vdb needs the reflected classes
//#define HK_EXCLUDE_FEATURE_RegisterReflectedClasses
#define HK_EXCLUDE_FEATURE_MemoryTracker

#include <Common/Base/Config/hkProductFeatures.cxx>
*/

