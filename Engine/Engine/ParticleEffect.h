#pragma once
#include "DX9Renderer.h"
#include <vector>
#include <string>

enum PARTICLE_DETAIL_LEVEL { PARTICLE_DETAIL_HIGH, PARTICLE_DETAIL_MEDIUM, PARTICLE_DETAIL_LOW };

inline float GetRandomFloat( float a, float b )
{
	if( a >= b )
		return a;

	float f = (rand()%10001) * 0.0001f;
	return (f*(b-a))+a;
}

struct Particle
{
	D3DXVECTOR3 initialPos;
	D3DXVECTOR3 initialVelocity;
	float		initialSize;
	float		initialTime;
	float		lifeTime;
	float		mass;
	D3DCOLOR	initialColor;

	static IDirect3DVertexDeclaration9* decl;
};

class ParticleEmitter
{
public:
	ParticleEmitter(){}
	bool Init( const D3DXVECTOR3& accel,
		int maxNumParticles, float timePerParticle );

	~ParticleEmitter(){}
	void Shutdown();

	float GetTime();
	void SetTime(float dt);
	const AABB& GetAABB() const;

	void SetWorldMatrix( const D3DXMATRIX& world );
	void AddParticle();
	
	virtual void OnLostDevice();
	virtual void OnResetDevice();

	virtual void InitParticle( Particle& out ) = 0;
	virtual void Update( float dt );
	virtual void Render();

	virtual void AddParticles( short direction, 
		PARTICLE_DETAIL_LEVEL detailLevel );

	void EnableEffect( bool enable );
	bool IsActive();
	void SetOrigin( D3DXVECTOR3& pos );
	void SetDirection( float dir );

protected:
	ID3DXEffect* mFX;
	D3DXHANDLE mhTech;
	D3DXHANDLE mhWVP;
	D3DXHANDLE mhEyePosL;
	D3DXHANDLE mhTex;
	D3DXHANDLE mhTime;
	D3DXHANDLE mhAccel;
	D3DXHANDLE mhViewportHeight;

	std::string filename;
	std::string techname;
	std::string texname;

	IDirect3DTexture9* mTex;
	IDirect3DVertexBuffer9* mVB;
	D3DXMATRIX mWorld;
	D3DXMATRIX mInvWorld;
	float mTime;
	D3DXVECTOR3 mAccel;
	AABB mBox;
	int mMaxNumParticles;
	float mTimePerParticle;
	float timeAccum;

	bool Active;
	D3DXVECTOR3 origin;
	float direction;

	std::vector<Particle> mParticles;
	std::vector<Particle*> mAliveParticles;
	std::vector<Particle*> mDeadParticles;
};

