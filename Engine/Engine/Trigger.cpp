#include "Trigger.h"


Trigger::Trigger( char* filename, D3DXVECTOR3 position, 
				 D3DXVECTOR3 rotation, float Scale )
{
	Graphic = 0;
	DX9Render::GetRenderer()->Load3DGameComponent( &Graphic, filename );

	Sphere = 0;
	DX9Render::GetRenderer()->LoadSphere(&Sphere, 1.0f, 5, 5);

	Graphic->pos = position;
	Graphic->rot = rotation;
	Graphic->scale = D3DXVECTOR3(Scale, Scale, Scale);

	Sphere->pos = position;
	Sphere->pos.y += 1.0f;
	Sphere->rot = rotation;
	Sphere->scale = D3DXVECTOR3(1.0f, 1.0f, 1.0f);
	this->Active = false;
}

Trigger::~Trigger()
{
	if( Graphic )
		Graphic->Shutdown();
	Graphic = 0;

	if( Sphere )
		Sphere->Shutdown();
	Sphere = 0;
}

void Trigger::Update()
{
	Active = false;
	for( short i = 0; i < MAX_NUM_PLAYERS; ++i )
	{
		if( D3DXVec3Length( &(EntityManager::GetEntityManager()->EntityList[i]->pos - Sphere->pos) ) < collideDist )
		{
			Active = true;
			break;
		}
	}
}

void Trigger::Render( Light* light )
{
	if( light )
		DX9Render::GetRenderer()->Render3DComponent( &Graphic, light, true );
	else
		DX9Render::GetRenderer()->Render3DComponent( &Graphic );
	//DX9Render::GetRenderer()->RenderPrimitive(&Sphere, true);
}

bool Trigger::IsActive()
{
	return Active;
}