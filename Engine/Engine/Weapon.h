#pragma once
#include "SoundLoader.h"

enum ElementType {NONE, FREEZE, POISON};
enum GUNTYPE {	 PISTOL = 0, ASSAULT = 1, SHOTGUN = 2, FIREGUN = 3, FREEZEGUN = 4, ACIDGUN = 5, ENEMYSHOTGUN = 6 };
enum GUNDMG { PISTOLDMG = 20, ASSAULTDMG = 10, SHOTGUNDMG = 35, FIREGUNDMG = 25, FREEZEGUNDMG = 30, ACIDGUNDMG = 10, ACIDTICKDMG = 15, TOUCHENEMYDMG = 10, ENEMYSHOTGUNDMG = 10};
//enum GRENDADETYPE
class Weapon
{
	friend class Entity;
	friend class Havok;

protected:
	GUNTYPE GunType;
	short Ammo;
	short Clip;		//the current number of bullets in the clip
	short Clipsize;	//max
	short Damage;
	float Range;	
	float CoolDown;
	float CoolDownRemaining; //this will be set to weapon::coolDown when the weapon empties its clipsize and needs to reload
	float RateOfFire;
	float ROFTimer;
	bool CanShoot;	

public:
	Weapon(void)
	{
		Ammo = 0;
		Clipsize = -1;
		Clip = Clipsize;
		Damage = 0;
		Range = 0;
		CoolDown = 0;
		CoolDownRemaining = 0;
		RateOfFire = 0;
		ROFTimer = 0;
		CanShoot = true;
	}
	~Weapon(void)
	{

	}
	GUNTYPE GetGunType(void)
	{ return GunType; }
	short GetDamage(void)
	{ return Damage; }
	float GetRange(void)
	{ return Range; }
	float GetCoolDownRemaining(void)
	{ return CoolDownRemaining; }
	void Shoot(void);
	void Reload(void);
	void Update(float dt);	//updates coolDownRemaining by subtracting deltatime
	SoundEffect* weaponFire;
};

class Pistol : public Weapon
{
public:
	Pistol(void)
	{
		GunType = PISTOL;
		Ammo = -1;	//-1 signifies the pistol has infinite ammo
		Clipsize = 16;
		Clip = Clipsize;
		Range = 0.5f; //if it goes offscreen, remove it
		Damage = PISTOLDMG;
		CoolDown = 2.5f;
		RateOfFire = 1.75f;
		CanShoot = true;
		weaponFire = SoundLoader::GetInstance()->Load(false,false,"assets\\soundfx\\pistol.mp3");
	}
	~Pistol(void)
	{
	}
};

class Assault : public Weapon
{
public:
	Assault(void)
	{
		GunType = ASSAULT;
		Ammo = 120;
		Clipsize = 30;
		Clip = Clipsize;
		Range = 3.0f;
		Damage = ASSAULTDMG;
		CoolDown = 3.0f;
		RateOfFire = 0.1f;
		weaponFire = SoundLoader::GetInstance()->Load(false,false,"assets\\soundfx\\Assault.ogg");
	}
};

class Shotgun : public Weapon
{
public:
	Shotgun(void)
	{
		GunType = SHOTGUN;
		Ammo = 24;
		Clipsize = 8;
		Clip = Clipsize;
		Range = 2.0f;
		Damage = SHOTGUNDMG;
		CoolDown = 3.0f;
		RateOfFire = 2.0f;
		weaponFire = SoundLoader::GetInstance()->Load(false,false,"assets\\soundfx\\shotgun.mp3");

	}
	~Shotgun(void)
	{
	}
};

class EnemyShotgun : public Weapon
{
public:
	EnemyShotgun(void)
	{
		GunType = ENEMYSHOTGUN;
		Ammo = 24;
		Clipsize = 8;
		Clip = Clipsize;
		Range = 2.0f;
		Damage = ENEMYSHOTGUNDMG;
		CoolDown = 3.0f;
		RateOfFire = 2.0f;
		weaponFire = SoundLoader::GetInstance()->Load(false,false,"assets\\soundfx\\shotgun.mp3");

	}
	~EnemyShotgun(void)
	{
	}
};


class FireGun :
	public Weapon
{
public:
	//float Tick;
	FireGun(void)
	{
		//The flamethrower is special. It can recharge its clip over time during the coolDown period
		GunType = FIREGUN;
		Ammo = -1;	//it does not have an ammo count since it will regenerate its clip
		Clipsize = 6;
		Clip = Clipsize;
		Range = 3.5f;
		Damage = FIREGUNDMG;
		//Tick = 2;	//how many ticks?
		CoolDown = 10.0f;
		RateOfFire = 0.5f;
		weaponFire = SoundLoader::GetInstance()->Load(false,false,"assets\\soundfx\\FlameThrower.ogg");

	}
	~FireGun(void)
	{
	}
};

class FreezeGun :
	public Weapon
{
public:
	FreezeGun(void)
	{
		GunType = FREEZEGUN;
		Ammo = -1;
		Clipsize = 2;
		Clip = Clipsize;
		Range = 2.0f;
		Damage = FREEZEGUNDMG;
		CoolDown = 5.0f;
		RateOfFire = 1.0f;
		weaponFire = SoundLoader::GetInstance()->Load(false,false,"assets\\soundfx\\Ice.ogg");

	}
	~FreezeGun(void)
	{
	}
};

class AcidGun :
	public Weapon
{
public:
	float Tick;

	AcidGun(void)
	{
		GunType = ACIDGUN;
		Ammo = 20;
		Clipsize = 5;
		Clip = Clipsize;
		Range = 3.5f;
		Damage = ACIDGUNDMG;
		Tick = 25.0f;	//how many ticks?
		CoolDown = 5.0f;
		RateOfFire = 1.0f;
		weaponFire = SoundLoader::GetInstance()->Load(false,false,"assets\\soundfx\\Acid.mp3");		
	}
	~AcidGun(void)
	{
	}	
};
/*
class ConfuseGun ://removed. became black hole
	public Weapon
{
public:
	ConfuseGun(void)
	{
		GunType = CONFUSEGUN;
		Ammo = -1;
		Clipsize = 1;
		Clip = Clipsize;
		Range = 0.0f;//if it goes offscreen, remove it
		Damage = CONFUSEGUNDMG;
		CoolDown = 15.0f;	//confuse effect lasts for 5.0 seconds
		RateOfFire = 1.0f;
		weaponFire = SoundLoader::GetInstance()->Load(false,false,"assets\\soundfx\\pistol 2.mp3");

	}
	~ConfuseGun(void)
	{

	}
};

*/
