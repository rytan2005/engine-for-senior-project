#pragma once

//#include "LuaObject.h"
#include "Renderer.h"
#include "DX9Renderer.h"
#include "Havok.h"
#include "GameTimer.h"
#include "SoundLoader.h"
#include "InputManager.h"
#include "Projectile.h"
#include <vector>
#include <stdio.h>

#include "AIHandler.h"
#include "EntityManager.h"
#include "UserInterface.h"
#include "MovingActor.h"
#include "PlatformManager.h"


using namespace std;

class LuaObject;

class CGameObject
{
private:
	Render* p_Renderer;

	Skybox* skybox;
	Skybox* skybox2;

	//Havok stuff
	GameComponent3D* temp;
	Projectile* tempBullet;
	hkpRigidBody* tempEnemy;

	Camera* p_Camera;

	//Havok Physics
	Havok* p_Havok;

	LuaObject* luaClass;
	GameTimer* p_Timer;

	SoundEffect* mainMenu;
	SoundEffect* inGame;

	InputManager* Input;
	AIHandler* AiHandler;
	float dist;

	float followStrictness;
	bool playMusic;
	bool station;
	bool gameWon;
	bool startMusic;
	bool xy;

	EntityManager* entityManager;
	UserInterface* UI;

	Light* light;
	GameComponent3D* lightMesh;
	PlatformManager* p_Platforms;
	GameComponent3D* weaponStation;

public:
	CGameObject(void);
	~CGameObject(void);

	bool Init( HWND& hwnd, HINSTANCE& hInst );
	void LoadLuaObject(LuaObject* refLState);
	void Update();
	void UpdateBullets();
	void Render();
	void Shutdown();
	
};

