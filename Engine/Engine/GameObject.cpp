#include "GameObject.h"
#include "LuaObject.h"

CGameObject::CGameObject(void)
{
	p_Renderer = 0;

	p_Camera = 0;

	Input = 0;

	AiHandler = 0;

	followStrictness = 6.0f;
	playMusic = false;
	xy = true;

	p_Timer = 0;
}


CGameObject::~CGameObject(void)
{
	SoundLoader::GetInstance()->FlushAll();
}

bool CGameObject::Init( HWND& hwnd, HINSTANCE& hInst )
{
	AudioManager::GetInstance()->Initialize();
	mainMenu = SoundLoader::GetInstance()->LoadBGM("assets/music/mainMenu.mp3");
	inGame = SoundLoader::GetInstance()->LoadBGM("assets/music/inGame.mp3");

	p_Renderer = DX9Render::GetRenderer();
	if( !p_Renderer->Init( hwnd ) )
	{
		MessageBox( 0, "Could Not Initialize Renderer", "Error", MB_OK );
		return false;
	}
	RECT r;
	GetWindowRect( hwnd, &r );
	float width = (float)(r.right - r.left);
	float height = (float)(r.bottom - r.top);
	p_Camera = new Camera(); 
	p_Camera->setLens( D3DX_PI/2.0f, width/height, 0.5f, 1000.0f );
	p_Camera->update();
	p_Renderer->SetCamera( p_Camera );

	UI = UserInterface::GetInstance((short)width,(short)height);
	UI->DisplayLoadingScreen();

	Input = InputManager::GetInputManager();
	Input->Init( &hwnd, &hInst );

	///////////////////////////
	//Init Havok
	///////////////////////////
	p_Havok = 0;
	p_Havok = Havok::GetHavok();
	if( !p_Havok->InitHavok() )
	{
		MessageBox( 0, "Could Not Initialize Havok", "Error", MB_OK );
		return false;
	} 

	entityManager = EntityManager::GetEntityManager();
	if( !entityManager->Init() )
	{
		MessageBox(0, "Failed to create entityManager", "Error", 0 );
		return false;
	}

	p_Platforms = PlatformManager::GetManager();

	luaClass = LuaObject::GetLua();
	luaClass->LoadLuaFile("Assets\\Scripts\\LevelOne.lvl");
	
	entityManager->SetActiveCamera( p_Camera );

	//setup the Havok Visual Debugger - remove this Visual Debugger code from the GOLD release code
	p_Havok->SetupHavokVisualDebugger();

	skybox = 0;
	skybox2 = 0;
	if( !p_Renderer->LoadSkybox( &skybox, "skybox.dds" ) ) return false;
	p_Renderer->SetSkybox( skybox );
	if( !p_Renderer->LoadSkybox( &skybox2, "skybox2.dds" ) ) return false;

	p_Timer = GameTimer::GetTimer();
	p_Timer->StartTimer();

	float p[] = {0.0f, 0.0f, 0.0f};
	float d[] = {1.0f, -1.0f, 0.0f};
	float a[] = {1.0f, 0.1f, 0.10f};
	float amb[] = {0.05f, 0.05f, 0.05f};
	light = new Light( 0, 8.0f, p, d, a, amb );
	p_Renderer->LoadSphere( &lightMesh, 0.2f, 6, 6 );
	entityManager->light = light;

	p_Renderer->Load3DGameComponent(&weaponStation, "Weapon_Station.x");
	weaponStation->pos = p_Platforms->GetCheckpoint();
	weaponStation->pos.y -= 0.5f;
	weaponStation->scale = D3DXVECTOR3(0.025f, 0.025f, 0.025f);
	weaponStation->rot = D3DXVECTOR3(0.0f, 0.0f, 0.0f);

	playMusic = true;
	startMusic = true;
	station = false;
	gameWon = false;

	return true;
}


void CGameObject::LoadLuaObject(LuaObject* refLState)
{
	luaClass = refLState;
}

void CGameObject::Update()
{	
	Input = InputManager::GetInputManager();
	Input->Update();
	p_Timer = GameTimer::GetTimer();
	p_Timer->Update();

	if( Input->CheckKeyboardBufferedKey( DIK_0 ) )
		PlatformManager::GetManager()->SetActiveLight( light );
	if( Input->CheckKeyboardBufferedKey( DIK_9 ) )
		PlatformManager::GetManager()->SetActiveLight( NULL );

	if(UI->Update() == WEAPON_SWITCH)
	{
		if(!station)
			AudioManager::GetInstance()->SetBGMVolume(0.0f);
		else
			AudioManager::GetInstance()->SetBGMVolume(100.0f);
	}

	if( UI->Update() == MAIN_MENU )
	{
		if(playMusic == true)
		{
			AudioManager::GetInstance()->SetBGMVolume(100.0f);
			AudioManager::GetInstance()->PlayBGM(*mainMenu);

			gameWon = false;
			playMusic = false;
		}
	}

		

	
	if( UI->Update() == IN_GAME )
	{
		///////////////
		//Havok Updates
		///////////////
		if(!station)
			station = true;

		if(startMusic)
		{
			playMusic = true;
			startMusic = false;
		}

		Havok::GetHavok()->Update(p_Timer->GetDeltaTime());

		EntityManager::GetEntityManager()->UpdateEntities();

		if( Input->CheckKeyboardBufferedKey( DIK_F ) )
			DX9Render::GetRenderer()->ToggleFullscreen();

		if(playMusic == true && gameWon == false)
		{
			AudioManager::GetInstance()->SetBGMVolume(100);
			AudioManager::GetInstance()->PlayBGM(*inGame);
			playMusic = false;
		}

		if( Input->CheckKeyboardBufferedKey( DIK_1 ) )
			DX9Render::GetRenderer()->SetSkybox( skybox );
		if( Input->CheckKeyboardBufferedKey( DIK_2 ) )
			DX9Render::GetRenderer()->SetSkybox( skybox2 );

		if( Input->CheckKeyboardKey( DIK_LEFT ) )
			light->position[0] -= 5.0f * p_Timer->GetDeltaTime();
		if( Input->CheckKeyboardKey( DIK_RIGHT ) )
			light->position[0] += 5.0f * p_Timer->GetDeltaTime();
		if( Input->CheckKeyboardKey( DIK_UP ) )
			light->position[1] += 5.0f * p_Timer->GetDeltaTime();
		if( Input->CheckKeyboardKey( DIK_DOWN ) )
			light->position[1] -= 5.0f * p_Timer->GetDeltaTime();
		//light->position[0] = p_Camera->pos().x;
		//light->position[1] = p_Camera->pos().y+2;
		light->position[0] = EntityManager::GetEntityManager()->GetActiveCamera()->pos().x;
		light->position[1] = EntityManager::GetEntityManager()->GetActiveCamera()->pos().y;
		//light->position[2] = entityManager->GetActiveCamera()->pos().z;


		lightMesh->pos.x = EntityManager::GetEntityManager()->GetActiveCamera()->pos().x;
		lightMesh->pos.y = EntityManager::GetEntityManager()->GetActiveCamera()->pos().y;
		lightMesh->pos.z = EntityManager::GetEntityManager()->GetActiveCamera()->pos().z;

		PlatformManager::GetManager()->Update();

	}else if(UI->GetState()== RESET_GAME)
	{
		entityManager = EntityManager::GetEntityManager();
		p_Platforms = PlatformManager::GetManager();
		UI->SetState(IN_GAME);
	}
	else if(UI->GetState() == GAME_WIN)
	{
		gameWon = true;
		playMusic = true;
	}
}

void CGameObject::Render()
{
	if( DX9Render::GetRenderer()->BeginRender() )
	{
		if( UI->GetState() == IN_GAME )
		{
			PlatformManager::GetManager()->SetActiveLight( light );
			EntityManager::GetEntityManager()->light = light;

			//render the platforms
			PlatformManager::GetManager()->Render();

			//DX9Render::GetRenderer()->Render3DComponent(&weaponStation, light, true);
			DX9Render::GetRenderer()->Render3DComponent(&weaponStation);

			DX9Render::GetRenderer()->RenderPrimitive( &lightMesh, true );

			EntityManager::GetEntityManager()->RenderEntities();
		}

		UserInterface::GetInstance()->RenderUI();

		DX9Render::GetRenderer()->EndRender();
	}
}

void CGameObject::Shutdown()
{
	delete light;
	SAFE_DELETE(mainMenu);
	SAFE_DELETE(inGame);
	lightMesh->Shutdown();

	p_Platforms->Shutdown();

	entityManager->Shutdown();

	p_Havok->ShutdownHavok();

	delete p_Camera;

	UI->Shutdown();

	if( skybox ) skybox->Shutdown();
	if( skybox2 ) skybox2->Shutdown();

	if( Input ) Input->Shutdown();

	if( p_Timer ) p_Timer->Shutdown();

	luaClass->ShutdownClass();

	weaponStation->Shutdown();
	p_Renderer->Shutdown();
}

