#include "Actor.h"

Actor::Actor( char* filename, D3DXVECTOR3 position,
				float TexU, float TexV,
			 D3DXVECTOR3 rotation, D3DXVECTOR3 scale )
{
	Graphic = 0;

	if( !filename )
	{
		hasTexture = false;
		DX9Render::GetRenderer()->LoadUntexturedCube( &Graphic, scale.x, scale.y, scale.z );

		Graphic->pos = position;
		Graphic->rot = rotation;
	}else
	{
		hasTexture = true;
		DX9Render::GetRenderer()->LoadTexturedCube( &Graphic,
			scale.x, scale.y, scale.z, filename, TexU, TexV );
		Graphic->pos = position;
		Graphic->rot = rotation;
	}
}

Actor::~Actor(void)
{
	/*if( Graphic )
		Graphic->Shutdown();
	if( rBody )
		rBody->removeReference();*/
	//Graphic = 0;
}

void Actor::Render( Light* light )
{
	if( hasTexture )
	{
		if( light )
			DX9Render::GetRenderer()->Render3DComponent( &Graphic, light, true );
		else
			DX9Render::GetRenderer()->Render3DComponent( &Graphic );
	}else
		DX9Render::GetRenderer()->RenderPrimitive( &Graphic, true );
}

void Actor::Shutdown()
{
	if( Graphic )
		Graphic->Shutdown();
	if( rBody )
		Havok::GetHavok()->GetPhysicsWorld()->removeEntity(rBody);
}