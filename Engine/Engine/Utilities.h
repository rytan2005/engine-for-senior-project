#pragma once

//#ifndef H_UTILITIES
//#define H_UTILITIES

// Macro for releasing COM objects easily
#define RELEASE_COM(x) { if(x) { x->Release(); x = 0; } }
#define SAFE_DELETE(x) { if(x) { delete x; x = 0; } }

enum EntityType
{
	ENTITY_PLAYER= 0,
	ENTITY_SMALL_SPIDER= 1,
	ENTITY_ROBOT = 2,
	ENTITY_GUARD = 3,
	ENTITY_COMMANDER = 4,
	ENTITY_JUGGERNAUT = 5,
	ENTITY_BOSS_SPIDER = 6,
	ENTITY_MECH_WARRIOR = 7	
};

// Simple enum to tell which way they are facing
enum Directions
{
	DIR_LEFT,
	DIR_RIGHT
};

//#endif