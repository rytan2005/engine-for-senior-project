#include "Skybox.h"


Skybox::Skybox(void)
{
	m_pTexture = 0;
}


Skybox::~Skybox(void)
{
}

bool Skybox::Load( IDirect3DDevice9* m_pD3DDevice, char* filename )
{
	char file[200];
	strcpy( file, "assets/skyboxes/" );
	strcat( file, filename );
	if( FAILED( D3DXCreateCubeTextureFromFile(
		m_pD3DDevice, file, &m_pTexture ) ) )
	{
		char errorMessage[200];
		strcpy( errorMessage, "Error loading asset: " );
		strcat( errorMessage, file );
		MessageBox( 0, errorMessage, "Error Loading Asset", MB_OK );

		return false;
	}

	return true;
}

void Skybox::Shutdown()
{
	m_pTexture->Release();
	m_pTexture = 0;
	delete this;
}

IDirect3DCubeTexture9* Skybox::GetTexture()
{
	return m_pTexture;
}