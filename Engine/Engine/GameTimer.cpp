#include "GameTimer.h"


GameTimer* GameTimer::timerObj = 0;
GameTimer* GameTimer::GetTimer()
{
	if( !timerObj )
		timerObj = new GameTimer();

	return timerObj;
}

GameTimer::GameTimer()
{
	timer = 0;
	prevTime= 0;
	timeDiff = 0;

	currTimeStamp = 0;
	prevTimeStamp = 0;
	deltaTime = 0;
	secsPerCnt = 0;

	fpsTimer = 0;
	fps = 0;
	frameCounter = 0;
}

void GameTimer::StartTimer()
{
	__int64 cntsPerSec = 0;
	QueryPerformanceFrequency( (LARGE_INTEGER*)&cntsPerSec );
	secsPerCnt = 1.0f / (float)cntsPerSec;
	QueryPerformanceCounter( (LARGE_INTEGER*)&prevTimeStamp );
}

void GameTimer::Update()
{
	QueryPerformanceCounter( (LARGE_INTEGER*)&currTimeStamp );
	deltaTime = ( currTimeStamp - prevTimeStamp ) * secsPerCnt;
	prevTimeStamp = currTimeStamp;
	
	if( deltaTime > 0.025f )
		deltaTime = 1.0f/60.0f;

	prevTime = timer;
	timer = (short)timeGetTime();
	timeDiff = timer - prevTime;

	++frameCounter;
	fpsTimer += deltaTime;
	if( fpsTimer > 1.0f )
	{
		fps = frameCounter;
		frameCounter = 0;
		fpsTimer = 0.0f;
	}
}

float GameTimer::GetDeltaTime()
{
	return deltaTime;
}

short GameTimer::GetElapsedMilliseconds()
{
	return timeDiff;
}

short GameTimer::GetFPS()
{
	return fps;
}

void GameTimer::Shutdown()
{
	if( timerObj )
		delete timerObj;
	timerObj = 0;
}