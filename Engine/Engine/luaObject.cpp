#include "LuaObject.h"

LuaObject* LuaObject::luaObjectInstance = NULL;

// Constructors/Destructor

LuaObject::LuaObject()
{
}
LuaObject::~LuaObject(void)
{

}

LuaObject* LuaObject::GetLua()
{
	if( !luaObjectInstance )
	{
		luaObjectInstance = new LuaObject();
		luaObjectInstance->LState = NULL;
	}
	return luaObjectInstance;
}

void LuaObject::LoadLuaFile(char* filename)
{
	LState = lua_open();
	luaL_openlibs(LState);

	lua_register(LState, "AddPlatform", AddPlatform);
	lua_register(LState, "AddDoorTrigger", AddDoorTrigger);
	lua_register(LState, "AddEntity", AddEntity);
	lua_register(LState, "SetCheckPoint", SetCheckPoint);
	lua_register(LState, "SetEndPoint", SetEndPoint);

	luaL_loadfile(LState, filename);
	luaL_dofile(LState, filename);

	lua_settop(LState, 0);

	lua_close(LState);

}

void LuaObject::ShutdownLua(void)
{
	lua_close(LState);
}

void LuaObject::ShutdownClass(void)
{
	/*	if(luaObjectInstance)
	{
	delete luaObjectInstance;
	luaObjectInstance = NULL;
	}*/
}



EntityType LuaObject::cycleThroughTypes(short typeNum)
{
	switch(typeNum)
	{
	case 0:
		return ENTITY_PLAYER;
		break;
	case 1:
		return ENTITY_SMALL_SPIDER;
		break;
	case 2:
		return ENTITY_ROBOT;
		break;
	case 3:
		return ENTITY_GUARD;
		break;
	case 4:
		return ENTITY_COMMANDER;
		break;
	case 5:
		return ENTITY_JUGGERNAUT;
		break;
	case 6:
		return ENTITY_BOSS_SPIDER;
		break;
	case 7:
		return ENTITY_MECH_WARRIOR;
		break;
	default:
		return ENTITY_GUARD;
		break;
	}
	return ENTITY_PLAYER;
}

int LuaObject::AddPlatform(lua_State *luaState)
{
	// gets the number of arguments entered in lua
	int args = lua_gettop(luaState);

	if (args != 9) 
		return luaL_error(luaState, "Got %d arguments expected 9", args); 

	char* texFile;
	float halfWidth, halfHeight, halfDepth, xPos,yPos,zPos, texU, texV;

	halfWidth = (float)lua_tonumber(luaState, -9);
	halfHeight = (float)lua_tonumber(luaState, -8);

	halfDepth = (float)lua_tonumber(luaState, -7);
	xPos = (float)lua_tonumber(luaState, -6);
	yPos = (float)lua_tonumber(luaState, -5);

	zPos = (float)lua_tonumber(luaState, -4);

	texU = (float)lua_tonumber(luaState, -3);
	texV = (float)lua_tonumber(luaState, -2);
	texFile = (char*)lua_tostring(luaState, -1);

	PlatformManager* p  = PlatformManager::GetManager();
	//luaObjectInstance->refGameObject->AddPlatform(halfWidth, halfHeight, halfDepth, D3DXVECTOR3(xPos, yPos, zPos), texFile, texU, texV);
	PlatformManager::GetManager()->AddPlatform(
		halfWidth, halfHeight, halfDepth,
		D3DXVECTOR3( xPos, yPos, zPos ), texFile,
		texU, texV );

	return 0;
}

int LuaObject::AddDoorTrigger(lua_State* luaState)
{
	// gets the number of arguments entered in lua
	int args = lua_gettop(luaState);

	float posSX, posSY, posSZ;				// Position of the Switch
	float posDX, posDY, posDZ;				// Position of the Door
	float scaleS, scaleX, scaleY, scaleZ;					// Scale of the Entity

	if (args == 10)
	{
		// Get the variables loaded for the switch
		posSX = (float)lua_tonumber(luaState, -10);
		posSY = (float)lua_tonumber(luaState, -9);
		posSZ = (float)lua_tonumber(luaState, -8);
		scaleS = (float)lua_tonumber(luaState, -7);

		// Get the variables loaded for the door
		posDX = (float)lua_tonumber(luaState, -6);
		posDY = (float)lua_tonumber(luaState, -5);
		posDZ = (float)lua_tonumber(luaState, -4);
		scaleX = (float)lua_tonumber(luaState, -3);
		scaleY = (float)lua_tonumber(luaState, -2);
		scaleZ = (float)lua_tonumber(luaState, -1);

		PlatformManager::GetManager()->AddDoorTrigger(D3DXVECTOR3(posSX, posSY, posSZ), scaleS, D3DXVECTOR3(posDX, posDY, posDZ), D3DXVECTOR3(scaleX, scaleY, scaleZ));
	}
	else
	{
		return luaL_error(luaState, "Got %d arguments expected 10", args); 
	}

	return 0;
}

/* Entity Managing */

int LuaObject::AddEntity(lua_State* luaState)
{
	// gets the number of arguments entered in lua
	int args = lua_gettop(luaState);

	char* fileName;						// Name of file to use for model

	float posX, posY, posZ;				// Position of the Entity
	float rotX, rotY, rotZ;				// Rotation of the Entity
	float offsetX, offsetY, offsetZ;	// Offset of the Entity
	float scale;						// Scale of the Entity

	float patrolRadius;

	short entityType;				// Needed to define what type of Entity to create


	if (args == 13)
	{
		entityType = (short)lua_tonumber(luaState, -13);
		posX = (float)lua_tonumber(luaState, -12);
		posY = (float)lua_tonumber(luaState, -11);
		posZ = (float)lua_tonumber(luaState, -10);

		rotX = (float)lua_tonumber(luaState, -9);
		rotY = (float)lua_tonumber(luaState, -8);
		rotZ = (float)lua_tonumber(luaState, -7);

		scale = (float)lua_tonumber(luaState, -6);

		offsetX = (float)lua_tonumber(luaState, -5);
		offsetY = (float)lua_tonumber(luaState, -4);
		offsetZ = (float)lua_tonumber(luaState, -3);

		patrolRadius = (float)lua_tonumber(luaState, -2);
		fileName = (char*)lua_tostring(luaState, -1);

		EntityManager::GetEntityManager()->AddEntity(fileName, D3DXVECTOR3(posX, posY, posZ), D3DXVECTOR3(rotX, rotY, rotZ), scale, D3DXVECTOR3(offsetX, offsetY, offsetZ), patrolRadius);
	}
	else if(args == 4)
	{
		entityType = (short)lua_tonumber(luaState, -4);
		posX = (float)lua_tonumber(luaState, -3);
		posY = (float)lua_tonumber(luaState, -2);
		posZ = (float)lua_tonumber(luaState, -1);

		EntityManager::GetEntityManager()->AddEntity((EntityType)entityType, D3DXVECTOR3(posX, posY, posZ));
	}
	else
	{
		return luaL_error(luaState, "Got %d arguments expected 4 or 13", args); 
	}

	return 0;
}

int LuaObject::SetCheckPoint(lua_State* LState)
{
	// gets the number of arguments entered in lua
	int args = lua_gettop(LState);

	float posX, posY, posZ;				// Position of the Entity

	if(args == 3)
	{
		posX = (float)lua_tonumber(LState, -3);
		posY = (float)lua_tonumber(LState, -12);
		posZ = (float)lua_tonumber(LState, -1);

		PlatformManager::GetManager()->setCheckpoint(D3DXVECTOR3(posX, posY, posZ));
	}
	else
	{
		return luaL_error(LState, "Got %d arguments expected 3", args); 
	}		

	return 0;
}

int LuaObject::SetEndPoint(lua_State* LState)
{
		// gets the number of arguments entered in lua
	int args = lua_gettop(LState);

	float posX, posY, posZ;				// Position of the Entity

	if(args == 3)
	{
		posX = (float)lua_tonumber(LState, -3);
		posY = (float)lua_tonumber(LState, -12);
		posZ = (float)lua_tonumber(LState, -1);

		PlatformManager::GetManager()->setEndpoint(D3DXVECTOR3(posX, posY, posZ));
	}
	else
	{
		return luaL_error(LState, "Got %d arguments expected 3", args); 
	}		

	return 0;
}