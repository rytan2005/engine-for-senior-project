#pragma once

#include <d3d9.h>
#include <d3dx9.h>
#pragma comment (lib, "d3d9.lib")
#pragma comment (lib, "d3dx9.lib")

class Skybox
{
private:
	IDirect3DCubeTexture9*	m_pTexture;

private:
	Skybox(void);
	~Skybox(void);

	bool Load( IDirect3DDevice9* m_pD3DDevice, char* filename );

public:
	void Shutdown();

private:
	IDirect3DCubeTexture9* GetTexture();

	friend class DX9Render;
};

