#pragma once

#include <d3d9.h>
#include <d3dx9.h>
#include <string>

#include "SkinnedMesh.h"
#include "Camera.h"
using namespace std;


#define SAFE_RELEASE(x){ if(x){ x->Release(); x = 0; } }


enum CHARACTER_STATE{ STATE_IDLE, STATE_WALKING, STATE_RUNNING, STATE_PATROL, STATE_ATTACK_RANGED, STATE_ATTACK_MELEE, STATE_SEEK, STATE_DYING, STATE_JUMP };
enum WEAPON_TYPE{ WEAPON_MELEE, WEAPON_PROJECTILE };


enum GAME_STATE{ LOADING, MAIN_MENU, PAUSED, WEAPON_SWITCH, CONTROLS, IN_GAME, RESET_GAME, OPTIONS, EXIT_GAME, CREDITS, GAME_WIN, GAME_OVER, GAME_OVER_CONTINUE };
enum LEVEL{LEVEL_TUTORIAL,LEVEL_ONE,LEVEL_TWO,LEVEL_THREE};


struct Material 
{
	float ambient[4];
	float diffuse[4];
	float emissive[4];
	float specular[4];
	float specularPower;
};

struct GameComponent2D
{
	float rotation;
	D3DXVECTOR2 position;
	D3DXVECTOR2 scale;
	IDirect3DTexture9* texture;
	D3DXIMAGE_INFO imageInfo;

	void Shutdown();
};

struct DefaultVertex
{
	DefaultVertex() : position(0,0,0), normal(0,0,0), texCoord(0,0) {}
	DefaultVertex( float x, float y, float z, 
		float xn, float yn, float zn, float u, float v ):
		position( x, y, z ), normal( xn, yn, zn ), texCoord( u, v ){}

	D3DXVECTOR3 position;
	D3DXVECTOR3 normal;
	D3DXVECTOR2 texCoord;

	static IDirect3DVertexDeclaration9* decl;
};

struct AnimVertex
{
	AnimVertex() : position(0,0,0,0), normal(0,0,0), texCoord(0,0) {}
	AnimVertex( float x, float y, float z, 
		float xn, float yn, float zn, float u, float v ):
		position( x, y, z, 0.0f ), normal( xn, yn, zn ), texCoord( u, v ){}

	D3DXVECTOR4 position;
	D3DXVECTOR3 normal;
	D3DXVECTOR2 texCoord;

	static IDirect3DVertexDeclaration9* decl;
};

struct AIComponent
{
	CHARACTER_STATE characterState;
	WEAPON_TYPE weaponType;
	 
	bool AIControlled;

	D3DXVECTOR3 startPos;
	float patrolRadius;
	D3DXVECTOR3 vel;

	D3DXVECTOR2 texCoord;

	float persuitRadius;

	D3DXVECTOR3 pos;
	D3DXVECTOR3 scale;
	D3DXVECTOR3 prevPos;
	D3DXVECTOR3 rot;

	D3DXVECTOR3 offset;

	bool useBoundingBox;
	float radius;
	AABB boundingBox;

	float meleeAttackRange;
	float projectileAttackRange;

	AIComponent();
	void EnableAI( bool enable );
	virtual void ChangeCharaterState( CHARACTER_STATE state );
	void ChangeWeaponType( WEAPON_TYPE type );
	void SetStartPos( D3DXVECTOR3 position, float PatrolRadius, float PersuitRadius, D3DXVECTOR3 velocity );
	void setAttackRanges( float meleeAtackDist, float projectileAttackDist );

	float animTime;
	bool canRemove;

	bool isJumping;
	float jumpTime;

	bool alive;
};



struct GameComponent3D : AIComponent
{
	IDirect3DTexture9** textures;
	ID3DXMesh*	mesh;
	Material* materials;
	DWORD numMaterials;

	void Update();
	void Shutdown();
};

struct GameComponent3DAnimated : AIComponent
{
	short currAnimation;

	SkinnedMesh* skinnedMesh;
	D3DXMATRIX worldMat;

	vector<string> animations;
	vector<string> bones;

	void Update( float dt );
	void Shutdown();

	void ChangeCharaterState( CHARACTER_STATE state );
	void ChangeAnimationTrack( string track );
};

struct AnimationList
{
	string fileName;
	SkinnedMesh* mesh;
	AnimationList* next;
	vector<string> boneNames;
	vector<string> animNames;

	AnimationList();
};

struct Light
{
	Light();
	Light( int Type, float Range, float Position[3],
		float Direction[3], float Attenuation[3], float Ambient[3] );

	int type;
	float range;
	float position[3];
	float direction[3];
	float attenuation[3];
	float ambient[3];
};