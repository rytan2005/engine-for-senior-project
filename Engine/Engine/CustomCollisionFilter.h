#pragma once

#include "Havok.h"

class CustomCollisionFilter
{
public:
	/// Basic groups for game objects.
		enum CustomCollisionGroups
		{
			/// collides always with everything (no layer collision detection used).
			LAYER_NONE = 0,

			/// collides with everything but LAYER_KEYFRAME.
			LAYER_STATIC,

			/// collides with everything.
			LAYER_DYNAMIC,

			/// collides with everything, except the player's bullets
			LAYER_PLAYER,

			/// collides with everything, except their enemy bullets
			LAYER_ENEMY,

			/// collides with everything but LAYER_KEYFRAME, LAYER_STATIC (good for keyframe animation).
			LAYER_KEYFRAME,

			/// collides with everything but the player
			LAYER_PLAYER_BULLET,

			/// collides with everything but enemies controlled by AI
			LAYER_ENEMY_BULLET,

			// collides only with enemies
			LAYER_PLAYER_ATTACK_PHANTOM
		};
		
		/*
		  Initialize the collision group filter so a player's bullets do not hit the player, etc.
		*/
		static void HK_CALL setupGroupFilter( hkpGroupFilter* filter )
		{
			hkUint32 allBits = (1<<LAYER_STATIC) | (1<<LAYER_DYNAMIC) | (1<<LAYER_PLAYER) | (1<<LAYER_ENEMY) | (1<<LAYER_KEYFRAME) |(1<<LAYER_PLAYER_BULLET) | (1<<LAYER_ENEMY_BULLET);
			filter->enableCollisionsUsingBitfield ( allBits, allBits );				//enable collisions between all the above mentioned layers with each other
			filter->enableCollisionsUsingBitfield ( 1<<LAYER_NONE, 0xffffffff );	//enable collision between LAYER_NONE and everything else
			filter->enableCollisionsUsingBitfield ( 0xffffffff, 1<<LAYER_NONE );	//enable collisions between every layer and LAYER_NONE

			filter->disableCollisionsBetween( LAYER_KEYFRAME,     LAYER_KEYFRAME);  //no not let keyframed objects collide with each other
			filter->disableCollisionsBetween( LAYER_KEYFRAME,     LAYER_STATIC );	//do not let keyframed objects collide with static objects (makes a trigger volume not trigger off the presence of a wall)
			//do not let a player's bullets hit the player
			filter->disableCollisionsBetween( LAYER_PLAYER_BULLET,  LAYER_PLAYER );	
			filter->disableCollisionsBetween( LAYER_PLAYER_BULLET,  LAYER_PLAYER_BULLET);
			filter->disableCollisionsBetween( LAYER_PLAYER, LAYER_PLAYER_BULLET);
			//do not let an enemy's bullets hit the enemies
			filter->disableCollisionsBetween( LAYER_ENEMY_BULLET, LAYER_ENEMY);
			filter->disableCollisionsBetween( LAYER_ENEMY, LAYER_ENEMY_BULLET);

			//only let LAYER_PLAYER_ATTACK_PHANTOM collide with LAYER_ENEMY
			/*filter->disableCollisionsBetween( LAYER_PLAYER_ATTACK_PHANTOM, LAYER_STATIC);
			filter->disableCollisionsBetween( LAYER_PLAYER_ATTACK_PHANTOM, LAYER_DYNAMIC);
			filter->disableCollisionsBetween( LAYER_PLAYER_ATTACK_PHANTOM, LAYER_PLAYER);
			filter->disableCollisionsBetween( LAYER_PLAYER_ATTACK_PHANTOM, LAYER_KEYFRAME);
			filter->disableCollisionsBetween( LAYER_PLAYER_ATTACK_PHANTOM, LAYER_PLAYER_BULLET);
			filter->disableCollisionsBetween( LAYER_PLAYER_ATTACK_PHANTOM, LAYER_ENEMY_BULLET);
			filter->disableCollisionsBetween( LAYER_PLAYER_ATTACK_PHANTOM, LAYER_PLAYER_ATTACK_PHANTOM);	//do not let players' phantoms collide with each other
			*/
			//filter->enableCollisionsBetween( LAYER_PLAYER_ATTACK_PHANTOM, LAYER_ENEMY);
			//filter->enableCollisionsBetween( LAYER_ENEMY, LAYER_PLAYER_ATTACK_PHANTOM);
			
		}
};

