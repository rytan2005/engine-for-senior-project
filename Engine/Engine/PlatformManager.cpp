#include "PlatformManager.h"

PlatformManager* PlatformManager::p_Instance = 0;

PlatformManager* PlatformManager::GetManager()
{
	if( !p_Instance )
	{
		p_Instance = new PlatformManager();
		p_Instance->p_Renderer = DX9Render::GetRenderer();
		p_Instance->platforms.clear();
		p_Instance->light = NULL;
	}
	return p_Instance;
}

PlatformManager::PlatformManager(void)
{
	checkpoint = D3DXVECTOR3(0,0,0);
	endpoint = D3DXVECTOR3(0,0,0);
}

PlatformManager::~PlatformManager(void)
{
}

void PlatformManager::AddPlatform( float width,
	float height, float depth, D3DXVECTOR3 pos, 
	char* texfile, float texU, float texV )
{
	Actor* temp = 0;
	temp = new Actor(texfile, pos, texU, texV,
		D3DXVECTOR3 (0,0,0), D3DXVECTOR3(width, height, depth) );
	
	/*p_Renderer->LoadTexturedCube( &temp->Graphic, width, height,
		depth, texfile, texU, texV );*/
	temp->rBody = Havok::GetHavok()->AddBoxShape( width, height, 
		depth, pos, 0, hkpMotion::MOTION_FIXED );

	temp->Graphic->pos = pos;

	platforms.push_back( temp );
}

void PlatformManager::AddDoorTrigger( D3DXVECTOR3 posTrigger, 
					float scaleTrigger, D3DXVECTOR3 posDoor, D3DXVECTOR3 scaleDoor )
{
	Trigger* tempTrigger = 0;
	MovingActor* tempDoor = 0;

	tempTrigger = new Trigger("switch.x", posTrigger, D3DXVECTOR3(0.0f, 0.0f, 0.0f), scaleTrigger);

	triggers.push_back( tempTrigger );

	tempDoor = new MovingActor("metal2.jpg", posDoor, triggers.back(), 1.0f, 1.0f, scaleDoor);
	movingActors.push_back(tempDoor);

	AddPlatform(0.5f, 0.10, 1.0, D3DXVECTOR3(posTrigger.x, posTrigger.y + 0.025f, 0.0f), "metal5.jpg", 1.0f, 1.0f);
}

void PlatformManager::SetActiveLight( Light* pLight )
{
	this->light = pLight;
}

void PlatformManager::Update()
{
	for(unsigned i = 0; i < triggers.size(); ++i)
		triggers[i]->Update();
	

	for(unsigned i = 0; i < movingActors.size(); ++i)
		movingActors[i]->Update();

}

void PlatformManager::Render()
{
	p_Renderer = DX9Render::GetRenderer();
	for( unsigned i = 0; i < platforms.size(); ++i )
	{
		if( light )
			p_Renderer->Render3DComponent( &platforms[i]->Graphic, light, true );
		else
			p_Renderer->Render3DComponent( &platforms[i]->Graphic );
	}

	for(unsigned i = 0; i < triggers.size(); ++i)
	{
		triggers[i]->Render( light );
		movingActors[i]->Render( light );
	}
}

void PlatformManager::Shutdown()
{
	if( p_Instance )
	{
		hkpWorld* p_World = Havok::GetHavok()->GetPhysicsWorld();
		for( unsigned i = 0; i < platforms.size(); ++i )
			if( platforms[i] )
			{
				p_World->removeEntity(platforms[i]->rBody);
				platforms[i]->rBody = HK_NULL;
				platforms[i]->Shutdown();
			}
		platforms.clear();
		
		while( movingActors.size() > 0 )
		{
			MovingActor* t = movingActors.back();
			movingActors.pop_back();
			t->Shutdown();
			delete t;
		}
		while( triggers.size() > 0 )
		{
			delete triggers.back();
			triggers.pop_back();
		}

		triggers.clear();
		movingActors.clear();

		delete p_Instance;
	}
	p_Instance = NULL;
}