//#ifndef __LUA_INC_H__
//#define __LUA_INC_H__
//
//#include "Assets/LUA/include/lua.hpp"
//extern "C"
//{
//	#include "Assets/LUA/include/lua.h"
//	#include "Assets/LUA/include/lauxlib.h"
//	#include "Assets/LUA/include/lualib.h"
//}
//#pragma comment(lib, "Assets/LUA/lib/lua5.1.lib")
//#pragma comment(lib, "Assets/LUA/lib/lua51.lib")
//
//#endif // __LUA_INC_H__

#pragma once

#include "Assets\\LUA\\include\\lua.hpp"

extern "C"
{
#include "Assets\\LUA\\include\\lualib.h"
#include "Assets\\LUA\\include\\lua.h"
#include "Assets\\LUA\\include\\lauxlib.h"
#include "Assets\\LUA\\include\\luaconf.h"
}

#pragma comment (lib, "Assets\\LUA\\lib\\lua5.1.lib")
#pragma comment (lib, "Assets\\LUA\\lib\\lua51.lib")