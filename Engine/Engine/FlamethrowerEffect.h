#pragma once
#include "ParticleEffect.h"

class FlamethrowerEffect : public ParticleEmitter
{
private:
	static FlamethrowerEffect* instance;
	FlamethrowerEffect(){}
	FlamethrowerEffect( const FlamethrowerEffect& t ){}
	void operator= (const FlamethrowerEffect& t){}

public:
	static FlamethrowerEffect* GetEffect();

	void InitParticle( Particle& out );

	void AddParticles( short direction, 
		PARTICLE_DETAIL_LEVEL detailLevel );

	~FlamethrowerEffect(void){}
};

