#pragma once
#include "ParticleEffect.h"

class AcidEffect : public ParticleEmitter
{
private:
	static AcidEffect* instance;
	AcidEffect(void){}
	AcidEffect( const AcidEffect& t ){}
	void operator= (const AcidEffect& t){}

public:
	static AcidEffect* GetEffect();

	void InitParticle( Particle& out );

	void AddParticles( short direction, 
		PARTICLE_DETAIL_LEVEL detailLevel );

	~AcidEffect(void);
};

