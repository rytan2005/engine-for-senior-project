#include "FreezeEffect.h"


FreezeEffect* FreezeEffect::instance = 0;
FreezeEffect* FreezeEffect::GetEffect()
{
	if( !instance )
	{
		instance = new FreezeEffect();
		instance->filename = "assets/shaders/flamethrower.hlsl";
		instance->techname = "tech0";
		instance->texname = "assets/textures/particles/ice.png";
	}
	return instance;
}

void FreezeEffect::InitParticle( Particle& out )
{
	out.initialPos = origin;

	out.initialTime = mTime;
	out.lifeTime = GetRandomFloat( 0.2f, 0.4f );
	out.initialColor = D3DXCOLOR( 1.0f, 1.0f, 1.0f, 1.0f );
	out.initialSize = GetRandomFloat( 2.0f, 3.0f );
	out.mass = GetRandomFloat( 0.8f, 1.2f );

	out.initialVelocity.x = GetRandomFloat( -3.0f, 3.0f );
	out.initialVelocity.y = GetRandomFloat( -3.0f, 3.0f );
	out.initialVelocity.z = GetRandomFloat( -3.0f, 3.0f );
}