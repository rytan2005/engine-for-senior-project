#pragma once

#include "DX9Renderer.h"
#include "GameTimer.h"
#include "InputManager.h"
#include "SoundLoader.h"
#include "UserInterface.h"
#include "EntityManager.h"

using namespace std;

class WeaponStation
{	
private:
	static WeaponStation* instance;
	WeaponStation(void){}
	~WeaponStation(void){}

	Render* p_Renderer;
	InputManager* p_Input;

	short screenW, screenH;

	// Weapon Switch 
	GameComponent2D* WS_BG;							// Background
	GameComponent2D* weaponSwitchNormal[14];		// Weapon Switch Graphics - Normal State
	GameComponent2D* weaponSwitchSelected[14];		// Weapon Switch Graphics - Selected State
	GameComponent2D* weaponSwitchHighlighted[14];	// Weapon Switch Graphics - Highlighted State
	GameComponent2D* weaponSwitchReadyNormal[2];	// Weapon Switch Graphics - Ready State
	GameComponent2D* weaponSwitchReadySelected[2];	// Weapon Switch Graphics - Ready State
	
	GameComponent2D* weaponSwitchPlayerOne[4];		// Weapon Switch Graphics - Current State Player One
	GameComponent2D* weaponSwitchPlayerTwo[4];		// Weapon Switch Graphics - Current State Player Two

	bool isReady[2];

	SoundEffect* errorSound;


public:
	static WeaponStation* WeaponStation::GetInstance();
	static WeaponStation* GetInstance(short screenWidth, short screenHeight);

	bool Init();
	void Update();
	void RenderStation();
	void Shutdown();

	void SetWeaponStatuses();
	void DoKeyboardInput();
	void DoXboxInput();
};

