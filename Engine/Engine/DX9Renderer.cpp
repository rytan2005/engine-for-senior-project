#include "DX9Renderer.h"
#include "GameTimer.h"

#include "FlamethrowerEffect.h"
#include "AcidEffect.h"
#include "FreezeEffect.h"
#include "BloodEffect.h"

DX9Render* DX9Render::renderer = 0;
DX9Render* DX9Render::GetRenderer()
{
	if( !renderer )
	{
		renderer = new DX9Render();
		renderer->Initialized = false;
		renderer->AnimList = NULL;
	}
	return renderer;
}


bool DX9Render::Init( HWND& hwnd )
{
	if( Initialized )
		return true;

	p_hWnd = &hwnd;

	m_pD3DObject = Direct3DCreate9( D3D_SDK_VERSION );

	RECT r;
	GetWindowRect( *p_hWnd, &r );
	int width = r.right - r.left;
	int height = r.bottom - r.top;

	ZeroMemory( &D3Dpp, sizeof(D3Dpp) );
	D3Dpp.hDeviceWindow					= *p_hWnd;
	D3Dpp.Windowed						= true;
	D3Dpp.AutoDepthStencilFormat		= D3DFMT_D24S8;
	D3Dpp.EnableAutoDepthStencil		= true;
	D3Dpp.BackBufferCount				= 1;
	D3Dpp.BackBufferFormat				= D3DFMT_X8R8G8B8;
	D3Dpp.BackBufferHeight				= height;
	D3Dpp.BackBufferWidth				= width;
	D3Dpp.SwapEffect					= D3DSWAPEFFECT_DISCARD;
	//D3Dpp.PresentationInterval			= D3DPRESENT_INTERVAL_IMMEDIATE;
	D3Dpp.PresentationInterval			= D3DPRESENT_RATE_DEFAULT;
	D3Dpp.Flags							= D3DPRESENTFLAG_DISCARD_DEPTHSTENCIL;
	D3Dpp.FullScreen_RefreshRateInHz	= D3DPRESENT_RATE_DEFAULT;
	D3Dpp.MultiSampleQuality			= 0;
	D3Dpp.MultiSampleType				= D3DMULTISAMPLE_NONE;

	DWORD deviceBehaviorFlags = 0;
	m_pD3DObject->GetDeviceCaps( D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, &m_D3DCaps );

	if( m_D3DCaps.DevCaps & D3DCREATE_HARDWARE_VERTEXPROCESSING )
		deviceBehaviorFlags |= D3DCREATE_HARDWARE_VERTEXPROCESSING;
	else
		deviceBehaviorFlags |= D3DCREATE_SOFTWARE_VERTEXPROCESSING;

	if( m_D3DCaps.DevCaps & D3DDEVCAPS_PUREDEVICE &&
		deviceBehaviorFlags & D3DCREATE_HARDWARE_VERTEXPROCESSING )
		deviceBehaviorFlags |= D3DCREATE_PUREDEVICE;

	if( FAILED( m_pD3DObject->CreateDevice( D3DADAPTER_DEFAULT,
		D3DDEVTYPE_HAL, *p_hWnd, deviceBehaviorFlags, &D3Dpp, &m_pD3DDevice ) ) )
		return false;

	D3DXCreateFont( m_pD3DDevice, 24, 0, FW_BOLD, 0, false, DEFAULT_CHARSET,
		OUT_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_DONTCARE,
		TEXT("Delicious-Roman"), &m_pFont );

	D3DXCreateSprite( m_pD3DDevice, &m_pSprite );

	// create default vertex declaration
	{
		D3DVERTEXELEMENT9 elements[] =
		{
			{0, 0, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_POSITION, 0},
			{0, 12, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_NORMAL, 0},
			{0, 24, D3DDECLTYPE_FLOAT2, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 0},
			D3DDECL_END()
		};
		m_pD3DDevice->CreateVertexDeclaration( elements, &(DefaultVertex::decl) );
	}
	{
		D3DVERTEXELEMENT9 elements[] =
		{
			{0, 0, D3DDECLTYPE_FLOAT4, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_POSITION, 0},
			{0, 16, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_NORMAL, 0},
			{0, 28, D3DDECLTYPE_FLOAT2, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 0},
			{0, 36, D3DDECLTYPE_FLOAT4, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_BLENDWEIGHT, 0},
			{0, 52, D3DDECLTYPE_UBYTE4, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_BLENDINDICES, 0},
			D3DDECL_END()
		};
	}

	// load the default effect
	LPD3DXBUFFER errors = 0;
	if( FAILED( D3DXCreateEffectFromFile( m_pD3DDevice,
		"assets/shaders/defaulteffect.fx", 0, 0, 0, 0, &m_pDefaultEffect, &errors ) ) )
	{
		MessageBox( 0, (char*)errors->GetBufferPointer(), "Error", MB_OK );
		SAFE_RELEASE( errors );
		m_pDefaultEffect = NULL;
	}

	if( FAILED( D3DXCreateEffectFromFile( m_pD3DDevice,
		"assets/shaders/animshader.fx", 0, 0, 0, 0, &m_pAnimEffect, &errors)))
	{
		MessageBox( NULL, (char*)errors->GetBufferPointer(), "Error", MB_OK);
		SAFE_RELEASE( errors );
		m_pAnimEffect = 0;
	}

	///////////////////////////////////////////////////////////////////////////
	///	CREATE THE CUBE MESH TO BE USED
	///////////////////////////////////////////////////////////////////////////
	D3DVERTEXELEMENT9 elems[64];
	UINT numElems;
	DefaultVertex::decl->GetDeclaration(elems, &numElems);
	D3DXCreateMesh(12, 24, D3DXMESH_MANAGED, elems, m_pD3DDevice, &(m_pCubeMesh));

	DefaultVertex* vb = 0;

	m_pCubeMesh->LockVertexBuffer(0, (VOID**)&vb);

	// front face
	vb[0] = DefaultVertex(-1.0f, -1.0f, -1.0f, 0.0f, 0.0f, -1.0f, 0.0f, 1.0f);
	vb[1] = DefaultVertex(-1.0f,  1.0f, -1.0f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f);
	vb[2] = DefaultVertex( 1.0f,  1.0f, -1.0f, 0.0f, 0.0f, -1.0f, 1.0f, 0.0f);
	vb[3] = DefaultVertex( 1.0f, -1.0f, -1.0f, 0.0f, 0.0f, -1.0f, 1.0f, 1.0f);

	// back face
	vb[4] = DefaultVertex(-1.0f, -1.0f,  1.0f, 0.0f, 0.0f,  1.0f, 1.0f, 1.0f);
	vb[5] = DefaultVertex( 1.0f, -1.0f,  1.0f, 0.0f, 0.0f,  1.0f, 0.0f, 1.0f);
	vb[6] = DefaultVertex( 1.0f,  1.0f,  1.0f, 0.0f, 0.0f,  1.0f, 0.0f, 0.0f);
	vb[7] = DefaultVertex(-1.0f,  1.0f,  1.0f, 0.0f, 0.0f,  1.0f, 1.0f, 0.0f);

	// top face
	vb[8] = DefaultVertex(-1.0f,  1.0f, -1.0f, 0.0f, 1.0f,  0.0f, 0.0f, 1.0f);
	vb[9] = DefaultVertex(-1.0f,  1.0f,  1.0f, 0.0f, 1.0f,  0.0f, 0.0f, 0.0f);
	vb[10]= DefaultVertex( 1.0f,  1.0f,  1.0f, 0.0f, 1.0f,  0.0f, 1.0f, 0.0f);
	vb[11]= DefaultVertex( 1.0f,  1.0f, -1.0f, 0.0f, 1.0f,  0.0f, 1.0f, 1.0f);

	// bottom face
	vb[12]= DefaultVertex(-1.0f, -1.0f, -1.0f, 0.0f, -1.0f, 0.0f, 1.0f, 1.0f);
	vb[13]= DefaultVertex( 1.0f, -1.0f, -1.0f, 0.0f, -1.0f, 0.0f, 0.0f, 1.0f);
	vb[14]= DefaultVertex( 1.0f, -1.0f,  1.0f, 0.0f, -1.0f, 0.0f, 0.0f, 0.0f);
	vb[15]= DefaultVertex(-1.0f, -1.0f,  1.0f, 0.0f, -1.0f, 0.0f, 1.0f, 0.0f);

	// left face
	vb[16]= DefaultVertex(-1.0f, -1.0f,  1.0f, -1.0f, 0.0f, 0.0f, 0.0f, 1.0f);
	vb[17]= DefaultVertex(-1.0f,  1.0f,  1.0f, -1.0f, 0.0f, 0.0f, 0.0f, 0.0f);
	vb[18]= DefaultVertex(-1.0f,  1.0f, -1.0f, -1.0f, 0.0f, 0.0f, 1.0f, 0.0f);
	vb[19]= DefaultVertex(-1.0f, -1.0f, -1.0f, -1.0f, 0.0f, 0.0f, 1.0f, 1.0f);

	// right face
	vb[20]= DefaultVertex( 1.0f, -1.0f, -1.0f, 1.0f, 0.0f, 0.0f, 0.0f, 1.0f);
	vb[21]= DefaultVertex( 1.0f,  1.0f, -1.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f);
	vb[22]= DefaultVertex( 1.0f,  1.0f,  1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f);
	vb[23]= DefaultVertex( 1.0f, -1.0f,  1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f);

	m_pCubeMesh->UnlockVertexBuffer();

	WORD* ib = 0;
	DWORD* ab = 0;
	m_pCubeMesh->LockIndexBuffer(0, (VOID**)&ib);
	m_pCubeMesh->LockAttributeBuffer(0, &ab);

	// front face
	ib[0] = 0; ib[1] = 1; ib[2] = 2;		ab[0] = 0;
	ib[3] = 0; ib[4] = 2; ib[5] = 3;		ab[1] = 0;
	
	// back face
	ib[6] = 4; ib[7] = 5; ib[8] = 6;		ab[2] = 0;
	ib[9] = 4; ib[10]= 6; ib[11]= 7;		ab[3] = 0;

	// top face
	ib[12]= 8; ib[13]= 9;  ib[14]= 10;		ab[4] = 0;
	ib[15]= 8; ib[16]= 10; ib[17]= 11;		ab[5] = 0;

	// bottom face
	ib[18] = 12; ib[19] = 13; ib[20] = 14;	ab[6] = 0;
	ib[21] = 12; ib[22] = 14; ib[23] = 15;	ab[7] = 0;

	// left face
	ib[24] = 16; ib[25] = 17; ib[26] = 18;	ab[8] = 0;
	ib[27] = 16; ib[28] = 18; ib[29] = 19;	ab[9] = 0;

	// right face
	ib[30] = 20; ib[31] = 21; ib[32] = 22;	ab[10] = 0;
	ib[33] = 20; ib[34] = 22; ib[35] = 23;	ab[11] = 0;

	m_pCubeMesh->UnlockIndexBuffer();
	m_pCubeMesh->UnlockAttributeBuffer();

	///////////////////////////////////////////////////////////////////////////
	///	SET UP THE SKYBOX PARAMETERS
	///////////////////////////////////////////////////////////////////////////
	m_pSkyboxEffect = 0;
	m_pSkyboxMesh = 0;

	D3DXCreateSphere( m_pD3DDevice, 500.0f, 15, 15, &m_pSkyboxMesh, 0 );
	ID3DXBuffer* envErrors = 0;
	D3DXCreateEffectFromFile( m_pD3DDevice, "assets/shaders/envmap.fx",
		0, 0, 0, 0, &m_pSkyboxEffect, &envErrors );
	if( envErrors )
	{
		MessageBox( 0, (char*)envErrors->GetBufferPointer(), "Skybox Shader Errors", MB_OK );
		SAFE_RELEASE( envErrors );
		return false;
	}
	m_pSkyboxEffect->SetTechnique( "tech0" );

	m_pSkybox = 0;

	camera = 0;

	AnimList = 0;

	Initialized = true;

	return true;
}

void DX9Render::SetCamera( Camera* cam )
{
	camera = cam;
}

bool DX9Render::DeviceLost()
{
	HRESULT hr = m_pD3DDevice->TestCooperativeLevel();

	switch( hr )
	{
	case D3DERR_DEVICELOST:
		Sleep(20);
		return true;
		break;
		
	case D3DERR_DRIVERINTERNALERROR:
		MessageBox( 0, "Internal Driver Error... Exiting", "Error", MB_OK );
		PostQuitMessage(0);
		return true;
		break;

	case D3DERR_DEVICENOTRESET:
		OnLostDevice();
		m_pD3DDevice->Reset( &D3Dpp );
		OnResetDevice();
		return false;
		break;

	default:
		return false;
	};
}

void DX9Render::OnLostDevice()
{
	if( m_pSprite ) m_pSprite->OnLostDevice();
	if( m_pFont ) m_pFont->OnLostDevice();
	if( m_pDefaultEffect ) m_pDefaultEffect->OnLostDevice();
	if( m_pAnimEffect ) m_pAnimEffect->OnLostDevice();
	if( m_pSkyboxEffect ) m_pSkyboxEffect->OnLostDevice();

	FlamethrowerEffect::GetEffect()->OnLostDevice();
	AcidEffect::GetEffect()->OnLostDevice();
	FreezeEffect::GetEffect()->OnLostDevice();
	BloodEffect::GetEffect()->OnLostDevice();
}

void DX9Render::OnResetDevice()
{
	if( m_pSprite ) m_pSprite->OnResetDevice();
	if( m_pFont ) m_pFont->OnResetDevice();
	if( m_pDefaultEffect ) m_pDefaultEffect->OnResetDevice();
	if( m_pAnimEffect ) m_pAnimEffect->OnResetDevice();
	if( m_pSkyboxEffect ) m_pSkyboxEffect->OnResetDevice();

	FlamethrowerEffect::GetEffect()->OnResetDevice();
	AcidEffect::GetEffect()->OnResetDevice();
	FreezeEffect::GetEffect()->OnResetDevice();
	BloodEffect::GetEffect()->OnResetDevice();
}

bool DX9Render::LoadSkybox( Skybox** skybox, char* filename )
{
	if( (*skybox) )
		return true;

	Skybox* sky = new Skybox();
	if( !sky->Load( m_pD3DDevice, filename ) )
		return false;

	(*skybox) = sky;

	return true;
}

void DX9Render::LoadSphere( GameComponent3D** object, float radius, UINT slices, UINT stacks )
{
	if( (*object) != NULL )
		return;

	GameComponent3D* component = new GameComponent3D();
	component->characterState = STATE_IDLE;
	component->AIControlled = true;
	component->SetStartPos( D3DXVECTOR3(0,0,0), 5.0f, 10.0f, D3DXVECTOR3(1,1,1) );
	component->prevPos = component->pos;
	component->setAttackRanges( 1.0f, 5.0f );
	component->weaponType = WEAPON_MELEE;


	component->materials = NULL;
	component->textures = NULL;
	component->numMaterials = 1;

	component->useBoundingBox = false;

	ID3DXMesh* t = 0;
	D3DXCreateSphere( m_pD3DDevice, radius, slices, stacks, &t, NULL );
	UINT numElems = 0;
	D3DVERTEXELEMENT9 e[64];
	DefaultVertex::decl->GetDeclaration(e, &numElems);
	t->CloneMesh( D3DXMESH_SYSTEMMEM, e, m_pD3DDevice, &(component->mesh) );
	SAFE_RELEASE( t );

	component->pos = D3DXVECTOR3( 0.0f, 0.0f, 0.0f );
	component->scale = D3DXVECTOR3( 1.0f, 1.0f, 1.0f );
	component->rot = D3DXVECTOR3( 0.0f, 0.0f, 0.0f );
	component->offset = D3DXVECTOR3( 0, 0, 0 );

	component->radius = radius;

	(*object) = component;
}

void DX9Render::LoadCylinder( GameComponent3D** object, float radius1, float radius2, float length, UINT slices, UINT stacks )
{
	if( (*object) != NULL )
		return;

	GameComponent3D* component = new GameComponent3D();

	component->characterState = STATE_IDLE;
	component->AIControlled = true;
	component->SetStartPos( D3DXVECTOR3(0,0,0), 5.0f, 10.0f, D3DXVECTOR3(1,1,1) );
	component->prevPos = component->pos;
	component->setAttackRanges( 1.0f, 5.0f );
	component->weaponType = WEAPON_MELEE;


	component->materials = NULL;
	component->textures = NULL;
	component->numMaterials = 1;

	component->useBoundingBox = false;

	ID3DXMesh* t = 0;
	D3DXCreateCylinder( m_pD3DDevice, radius1, radius2, length, slices, stacks, &t, NULL );
	UINT numElems = 0;
	D3DVERTEXELEMENT9 e[64];
	DefaultVertex::decl->GetDeclaration(e, &numElems);
	t->CloneMesh( D3DXMESH_SYSTEMMEM, e, m_pD3DDevice, &(component->mesh) );
	SAFE_RELEASE( t );

	component->pos = D3DXVECTOR3( 0.0f, 0.0f, 0.0f );
	component->scale = D3DXVECTOR3( 1.0f, 1.0f, 1.0f );
	component->rot = D3DXVECTOR3( 0.0f, 0.0f, 0.0f );
	component->offset = D3DXVECTOR3( 0, 0, 0 );

	DefaultVertex* tVert = 0;
	component->mesh->LockVertexBuffer(0, (void**)&tVert);
	D3DXVECTOR3 center;
	D3DXComputeBoundingSphere(&tVert[0].position, component->mesh->GetNumVertices(), 
		sizeof(DefaultVertex), &center, &component->radius);
	component->mesh->UnlockVertexBuffer();

	(*object) = component;
}

void DX9Render::LoadUntexturedCube( GameComponent3D** object, float width, float height, float depth )
{
	if( (*object) != NULL )
		return;

	GameComponent3D* component = new GameComponent3D();

	component->characterState = STATE_IDLE;
	component->AIControlled = true;
	component->SetStartPos( D3DXVECTOR3(0,0,0), 5.0f, 10.0f, D3DXVECTOR3(1,1,1) );
	component->prevPos = component->pos;
	component->setAttackRanges( 1.0f, 5.0f );
	component->weaponType = WEAPON_MELEE;


	component->materials = NULL;
	component->textures = NULL;
	component->numMaterials = 1;

	component->useBoundingBox = false;

	ID3DXMesh* t = 0;
	D3DXCreateBox(m_pD3DDevice, width*2.0f, height*2.0f, depth*2.0f, &t, NULL);
	UINT numElems = 0;
	D3DVERTEXELEMENT9 e[64];
	DefaultVertex::decl->GetDeclaration(e, &numElems);
	t->CloneMesh(D3DXMESH_SYSTEMMEM, e, m_pD3DDevice, &(component->mesh));
	SAFE_RELEASE( t );

	component->pos = D3DXVECTOR3( 0.0f, 0.0f, 0.0f );
	component->scale = D3DXVECTOR3( 1.0f, 1.0f, 1.0f );
	component->rot = D3DXVECTOR3( 0.0f, 0.0f, 0.0f );
	component->offset = D3DXVECTOR3( 0, 0, 0 );

	DefaultVertex* tVert = 0;
	component->mesh->LockVertexBuffer(0, (void**)&tVert);
	D3DXVECTOR3 center;
	D3DXComputeBoundingSphere(&tVert[0].position, component->mesh->GetNumVertices(), 
		sizeof(DefaultVertex), &center, &component->radius);
	component->mesh->UnlockVertexBuffer();

	(*object) = component;
}

bool DX9Render::LoadTexturedCube( GameComponent3D** object, float width, float height, float depth, char* texfile, float TexUScale, float TexVScale )
{
	if( (*object) != NULL )
		return true;

	GameComponent3D* component = new GameComponent3D();

	component->texCoord.x = TexUScale * 1.0f;
	component->texCoord.y = TexVScale * 1.0f;

	component->characterState = STATE_IDLE;
	component->AIControlled = true;
	component->SetStartPos( D3DXVECTOR3(0,0,0), 5.0f, 10.0f, D3DXVECTOR3(1,1,1) );
	component->prevPos = component->pos;
	component->setAttackRanges( 1.0f, 5.0f );
	component->weaponType = WEAPON_MELEE;

	component->mesh = m_pCubeMesh;
	component->useBoundingBox = true;
	component->boundingBox.Max = D3DXVECTOR3( width, height, depth );
	component->boundingBox.Min = D3DXVECTOR3( -width, -height, -depth );

	component->numMaterials = 1;

	component->materials = new Material[component->numMaterials];
	
	component->materials[0].ambient[0] = 1.0f;
	component->materials[0].ambient[1] = 1.0f;
	component->materials[0].ambient[2] = 1.0f;
	component->materials[0].ambient[3] = 1.0f;

	component->materials[0].diffuse[0] = 1.0f;
	component->materials[0].diffuse[1] = 1.0f;
	component->materials[0].diffuse[2] = 1.0f;
	component->materials[0].diffuse[3] = 1.0f;
	
	component->materials[0].emissive[0] = 1.0f;
	component->materials[0].emissive[1] = 1.0f;
	component->materials[0].emissive[2] = 1.0f;
	component->materials[0].emissive[3] = 1.0f;
	
	component->materials[0].specular[0] = 1.0f;
	component->materials[0].specular[1] = 1.0f;
	component->materials[0].specular[2] = 1.0f;
	component->materials[0].specular[3] = 1.0f;
	component->materials[0].specularPower = 50.0f;

	component->textures = new IDirect3DTexture9*[component->numMaterials];

	char file[200];
	strcpy_s( file, "assets/textures/" );
	strcat_s( file, texfile );
	if( FAILED( D3DXCreateTextureFromFile( m_pD3DDevice, file, &(component->textures[0])) ) )
	{
		char errorMessage[200];
		strcpy_s( errorMessage, "Error loading asset: " );
		strcat_s( errorMessage, file );
		MessageBox( 0, errorMessage, "Error loading asset", MB_OK );
		PostQuitMessage(0);
		return false;
	}

	component->pos = D3DXVECTOR3( 0.0f, 0.0f, 0.0f );
	//component->scale = D3DXVECTOR3( 1.0f, 1.0f, 1.0f );
	component->scale = D3DXVECTOR3( width, height, depth );
	component->rot = D3DXVECTOR3( 0.0f, 0.0f, 0.0f );
	component->offset = D3DXVECTOR3( 0, 0, 0 );

	DefaultVertex* tVert = 0;
	component->mesh->LockVertexBuffer(0, (void**)&tVert);
	D3DXVECTOR3 center;
	D3DXComputeBoundingSphere(&tVert[0].position, component->mesh->GetNumVertices(), 
		sizeof(DefaultVertex), &center, &component->radius);
	component->mesh->UnlockVertexBuffer();

	(*object) = component;

	return true;
}

bool DX9Render::Load3DGameComponent( GameComponent3D** object, char* filename )
{
	if( (*object) != 0 )
		return true;

	GameComponent3D* component = new GameComponent3D();

	component->characterState = STATE_IDLE;
	component->AIControlled = true;
	component->SetStartPos( D3DXVECTOR3(0,0,0), 5.0f, 10.0f, D3DXVECTOR3(1,1,1) );
	component->prevPos = component->pos;
	component->setAttackRanges( 1.0f, 5.0f );
	component->weaponType = WEAPON_MELEE;

	ID3DXMesh* meshSys = 0;
	ID3DXBuffer* adjBuffer = 0;
	ID3DXBuffer* mtrlBuffer = 0;

	char file[200];
	strcpy_s( file, "assets/models/" );
	strcat_s( file, filename );
	
	// step 1: load the mesh
	if( FAILED( D3DXLoadMeshFromX( file, D3DXMESH_SYSTEMMEM, m_pD3DDevice,
		&adjBuffer, &mtrlBuffer, 0, &(component->numMaterials), &meshSys ) ) )
	{
		char errorMessage[200];
		strcpy_s( errorMessage, "Could not load asset: " );
		strcat_s( errorMessage, file );
		MessageBox( 0, errorMessage, "Error loading asset", MB_OK );
		PostQuitMessage(0);

		delete component;
		(*object) = NULL;
		return false;
	}

	// step 2: find the vertex normals, if they exist
	D3DVERTEXELEMENT9 elems[MAX_FVF_DECL_SIZE];
	meshSys->GetDeclaration( elems );
	bool hasNormals = false;
	for( byte i = 0; i < MAX_FVF_DECL_SIZE; ++i )
	{
		if( elems[i].Stream == 0xff )
			break;

		if( elems[i].Type == D3DDECLTYPE_FLOAT3 &&
			elems[i].Usage == D3DDECLUSAGE_NORMAL &&
			elems[i].UsageIndex == 0 )
		{
			hasNormals = true;
			break;
		}
	}
	
	// step 3: chance the vertex format
	D3DVERTEXELEMENT9 elements[64];
	UINT numElements = 0;
	DefaultVertex::decl->GetDeclaration( elements, &numElements );
	ID3DXMesh* temp = 0;
	meshSys->CloneMesh( D3DXMESH_SYSTEMMEM, elements, m_pD3DDevice, &temp );
	SAFE_RELEASE( meshSys );
	meshSys = temp;

	// step 4: if mesh did not have normals, generate them
	if( hasNormals == false )
		D3DXComputeNormals( meshSys, 0 );

	// step 5: optimize mesh
	meshSys->Optimize( D3DXMESH_MANAGED | D3DXMESHOPT_COMPACT |
		D3DXMESHOPT_ATTRSORT | D3DXMESHOPT_VERTEXCACHE,
		(DWORD*)adjBuffer->GetBufferPointer(), 0, 0, 0, &(component->mesh) );
	SAFE_RELEASE( meshSys );
	SAFE_RELEASE( adjBuffer );

	// step 6: extract materials and textures
	if( mtrlBuffer != 0 && component->numMaterials != 0 )
	{
		D3DXMATERIAL* d3dxmtrls = 
			(D3DXMATERIAL*)mtrlBuffer->GetBufferPointer();

		component->materials = new Material[component->numMaterials];
		component->textures = new IDirect3DTexture9*[component->numMaterials];
		for( DWORD i = 0; i < component->numMaterials; ++i )
		{
			//component->materials[i].MatD3D.Ambient = d3dxmtrls[i].MatD3D.Ambient;
			component->materials[i].ambient[0] = d3dxmtrls[i].MatD3D.Ambient.r;
			component->materials[i].ambient[1] = d3dxmtrls[i].MatD3D.Ambient.g;
			component->materials[i].ambient[2] = d3dxmtrls[i].MatD3D.Ambient.b;
			component->materials[i].ambient[3] = d3dxmtrls[i].MatD3D.Ambient.a;
			//component->materials[i].MatD3D.Diffuse = d3dxmtrls[i].MatD3D.Diffuse;
			component->materials[i].diffuse[0] = d3dxmtrls[i].MatD3D.Diffuse.r;
			component->materials[i].diffuse[1] = d3dxmtrls[i].MatD3D.Diffuse.g;
			component->materials[i].diffuse[2] = d3dxmtrls[i].MatD3D.Diffuse.b;
			component->materials[i].diffuse[3] = d3dxmtrls[i].MatD3D.Diffuse.a;

			component->materials[i].emissive[0] = d3dxmtrls[i].MatD3D.Emissive.r;
			component->materials[i].emissive[1] = d3dxmtrls[i].MatD3D.Emissive.g;
			component->materials[i].emissive[2] = d3dxmtrls[i].MatD3D.Emissive.b;
			component->materials[i].emissive[3] = d3dxmtrls[i].MatD3D.Emissive.a;
			//component->materials[i].MatD3D.Specular = d3dxmtrls[i].MatD3D.Specular;
			component->materials[i].specular[0] = d3dxmtrls[i].MatD3D.Specular.r;
			component->materials[i].specular[1] = d3dxmtrls[i].MatD3D.Specular.g;
			component->materials[i].specular[2] = d3dxmtrls[i].MatD3D.Specular.b;
			component->materials[i].specular[3] = d3dxmtrls[i].MatD3D.Specular.a;
			//component->materials[i].MatD3D.Power = d3dxmtrls[i].MatD3D.Power;
			component->materials[i].specularPower = d3dxmtrls[i].MatD3D.Power;

			if( d3dxmtrls[i].pTextureFilename != 0 )
			{
				ZeroMemory( file, sizeof( 200 ) );
				strcpy_s( file, "assets/textures/" );
				strcat_s( file, d3dxmtrls[i].pTextureFilename );

				if( FAILED( D3DXCreateTextureFromFile( m_pD3DDevice, 
					file, &(component->textures[i]) ) ) )
				{
					char errorMessage[200];
					strcpy_s( errorMessage, "Error loading asset: " );
					strcat_s( errorMessage, file );
					MessageBox( 0, errorMessage, "Error loading asset", MB_OK );
					PostQuitMessage(0);
					return false;
				}
			}else
			{
				component->textures[i] = NULL;
			}
		}
	}

	SAFE_RELEASE( mtrlBuffer );

	component->pos = D3DXVECTOR3( 0.0f, 0.0f, 0.0f );
	component->scale = D3DXVECTOR3( 1.0f, 1.0f, 1.0f );
	component->rot = D3DXVECTOR3( 0.0f, 0.0f, 0.0f );
	component->offset = D3DXVECTOR3( 0, 0, 0 );

	DefaultVertex* tVert = 0;
	component->mesh->LockVertexBuffer(0, (void**)&tVert);
	D3DXVECTOR3 center;
	D3DXComputeBoundingSphere(&tVert[0].position, component->mesh->GetNumVertices(), 
		sizeof(DefaultVertex), &center, &component->radius);
	component->mesh->UnlockVertexBuffer();

	component->useBoundingBox = false;

	(*object) = component;

	return true;
}

bool DX9Render::Load3DGameComponent( GameComponent3DAnimated** object, char* filename)
{
	if( (*object) ) 
		return true;

	GameComponent3DAnimated* component = new GameComponent3DAnimated();

	// check active Animation List
	if( AnimList != NULL )
	{
		if( AnimList->fileName.compare(filename) == 0 )
		{
			component->skinnedMesh = NULL;
			AnimList->mesh->CopyMesh( &component->skinnedMesh );
		}else
		{
			AnimationList* temp = AnimList;

			while( true )
			{
				if( temp->next )
				{
					if( temp->next->fileName.compare(filename) == 0 )
					{
						component->skinnedMesh = NULL;
						temp->next->mesh->CopyMesh( &component->skinnedMesh );
						break;
					}
				}else
				{
					temp->next = new AnimationList();
					temp->next->fileName = filename;
					temp->next->mesh = new SkinnedMesh();
					temp->next->mesh->Load( filename, m_pD3DDevice );
					temp->next->next = NULL;
					temp->next->mesh->GetBoneList( temp->next->boneNames );
					temp->next->mesh->GetAnimations( temp->next->animNames );

					component->skinnedMesh = NULL;
					temp->next->mesh->CopyMesh( &component->skinnedMesh );

					break;
				}

				temp = temp->next;
			}
		}
	}else
	{
		AnimList = new AnimationList();
		AnimList->fileName = filename;
		AnimList->mesh = new SkinnedMesh();
		AnimList->mesh->Load( filename, m_pD3DDevice );
		AnimList->next = NULL;
		AnimList->mesh->GetBoneList( AnimList->boneNames );
		AnimList->mesh->GetAnimations( AnimList->animNames );

		component->skinnedMesh = NULL;
		AnimList->mesh->CopyMesh( &component->skinnedMesh );
	}

	component->characterState = STATE_IDLE;
	component->AIControlled = true;
	component->SetStartPos( D3DXVECTOR3(0,0,0), 5.0f, 10.0f, D3DXVECTOR3(1,1,1) );
	component->prevPos = component->pos;
	component->setAttackRanges( 1.0f, 5.0f );
	component->weaponType = WEAPON_MELEE;
	component->currAnimation = 1;

	component->pos = D3DXVECTOR3( 0.0f, 0.0f, 0.0f );
	component->rot = D3DXVECTOR3( 0.0f, 0.0f, 0.0f );
	component->scale = D3DXVECTOR3( 1, 1, 1 );
	component->offset = D3DXVECTOR3( 0, 0, 0 );

	/*component->skinnedMesh->GetAnimations( component->animations );
	component->skinnedMesh->GetBoneList( component->bones );*/

	/*DefaultVertex* tVert = 0;
	component->skinnedMesh->m_pRootBone->pMeshContainer->MeshData.pMesh->LockVertexBuffer(0, (void**)&tVert);
	D3DXVECTOR3 center;
	D3DXComputeBoundingSphere(&tVert[0].position, component->skinnedMesh->m_pRootBone->pMeshContainer->MeshData.pMesh->GetNumVertices(), 
		sizeof(DefaultVertex), &center, &component->radius);
	component->skinnedMesh->m_pRootBone->pMeshContainer->MeshData.pMesh->UnlockVertexBuffer();*/

	component->useBoundingBox = false;

	(*object) = component;

	return true;
}

bool DX9Render::Load2DGameComponent( GameComponent2D** object, char* filename )
{
	if( (*object) != 0 )
		return true;

	char file[200];
	strcpy_s( file, "assets/UserInterface/" );
	strcat_s( file, filename );

	GameComponent2D* component = new GameComponent2D();
	if( FAILED( D3DXCreateTextureFromFile( m_pD3DDevice, file, &(component->texture) ) ) )
	{
		char errorMessage[200];
		strcpy_s( errorMessage, "Failed to load asset: " );
		strcat_s( errorMessage, file );
		MessageBox( 0, errorMessage, "Error loading asset", MB_OK );
		PostQuitMessage(0);

		delete component;
		(*object) = NULL;
		return false;
	}
	D3DXGetImageInfoFromFile( file, &(component->imageInfo) );
	component->position = D3DXVECTOR2( 0.0f, 0.0f );
	component->scale = D3DXVECTOR2( 1.0f, 1.0f );
	component->rotation = 0.0f;

	(*object) = component;

	return true;
}

void DX9Render::SetSkybox( Skybox* skybox )
{
	m_pSkybox = skybox;

	m_pSkyboxEffect->SetTexture( "tex", m_pSkybox->GetTexture() );
}

bool DX9Render::BeginRender()
{
	if( !m_pD3DDevice || DeviceLost() )
		return false;

	NumRenderCalls = 0;

	if( m_pSkybox )
	{
		if( SUCCEEDED( m_pD3DDevice->BeginScene() ) )
		{
			if( !camera )
			{
				MessageBox( 0, "Camera Not Set.", "Error", MB_OK );
				PostQuitMessage(0);
				return false;
			}
			D3DXMATRIX skyW;
			D3DXVECTOR3 skyP = camera->pos();
			D3DXMatrixTranslation( &skyW, skyP.x, skyP.y, skyP.z );
			m_pSkyboxEffect->SetMatrix( "wvpMat", &(skyW*camera->viewProj()) );

			m_pSkyboxEffect->Begin( 0, 0 );
			m_pSkyboxEffect->BeginPass(0);
			m_pSkyboxMesh->DrawSubset(0);
			m_pSkyboxEffect->EndPass();
			m_pSkyboxEffect->End();

			return true;
		}
		return false;
	}else
	{
		if( SUCCEEDED( m_pD3DDevice->Clear( 0, 0, D3DCLEAR_TARGET | D3DCLEAR_ZBUFFER,
			D3DCOLOR_XRGB( 0, 0, 0 ), 1.0f, 0 ) ) )
			//D3DCOLOR_XRGB( 255, 255, 255 ), 1.0f, 0 ) ) )
		{
			if( SUCCEEDED( m_pD3DDevice->BeginScene() ) )
			{
				m_pD3DDevice->SetVertexDeclaration( DefaultVertex::decl );
				if( !camera )
				{
					MessageBox( 0, "Camera Not Set.", "Error", MB_OK );
					PostQuitMessage(0);
					return false;
				}
				m_pDefaultEffect->SetFloatArray( "camPos", (float*)camera->pos(), 3 );
				return true;
			}
			return false;
		}
		return false;
	}
}

void DX9Render::Begin2DRender()
{
	m_pSprite->Begin( D3DXSPRITE_ALPHABLEND );
}

void DX9Render::RenderPrimitive( GameComponent3D** object, bool wireframe ) 
{
	if( (*object) == NULL || m_pDefaultEffect == NULL )
		return;

	m_pD3DDevice->SetVertexDeclaration( DefaultVertex::decl );

	GameComponent3D* toRender = (*object);

	D3DXVECTOR3 position = D3DXVECTOR3( toRender->pos.x + toRender->offset.x,
		toRender->pos.y + toRender->offset.y, toRender->pos.z + toRender->offset.z );
	if( !camera->IsSphereVisible(&position, toRender->radius) )
		return;

	D3DXMatrixIdentity( &transMat );
	D3DXMatrixIdentity( &rotMat );
	D3DXMatrixIdentity( &worldMat );
	D3DXMatrixIdentity( &scaleMat );

	D3DXMatrixTranslation( &transMat, toRender->pos.x, toRender->pos.y, toRender->pos.z );
	D3DXMatrixRotationYawPitchRoll( &rotMat, toRender->rot.y, toRender->rot.x, toRender->rot.z );
	D3DXMatrixScaling( &scaleMat, toRender->scale.x, toRender->scale.y, toRender->scale.z );
	D3DXMatrixMultiply( &scaleMat, &scaleMat, &transMat );
	D3DXMatrixMultiply( &worldMat, &rotMat, &scaleMat );

	m_pDefaultEffect->SetMatrix( "worldMat", &worldMat );
	worldMat = worldMat * camera->viewProj();
	m_pDefaultEffect->SetMatrix( "WVPmat", &worldMat );

	if( wireframe )
		m_pDefaultEffect->SetTechnique( "primitiveWireframe" );
	else
		m_pDefaultEffect->SetTechnique( "primitiveSolid" );

	UINT numPasses = 0;
	m_pDefaultEffect->Begin(&numPasses, 0);
	for( UINT i = 0; i < numPasses; ++i )
	{
		m_pDefaultEffect->BeginPass(i);
		toRender->mesh->DrawSubset(0);
		m_pDefaultEffect->EndPass();
	}
	m_pDefaultEffect->End();

	++NumRenderCalls;
}

void DX9Render::Render3DComponent( GameComponent3D** object, Light* light, bool lit )
{
	if( (*object) == NULL || m_pDefaultEffect == NULL )
		return;

	m_pD3DDevice->SetVertexDeclaration( DefaultVertex::decl );

	GameComponent3D* toRender = (*object);

	if( toRender->useBoundingBox )
	{
		AABB box = toRender->boundingBox;
		box.Max += toRender->pos;
		box.Min += toRender->pos;
		if( !camera->IsAABBVisible( box ) )
			return;
	}else
	{
		D3DXVECTOR3 position = D3DXVECTOR3( toRender->pos.x + toRender->offset.x,
			toRender->pos.y + toRender->offset.y, toRender->pos.z + toRender->offset.z );
		if( !camera->IsSphereVisible(&position, toRender->radius) )
			return;
	}

	D3DXMatrixIdentity( &transMat );
	D3DXMatrixIdentity( &rotMat );
	D3DXMatrixIdentity( &worldMat );
	D3DXMatrixIdentity( &scaleMat );

	D3DXMatrixTranslation( &transMat, toRender->pos.x,
		toRender->pos.y, toRender->pos.z );
	D3DXMatrixRotationYawPitchRoll( &rotMat, toRender->rot.y,
		toRender->rot.x, toRender->rot.z );
	D3DXMatrixScaling( &scaleMat, toRender->scale.x, toRender->scale.y,
		toRender->scale.z );
	D3DXMatrixMultiply( &scaleMat, &scaleMat, &transMat );
	D3DXMatrixMultiply( &worldMat, &rotMat, &scaleMat );

	m_pDefaultEffect->SetMatrix( "worldMat", &worldMat );
	D3DXMATRIX WIT;
	D3DXMatrixInverse( &WIT, NULL, &worldMat );
	D3DXMatrixTranspose( &WIT, &WIT );
	m_pDefaultEffect->SetMatrix( "WITmat", &WIT );
	worldMat = worldMat * camera->viewProj();
	m_pDefaultEffect->SetMatrix( "WVPmat", &worldMat );

	if( lit && light != NULL )
	{
		m_pDefaultEffect->SetTechnique( "lit" );
		m_pDefaultEffect->SetValue( "light", (void*)light, sizeof( Light ) );
		/*
		m_pDefaultEffect->SetInt( "lighttype", light->type );
		m_pDefaultEffect->SetFloat( "lightrange", light->range );
		m_pDefaultEffect->SetFloatArray( "lightpos", light->position, 3 );
		m_pDefaultEffect->SetFloatArray( "lightdir", light->direction, 3 );
		m_pDefaultEffect->SetFloatArray( "lightatt", light->attenuation, 3 );
		m_pDefaultEffect->SetFloatArray( "lightamb", light->ambient, 3 );*/
	}else
	{
		m_pDefaultEffect->SetTechnique( "unlit" );
	}

	m_pDefaultEffect->SetFloat( "TexUScale", toRender->texCoord.x );
	m_pDefaultEffect->SetFloat( "TexVScale", toRender->texCoord.y );

	UINT numPasses = 0;
	m_pDefaultEffect->Begin( &numPasses, 0 );
	for( UINT i = 0; i < numPasses; ++i )
	{
		m_pDefaultEffect->BeginPass( i );

		for( short j = 0; j < (short)(toRender->numMaterials); ++j )
		{
			//m_pDefaultEffect->SetValue( "mat", (void*)&(component->materials[j]), 62 );
			if( toRender->materials != NULL )
			{
				m_pDefaultEffect->SetFloatArray( "ambient", toRender->materials[j].ambient, 4 );
				m_pDefaultEffect->SetFloatArray( "diffuse", toRender->materials[j].diffuse, 4 );
				m_pDefaultEffect->SetFloatArray( "emissive", toRender->materials[j].emissive, 4 );
				m_pDefaultEffect->SetFloatArray( "specular", toRender->materials[j].specular, 4 );
				m_pDefaultEffect->SetFloat( "specularPower", toRender->materials[j].specularPower );
			}else
			{
				m_pDefaultEffect->SetFloatArray( "ambient", (float*)D3DXVECTOR4( 1.0f, 0.0f, 0.0f, 1.0f ), 4 );
				m_pDefaultEffect->SetFloatArray( "diffuse", (float*)D3DXVECTOR4( 1.0f, 0.0f, 0.0f, 1.0f ), 4 );
				m_pDefaultEffect->SetFloatArray( "emissive", (float*)D3DXVECTOR4( 1.0f, 0.0f, 0.0f, 1.0f ), 4 );
				m_pDefaultEffect->SetFloatArray( "specular", (float*)D3DXVECTOR4( 1.0f, 0.0f, 0.0f, 1.0f ), 4 );
				m_pDefaultEffect->SetFloat( "specularPower", 0 );
			}

			if( toRender->textures != NULL )
			{
				m_pDefaultEffect->SetTexture( "tex", toRender->textures[j] );
			}else
			{
				m_pDefaultEffect->SetTexture( "tex", NULL );
			}

			m_pDefaultEffect->CommitChanges();

			toRender->mesh->DrawSubset( j );
		}

		m_pDefaultEffect->EndPass();
	}
	m_pDefaultEffect->End();

	++NumRenderCalls;
}

void DX9Render::Render3DComponent( GameComponent3DAnimated** object, Light* light, bool lit )
{
	if( !m_pAnimEffect || !(*object) )
		return;

	//GameComponent3DAnimated* toRender = (*object);
	//D3DXVECTOR3 position = D3DXVECTOR3( toRender->pos.x + toRender->offset.x,
	//	toRender->pos.y + toRender->offset.y, toRender->pos.z + toRender->offset.z );
	//if( !camera->IsSphereVisible(&position, toRender->radius) )
	//	return;

	(*object)->skinnedMesh->m_pAnimControl->AdvanceTime(GameTimer::GetTimer()->GetDeltaTime(), NULL);

	/*D3DXMATRIX w;
	D3DXMatrixRotationY( &w, (*object)->skinnedMesh->headAngle );
	LPD3DXFRAME b = D3DXFrameFind( (*object)->skinnedMesh->m_pRootBone, "Bip01_Head" );
	D3DXMatrixMultiply( &b->TransformationMatrix, &w, &b->TransformationMatrix );*/

	(*object)->skinnedMesh->UpdateMatrices(
		(Bone*)(*object)->skinnedMesh->m_pRootBone, &(*object)->worldMat );

	// set up the animated effect parameters
	m_pAnimEffect->SetFloatArray( "ambientLight", (float*)&D3DXVECTOR3(0.0f, 0.0f, 0.0f), 3 );
	m_pAnimEffect->SetFloatArray( "specularLight", (float*)&D3DXVECTOR3(1.0f, 1.0f, 1.0f ), 3 );
	m_pAnimEffect->SetFloatArray( "diffuseLight", (float*)&D3DXVECTOR3( 0.6f, 0.6f, 0.6f ), 3 );
	m_pAnimEffect->SetFloatArray( "lightAttenuation", (float*)D3DXVECTOR3( 0.0f, 0.005f, 0.0f), 3);
	m_pAnimEffect->SetFloatArray( "eyePos", (float*)camera->pos(), 3 );
	m_pAnimEffect->SetFloatArray( "lightPos", (float*)D3DXVECTOR3(0.0f, 0.0f, 0.0f ), 3);

	m_pAnimEffect->SetMatrix( "viewProj", &camera->viewProj() );
	
	// set up the static effect parameters
	m_pDefaultEffect->SetFloatArray( "camPos", (float*)&camera->pos(), 3 );

	if( lit && light != 0 )
	{
		m_pDefaultEffect->SetTechnique( "lit" );
		m_pAnimEffect->SetTechnique( "tech0" );

		m_pDefaultEffect->SetValue( "light", (void*)light, sizeof( Light ) );

		m_pAnimEffect->SetValue( "light", (void*)light, sizeof( Light ) );
	}else
	{
		m_pDefaultEffect->SetTechnique( "unlit" );
		m_pAnimEffect->SetTechnique( "unlit" );
	}

	// call the draw method for the mesh
	D3DXMATRIX vp = camera->viewProj();
	(*object)->skinnedMesh->Render( NULL, m_pAnimEffect, m_pDefaultEffect, vp, m_pD3DDevice );

	++NumRenderCalls;
}

void DX9Render::Render2DComponent( GameComponent2D** object )
{
	
	GameComponent2D* component = (*object);
	if( !component )
		return;

	D3DXVECTOR2 s;
	int power = 0;
	bool xdone = false, ydone = false;
	while( true )
	{
		int result = pow( 2.0f, power );
		if( result >= component->imageInfo.Width && !xdone )
		{
			float t = (float)component->imageInfo.Width / (float)result;
			s.x = t;
			xdone = true;
		}
		if( result >= component->imageInfo.Height && !ydone )
		{
			float t = (float)component->imageInfo.Height / (float) result;
			s.y = t;
			ydone = true;
		}
		power++;

		if( xdone && ydone )
			break;
	}

	/*
		This commented block, will calculate the scale of the image
			when using the default DirectX scaling up to the power of
			2, otherwise, the 2 calls below, will undo, the scale
			to the nearest 2 that DirectX does by default.
	*/
	//D3DXVECTOR2 scaling = component->scale;
	/*D3DXVECTOR2 center = D3DXVECTOR2( 
		component->imageInfo.Width/2.0f * component->scale.x,
		component->imageInfo.Height/2.0f * component->scale.y );*/
	D3DXVECTOR2 scaling = s;
	D3DXVECTOR2 center = D3DXVECTOR2( 
		component->imageInfo.Width/2.0f,
		component->imageInfo.Height/2.0f );
	D3DXVECTOR2 position = component->position - center;

	D3DXMATRIX mat;

	D3DXMatrixTransformation2D( &mat, 0, 0.0f,
		&scaling, &center, component->rotation, &position );

	m_pSprite->SetTransform( &mat );
	m_pSprite->Draw( component->texture, NULL, NULL, NULL,
		D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f) );
}

void DX9Render::RenderText( char* message, float xPos, float yPos )
{
	RECT r;
	r.left = (LONG)xPos;
	r.bottom = (LONG)yPos;
	r.right = (LONG)xPos + 1;
	r.top = (LONG)yPos + 1;

	m_pFont->DrawTextA( NULL, message, -1, &r,
		DT_TOP | DT_LEFT | DT_NOCLIP,
		D3DXCOLOR( 1.0f, 1.0f, 1.0f, 1.0f ) );
}

void DX9Render::End2DRender()
{
	m_pSprite->Flush();
	m_pSprite->End();
}

void DX9Render::EndRender()
{ 
	m_pD3DDevice->EndScene();
	m_pD3DDevice->Present( 0, 0, 0, 0 );
}

void DX9Render::SetFullscreen( bool enable )
{
	if( enable )
	{
		if( !D3Dpp.Windowed )
			return;

		D3Dpp.BackBufferFormat	= D3DFMT_X8R8G8B8;
		D3Dpp.Windowed			= false;

		SetWindowLongPtr( *p_hWnd, GWL_STYLE, WS_POPUP );
		SetWindowPos( *p_hWnd, HWND_TOP, 0, 0,
			D3Dpp.BackBufferWidth, D3Dpp.BackBufferHeight,
			SWP_NOZORDER | SWP_SHOWWINDOW );
	}else
	{
		if( D3Dpp.Windowed )
			return;

		RECT r = {0, 0, D3Dpp.BackBufferWidth, D3Dpp.BackBufferHeight};
		AdjustWindowRect( &r, WS_OVERLAPPEDWINDOW, false );
		D3Dpp.BackBufferFormat	= D3DFMT_UNKNOWN;
		D3Dpp.Windowed			= true;

		SetWindowLongPtr( *p_hWnd, GWL_STYLE, WS_OVERLAPPEDWINDOW );
		SetWindowPos( *p_hWnd, HWND_TOP, 100, 100,
			r.right, r.bottom, SWP_NOZORDER | SWP_SHOWWINDOW );
	}

	OnLostDevice();
	m_pD3DDevice->Reset( &D3Dpp );
	OnResetDevice();
}

void DX9Render::ToggleFullscreen()
{
	if( D3Dpp.Windowed )
	{
		D3Dpp.BackBufferFormat	= D3DFMT_X8R8G8B8;
		D3Dpp.Windowed			= false;

		SetWindowLongPtr( *p_hWnd, GWL_STYLE, WS_POPUP );
		SetWindowPos( *p_hWnd, HWND_TOP, 0, 0,
			D3Dpp.BackBufferWidth, D3Dpp.BackBufferHeight,
			SWP_NOZORDER | SWP_SHOWWINDOW );
	}else
	{
		RECT r = {0, 0, D3Dpp.BackBufferWidth, D3Dpp.BackBufferHeight};
		AdjustWindowRect( &r, WS_OVERLAPPEDWINDOW, false );
		D3Dpp.BackBufferFormat	= D3DFMT_UNKNOWN;
		D3Dpp.Windowed			= true;

		SetWindowLongPtr( *p_hWnd, GWL_STYLE, WS_OVERLAPPEDWINDOW );
		SetWindowPos( *p_hWnd, HWND_TOP, 100, 100,
			r.right, r.bottom, SWP_NOZORDER | SWP_SHOWWINDOW );
	}

	OnLostDevice();
	m_pD3DDevice->Reset(&D3Dpp);
	OnResetDevice();
}

void DX9Render::Shutdown()
{
	if( !renderer )
		return;

	SetFullscreen( false );

	SAFE_RELEASE( m_pCubeMesh );

	ClearAnimationList( AnimList );
	AnimList = 0;

	SAFE_RELEASE( DefaultVertex::decl );

	SAFE_RELEASE( m_pDefaultEffect );
	SAFE_RELEASE( m_pAnimEffect );

	SAFE_RELEASE( m_pSkyboxEffect );
	SAFE_RELEASE( m_pSkyboxMesh );

	SAFE_RELEASE( m_pSprite );
	SAFE_RELEASE( m_pFont );
	SAFE_RELEASE( m_pD3DObject );
	SAFE_RELEASE( m_pD3DDevice );

	delete renderer;
	renderer = false;
}

void DX9Render::ClearAnimationList( AnimationList* animList )
{
	if( animList == 0 )
		return;

	ClearAnimationList( animList->next );

	animList->mesh->Shutdown();
}
