#pragma once

#include "GFx_Kernel.h"
#include "GFx.h"
#include "GFx_Renderer_D3D9.h"
#include "Render/Renderer2D.h"

namespace SF = Scaleform;
using namespace Scaleform;
using namespace Render;
using namespace GFx;

class GFxClass
{
private:
	// One GFxLoader per application
	Loader           gfxLoader;

	// One GFxMovieDef per SWF/GFx file
	Ptr<MovieDef>   pUIMovieDef;

	// One GFxMovieView per playing instance of movie
	Ptr<Movie>  pUIMovie;

	// Scaleform GFx object pointers.
	Ptr<MovieDef>           pMovieDef;
	Ptr<Movie>              pMovie;
	Ptr<Render::D3D9::HAL>  pRenderHAL; 
	Ptr<Render::Renderer2D> pRenderer;

	MovieDisplayHandle        hMovieDisplay;

	D3DPRESENT_PARAMETERS	presentParams;

	// Movie frame time
	DWORD MovieLastTime;

	// Toggle states.
	bool                    Wireframe; 
	bool                    ControlKeyDown;
	bool                    AAEnabled;
	bool                    Paused;         // Set when the movie is paused in the player.    

	// Playback filename and window size.
	String                  FileName;
	int                     Width;
	int                     Height;

	bool                    VerboseAction;

protected:
	// none
public:
	GFxClass(void);
	~GFxClass(void);

	bool InitGFx();
	void OnCreateDevice(IDirect3DDevice9* pd3dDevice);
	void OnResetDevice(IDirect3DDevice9* pd3dDevice);
	void OnLostDevice();
	void OnDestroyDevice();
	void StorePresentParameters(D3DPRESENT_PARAMETERS p);
	void AdvanceAndRender();
	void HandleKeyEvent(unsigned keyCode, bool downFlag);


#if defined(GFX_ENABLE_SOUND)
	virtual Sound::SoundRenderer* GetSoundRenderer();
#endif
};

