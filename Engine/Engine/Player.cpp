#include "Player.h"


// Constructors
Player::Player()
{
	IsPrimary = false;		//start with a pistol out
	Secondary = Pistol();   //instantiate all of the pistol's stats
}

Player::~Player(void)
{
}

//update the player's ammo count, cooldown remaining
void Player::Update(float dt)
{

	//update the weapon's coolDownRemaining
	if(false == IsPrimary)
	{
		Secondary.Update(dt);
		//if you are currently holding a pistol, then only recharge the secondary if its a firegun,freezegun,confusegun
		//it doesnt make sense that you can reload a shotgun/acidgun while you are shooting your pistol.	
		switch ( Primary.GetGunType() )
		{
		case FIREGUN:
			{
				Primary.Update(dt);
			}
			break;
		case FREEZEGUN:
			{
				Primary.Update(dt);
			}
			break;
		case ACIDGUN:
			{
				Primary.Update(dt);
			}
			break;
		default:
			break;
		}
	}
	else //you are holding a big weapon so update it
	{
		Primary.Update(dt);
	}
}

//This method should be called by the player's choice at the weapon station
void Player::SetWeaponTo(GUNTYPE gt)
{
	switch( gt ) //GUNTYPE PISTOL = 0, FIREGUN = 1, FREEZEGUN = 2, ACIDGUN = 3, CONFUSEGUN = 4, SHOTGUN = 5
		{
		case FIREGUN:
			{
			Primary = FireGun();
			}
			break;

		case FREEZEGUN:
			{
				Primary = FreezeGun();
			}
			break;

		case ACIDGUN:
			{
				Primary = AcidGun();
			}
			break;

		case SHOTGUN:
			{
				Primary = Shotgun();
			}
			break;

		default:
			{
				Primary = Pistol();
			}
			break;
		}
}

Weapon* Player::getActiveWeapon()
{
	if(true == IsPrimary)
	{
		return &Primary; 
	}
	else
	{
		return &Secondary;
	}
}

Projectile* Player::Shoot()
{
	Projectile* bullet = 0;

	if(true == CanShoot())
	{
		//update the appropriate gun's ammo count
		if(true == IsPrimary)
			Primary.Shoot();
		else
			Secondary.Shoot();

		//create a projectile with the correct damage and range for this weapon
		//shoot a bullet in the player's current direction
		//bullet = new Projectile(pos, dir, GetGunDamage(),GetGunRange());

		//Play the correct gun sound
		SoundEffect* tempSound = getActiveWeapon()->weaponFire;
		AudioManager::GetInstance()->PlaySFX(*tempSound);

		return bullet;
	}

	return bullet;//(this = 0) because the player cannot shoot right now
}

bool Player::CanShoot()
{/*
	if( true == IsPrimary )
	{
		if( Primary.Clip > 0 )
			return Primary.CanShoot;

		return false;
	}else
	{
		if( Secondary.Clip > 0 )
			return Secondary.CanShoot;
	
		return false;
	}*/
	return true;//delete this
}

short Player::GetGunDamage()
{
	if(true == IsPrimary)
		return Primary.GetDamage();
	else
		return Secondary.GetDamage();
}
float Player::GetGunRange()
{
	if(true == IsPrimary)
		return Primary.GetRange();
	else
		return Secondary.GetRange();
}

float Player::GetGunCoolDownRemaining()
{
	if(true == IsPrimary)
		return Primary.GetCoolDownRemaining();
	else
		return Secondary.GetCoolDownRemaining();
}
