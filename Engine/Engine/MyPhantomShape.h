#pragma once

#include "Havok.h"
#include "EntityManager.h"

class MyPhantomShape : public hkpPhantomCallbackShape
{
public:
	//MyPhantomShape(void);
	//~MyPhantomShape(void);

	virtual void phantomEnterEvent( const hkpCollidable* collidableA, const hkpCollidable* collidableB, const hkpCollisionInput& env )
	{
		//get the body and collisionfilter info
		hkpRigidBody* body0 = hkpGetRigidBody(collidableA);
		hkpRigidBody* body1 = hkpGetRigidBody(collidableB);
		int body0Info = body0->getCollisionFilterInfo();
		int body1Info = body1->getCollisionFilterInfo();

		if(CustomCollisionFilter::LAYER_PLAYER_ATTACK_PHANTOM == body0Info && CustomCollisionFilter::LAYER_ENEMY == body1Info)
		{
			Entity* enemy;
			//get the correct enemy
			enemy = EntityManager::GetEntityManager()->GetEnemyUsingID(body1->getUid());

			//now hurt the enemy with damage equal to this bullet's damage
			enemy->TakeDamage(FIREGUNDMG);
		}
	}

	// hkpPhantom interface implementation
	virtual void phantomLeaveEvent( const hkpCollidable* collidableA, const hkpCollidable* collidableB )
	{
		//get the body and collisionfilter info
		hkpRigidBody* body0 = hkpGetRigidBody(collidableA);
		hkpRigidBody* body1 = hkpGetRigidBody(collidableB);
		int body0Info = body0->getCollisionFilterInfo();
		int body1Info = body1->getCollisionFilterInfo();

		if(CustomCollisionFilter::LAYER_PLAYER_ATTACK_PHANTOM == body0Info && CustomCollisionFilter::LAYER_ENEMY == body1Info)
		{
			//
		}
	}
};

