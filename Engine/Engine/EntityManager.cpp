#include "EntityManager.h"
#include "AIHandler.h"
#include "UserInterface.h"
#include "LuaObject.h"
#include "BloodEffect.h"

EntityManager* EntityManager::instance = 0;

EntityManager* EntityManager::GetEntityManager()
{
	if( !instance )
		instance = new EntityManager();

	return instance;
}

bool EntityManager::Init()
{
	if( !instance )
		return false;

	EntityList.clear();
	EntityList.reserve(MAX_NUM_PLAYERS + MAX_NUM_ENEMIES);
	p_Renderer = DX9Render::GetRenderer();
	p_Input = InputManager::GetInputManager();
	p_Havok = Havok::GetHavok();
	p_Timer = GameTimer::GetTimer();
	p_Camera = 0;
	light = 0;
	aiHandler = AIHandler::GetAIHandler();
	if(!aiHandler->Init())
	{
		MessageBox(0, "failed to create AI Handler","Error", 0);
		return false;
	}

	checkpointReached = false;

	p_Flamethrower = FlamethrowerEffect::GetEffect();
	p_Flamethrower->Init( D3DXVECTOR3( 0.0f, 0.0f, 0.0f ), 200, 0.01f );
	p_Acid = AcidEffect::GetEffect();
	p_Acid->Init( D3DXVECTOR3( 0.0f, 9.8f, 0.0f ), 200, 0.05f );
	p_Acid->EnableEffect( false );
	p_Freeze = FreezeEffect::GetEffect();
	p_Freeze->Init( D3DXVECTOR3(0, 0, 0), 200, 0.02f );
	BloodEffect::GetEffect()->Init( D3DXVECTOR3(0.0f, -9.0f, 0.0f), 2000, 0.05f );

	p_Platforms = PlatformManager::GetManager();
	return true;
}

void EntityManager::SetActiveCamera( Camera* cam )
{
	p_Camera = cam;
}
//add player
void EntityManager::AddEntity( char* fileName, D3DXVECTOR3 pos, D3DXVECTOR3 rot, 
	float scale, D3DXVECTOR3 offset, float patrolRadius )
{
	if( EntityList.size() < MAX_NUM_ENEMIES + MAX_NUM_PLAYERS )
	{
		Entity* comp = new Entity();

		GameComponent3DAnimated* temp = 0;
		p_Renderer->Load3DGameComponent( &temp, fileName );

		comp->skinnedMesh = temp->skinnedMesh;

		comp->scale = D3DXVECTOR3( scale, scale, scale );
		comp->pos = pos;
		comp->rot = rot;
		comp->offset = offset;

		comp->alive = true;
		comp->dir = DIR_LEFT;
		comp->health = PLAYER_HEALTH;

		if( EntityList.size() < MAX_NUM_PLAYERS )
		{
			comp->AIControlled = false;
			comp->entityType = ENTITY_PLAYER;
			float pos[3] = { comp->pos.x, comp->pos.y, comp->pos.z };
			comp->rBody = p_Havok->CreatePlayerRigidBody( EntityList.size(), pos );
			p_Havok->CreateFireGunPhantomAABB(EntityList.size());

			//For now, start the player with a freezegun. remove this once the weapon station has been added and takes care of this
			comp->SetWeaponTo(PISTOL);
			comp->SetWeaponTo(FIREGUN);
			comp->SwitchWeapon();
		}else
		{
			comp->AIControlled = true;
			comp->entityType = ENTITY_GUARD;

			comp->SetStartPos( pos, patrolRadius, 10.0f, D3DXVECTOR3(3,0,0) );
			comp->ChangeWeaponType(WEAPON_MELEE);
			comp->rBody = p_Havok->AddEnemy(comp->pos);
		}

		comp->ID = comp->rBody->getUid();
		EntityList.push_back(comp);
	}
}
//add enemy
void EntityManager::AddEntity(EntityType type, D3DXVECTOR3 pos)
{
	if( EntityList.size() < MAX_NUM_ENEMIES + MAX_NUM_PLAYERS )
	{
		switch (type)
		{
		case ENTITY_GUARD:
			{
				Guard* toAdd = new Guard(pos);
				Entity* obj = (Entity*)toAdd;
				obj->ID = obj->GetRigidBody()->getUid();//set ID to the rigidbody's unique getUid() value
				EntityList.push_back(obj);
			}break;

		}
	}
}

void EntityManager::UpdateEntities()
{
	D3DXVECTOR3 followPos = D3DXVECTOR3( 0, 0, 0 );
	D3DXVECTOR2 minmaxX = D3DXVECTOR2( 0, 0 );
	minmaxX.x = EntityList[0]->pos.x;
	minmaxX.y = EntityList[0]->pos.x;

	float hki = 0.0f;
	short ghh = 0;
	for( short i = 0; i < MAX_NUM_PLAYERS; ++i )
	{
		if( EntityList[i]->alive )
		{
			hki += EntityList[i]->pos.x;
			++ghh;
		}
	}
	p_Camera->pos().x = hki/(float)ghh;

	if( ghh < 2 )
	{
		EntityList[0]->canTakeDamage = false;
		EntityList[1]->canTakeDamage = false;
	}else
	{
		EntityList[0]->canTakeDamage = true;
		EntityList[1]->canTakeDamage = true;
	}

	float numAlivePlayers = 0.0f;
	for( short i = 0; i < (short)EntityList.size(); ++i )
	{
		EntityList[i]->Update( GameTimer::GetTimer()->GetDeltaTime() );
		if( !EntityList[i]->alive )
			continue;

		if (p_Camera->pos().x >= p_Platforms->GetCheckpoint().x && checkpointReached == false)
		{
			checkpointReached = true;
		}
		if (i == 0 || i== 1)
		{
			if( EntityList[i]->pos.y <= -5 )
			{
				EntityList[i]->SetHealth(EntityList[i]->GetMaxHealth());
				Havok::GetHavok()->setPosition(0,D3DXVECTOR3(0,0,0));
				Havok::GetHavok()->setPosition(1,D3DXVECTOR3(0,0,0));
				return;
			}
			if (EntityList[i]->pos.x >= p_Platforms->GetEndpoint().x)
			{
				UserInterface::GetInstance()->SetLevel(LEVEL_ONE);
				UserInterface::GetInstance()->SetState(GAME_WIN);
			}
		}
		else if( EntityList[i]->pos.y <= -5 && EntityList[i]->alive == true)
		{
			EntityList[i]->alive = false;
			break;
		}

		//EntityList[i]->Update(p_Timer->GetDeltaTime());

		if( EntityList[i]->AIControlled )
		{
			Entity* closest = EntityList[0];
			D3DXVECTOR2 toEntity = D3DXVECTOR2( 
				closest->pos.x - EntityList[i]->pos.x,
				closest->pos.y - EntityList[i]->pos.y );
			float dist1 = toEntity.x*toEntity.x + toEntity.y*toEntity.y;
			// find the closest player
			for( short j = 1; j < MAX_NUM_PLAYERS; ++j )
			{
				toEntity = D3DXVECTOR2(
					EntityList[j]->pos.x - EntityList[i]->pos.x,
					EntityList[j]->pos.y - EntityList[i]->pos.y );
				float dist2 = toEntity.x*toEntity.x + toEntity.y*toEntity.y;

				if( dist2 < dist1 )
				{
					dist1 = dist2;
					closest = EntityList[j];
				}
			}

			aiHandler->Update(EntityList[i], closest, EntityList[i]->ID);

			//Get the AI's new position from HAVOK
			D3DXVECTOR3 tempPos = p_Havok->GetCharacterPosition(false, EntityList[i]->ID);
			EntityList[i]->pos = tempPos;

			//check if this AI is trying to shoot
			bool AIShoot = false;
			AIShoot = aiHandler->CheckWantToShoot();

			if(true == AIShoot && EntityList[i]->GetStatus() != FROZEN) //if he's not frozen then he can shoot
			{
				Projectile* tempBullet = 0;
				EntityType debugCheck = EntityList[i]->entityType;
				tempBullet = EntityList[i]->Shoot(); //update the ammo count
				if(tempBullet != 0)
					Bullets.push_back(tempBullet);
			}

		}else
		{
			numAlivePlayers += 1.0f;
			// handle player 
			EntityList[i]->pos = p_Havok->GetCharacterPosition(true,  i );
			(EntityList[i])->Update( p_Timer->GetDeltaTime() );

			followPos += EntityList[i]->pos;
			if( EntityList[i]->pos.x < minmaxX.x )
				minmaxX.x = EntityList[i]->pos.x;
			if( EntityList[i]->pos.x > minmaxX.y )
				minmaxX.y = EntityList[i]->pos.x;

			// check player input
			bool MoveLeft = 0, MoveRight = 0, Jump = 0, Shoot = 0, SwitchWeapon = 0;
			float moveSpeed = 0;	//moveSpeed ranges from -1.0f to 1.0f (suppose to get stick sensitivity)
			if( p_Input->CheckXboxControllerConnected((BYTE)i) )
			{
				if( p_Input->CheckXboxLStickX((BYTE)i) != 0 )
					moveSpeed = (float)p_Input->CheckXboxLStickX((BYTE)i);
				else moveSpeed = 0.0f;
				//shrink down moveSpeed to a range of -1.0f to 1.0f
				moveSpeed /= 100.0f;

				MoveLeft =  moveSpeed < 0 ? true:false;
				MoveRight = moveSpeed > 0 ? true:false;

				Jump = p_Input->CheckXboxBufferedButton((BYTE)i, XBUTTON_A);
				SwitchWeapon = p_Input->CheckXboxBufferedButton((BYTE)i, XBUTTON_Y);
				Shoot = p_Input->CheckXboxRTrigger((BYTE)i) > 10 ? true : false;			
			}else
			{
				moveSpeed = 1.0f;
				if( p_Input->CheckKeyboardKey( DIK_A ) )
				{
					MoveLeft = true;
					MoveRight = false;
					moveSpeed = -1.0f;
				}
				if( p_Input->CheckKeyboardKey( DIK_D ) )
				{
					MoveLeft = false;
					MoveRight = true;
					moveSpeed = 1.0f;
				}
				Jump = p_Input->CheckKeyboardKey(DIK_W);
				SwitchWeapon = p_Input->CheckKeyboardBufferedKey(DIK_TAB);
				
				Shoot = p_Input->CheckKeyboardKey(DIK_SPACE);				
			}

			if( Jump )
			{
				p_Havok->Jump(true,  i );
				//EntityList[i]->ChangeCharaterState( STATE_JUMP );
			}

			if( p_Havok->GetCharacterData( true, i )->lockMovement == false )
			{
				if( MoveLeft )
				{
					EntityList[i]->dir = DIR_LEFT;
					p_Havok->Move(true, i, moveSpeed);
					EntityList[i]->rot = D3DXVECTOR3(EntityList[i]->rot.x, 1.57f, EntityList[i]->rot.z);
					EntityList[i]->ChangeCharaterState(STATE_RUNNING);
				}
				if( MoveRight )
				{
					EntityList[i]->dir = DIR_RIGHT;
					p_Havok->Move(true, i, moveSpeed);
					EntityList[i]->rot = D3DXVECTOR3(EntityList[i]->rot.x, -1.57f, EntityList[i]->rot.z);
					EntityList[i]->ChangeCharaterState(STATE_RUNNING);
				}
			}
			if( !MoveLeft && !MoveRight )
			{
				p_Havok->StopMovingHorizontal(true, i );
				EntityList[i]->ChangeCharaterState(STATE_IDLE);
			}

			if(SwitchWeapon)
			{
				EntityList[i]->SwitchWeapon();
			}

			if( Shoot)
			{
				if(EntityList[i]->getActiveWeapon()->GetGunType() == FIREGUN)
				{
					if(p_Havok->GetCharacterData(true, i)->lockMovement == false && EntityList[i]->getActiveWeapon()->GetCoolDownRemaining() <= 0)
					{
						//spawn the firegun phantom for the 3 seconds that it shoots
						p_Havok->ShootFireGun((FireGun*)EntityList[i]->getActiveWeapon(), i);

						//display the flamethrower particle effect
						//p_Flamethrower->EnableEffect(true);
						EntityList[i]->ChangeCharaterState( CHARACTER_STATE::STATE_IDLE );

						//play the flamethrower sound
						AudioManager::GetInstance()->PlaySFX(*EntityList[i]->getActiveWeapon()->weaponFire);

						//lock the player's movement for 3 seconds while they unload the flamethrower (this flamethrower doesn't work right if you leave movement unlocked)
						p_Havok->LockMovement(true, i, 3.0f);
					}
				}
				else //ordinary gun that shoots bullets
				{
					Projectile* tempBullet = 0;
					//Projectile* shotgunSlug = 0;
					//UPDATE THIS TO SHOOT PRIMARY OR SECONDARY BASED ON THE CONTROLLER INPUT OR KEYBOARD INPUT
					tempBullet = EntityList[i]->Shoot(); //update the ammo count
					if(tempBullet != 0)
						Bullets.push_back(tempBullet);
					/*if(EntityList[i]->getActiveWeapon()->GetGunType() == SHOTGUN)
					{
					for(byte b = 0; b < 7; b++)
					{
					D3DXVECTOR3 shotPos;
					shotPos[0] = tempBullet->GetRigidBody()->getPosition()(0);
					shotPos[1] = tempBullet->GetRigidBody()->getPosition()(1);
					shotPos[2] = tempBullet->GetRigidBody()->getPosition()(2);
					shotgunSlug = new Projectile(true, shotPos, EntityList[i]->dir, NONE, SHOTGUNDMG,tempBullet->GetRange());
					Bullets.push_back(shotgunSlug);
					}
					}*/
				}
			}

		} // end of else statement for players

	} // end of loop

	//now update the bullets
	UpdateBullets();

	//check if players are near the weaponstation and want to use it
	CheckToUseStation();

	followPos /= numAlivePlayers;

	float distance = 0.0f;
	D3DXVECTOR3 currSeparation = D3DXVECTOR3( 0, 0, 0 );

	// moves the players towards each other if they deparation distance becomes too big
	for( short i = 0; i < MAX_NUM_PLAYERS; ++i )
	{
		currSeparation = EntityList[i]->pos - followPos;
		distance = D3DXVec3Length( &currSeparation );

		if( distance > 10.0f )
		{
			if( EntityList[i]->pos.x < followPos.x )
			{
				p_Havok->Move(true, i, 1.0f);//left
				EntityList[i]->pos = p_Havok->GetCharacterPosition(true, i );
			}else
			{
				p_Havok->Move(true, i, -1.0f);//right
				EntityList[i]->pos= p_Havok->GetCharacterPosition(true, i );
			}
		}

		if( !p_Havok->CheckPlayerSupported( i, p_Timer->GetDeltaTime() ) )
		{
			if( EntityList[i]->isJumping == false )
			{
				EntityList[i]->ChangeCharaterState( STATE_JUMP );
				EntityList[i]->isJumping = true;
			}
		}else
		{
			EntityList[i]->isJumping = false;
		}
	}

	float distanceToTarget = ( minmaxX.y - minmaxX.x ) / 4.0f;
	distanceToTarget += 5.0f;
	p_Camera->follow2DXY(followPos, distanceToTarget, 10, p_Timer->GetDeltaTime());
	p_Camera->lookAt(p_Camera->pos(),followPos, D3DXVECTOR3 (0,1,0));


	D3DXVECTOR3 tempPos;
	if( EntityList.size() > 0.0f )
	{
		for(byte i = 0; i < MAX_NUM_PLAYERS; ++i)
		{
			if(EntityList[i]->getActiveWeapon()->GetGunType() == FIREGUN &&  p_Havok->GetCharacterData(true, i)->lockMovement == true)
			{
				p_Flamethrower->SetOrigin( EntityList[i]->pos );
				if( EntityList[i]->dir == Directions::DIR_LEFT )
				{
					p_Flamethrower->SetDirection( -1.0f );
					p_Flamethrower->AddParticles( -1, PARTICLE_DETAIL_LOW );
				}
				if( EntityList[i]->dir == Directions::DIR_RIGHT )
				{
					p_Flamethrower->SetDirection( 1.0f );
					p_Flamethrower->AddParticles( 1, PARTICLE_DETAIL_LOW );
				}

				tempPos = EntityList[i]->pos;
				tempPos.y += 0.3f;
				p_Flamethrower->SetOrigin( tempPos );
			}

		}
	}

	short numPoisoned = 0;
	short numFrozen = 0;
	//count up all the entities currently being poisoned and frozen
	for(byte j = MAX_NUM_PLAYERS; j < EntityList.size(); ++j)
	{
		if( !EntityList[j]->alive )
			continue; 

		if(EntityList[j]->GetStatus() == POISONED)
			numPoisoned++;
		else if(EntityList[j]->GetStatus() == FROZEN)
			numFrozen++;
	}

	/*if( numPoisoned < 1 ) p_Acid->EnableEffect(false);
	else p_Acid->EnableEffect(true);
	if( numFrozen < 1 ) p_Freeze->EnableEffect(false);
	else p_Freeze->EnableEffect(true);*/
	//then update the particle effects for those entities
	for(byte j = MAX_NUM_PLAYERS; j < EntityList.size(); ++j)
	{
		if( !EntityList[j]->alive )
			continue;

		if(EntityList[j]->GetStatus() == POISONED)
		{
			tempPos = EntityList[j]->pos;
			tempPos.y += 0.3f;
			p_Acid->SetOrigin( tempPos );
			p_Acid->AddParticles( 1, PARTICLE_DETAIL_LOW );
		}
		else if(EntityList[j]->GetStatus() == FROZEN)
		{
			tempPos = EntityList[j]->pos;
			tempPos.y += 0.3f;
			p_Freeze->SetOrigin( tempPos );
			p_Freeze->AddParticles( 1, PARTICLE_DETAIL_LOW );
		}		
	}
	p_Acid->Update( GameTimer::GetTimer()->GetDeltaTime() );
	p_Freeze->Update( GameTimer::GetTimer()->GetDeltaTime() );
	p_Flamethrower->Update( GameTimer::GetTimer()->GetDeltaTime() );
	BloodEffect::GetEffect()->Update( GameTimer::GetTimer()->GetDeltaTime() );

	//remove any enemies that got killed by the updated bullets
	ClearDeadEntities();
}

void EntityManager::UpdateBullets()
{
	{
		//update the bullets' positions
		unsigned j = Bullets.size();

		for(unsigned i = 0; i < j; i++)
		{
			hkpWorld* world = Bullets[i]->GetRigidBody()->getWorld();
			float DistTrav = Bullets[i]->GetDistTraveled();
			float Range = Bullets[i]->GetRange();

			if(!p_Camera->IsSphereVisible(&Bullets[i]->GetGC3D()->pos, 2) ) //check if its offscreen, then delete it	
			{
				if( 0 == world )
					continue;

				world->removeEntity(Bullets[i]->GetRigidBody());
				Bullets[i]->Shutdown();
				delete Bullets[i];
				Bullets.erase(Bullets.begin() + i);
				--i;
				--j;
			}
			else if(DistTrav > Range)//the bullet has traveled further than its max range, so remove it
			{
				if( 0 == world )
					continue;

				world->removeEntity(Bullets[i]->GetRigidBody());
				Bullets[i]->Shutdown();
				delete Bullets[i];
				Bullets.erase(Bullets.begin() + i);
				--i;
				--j;
			}
			else if(0 == world )
			{
				Bullets[i]->Shutdown();
				delete Bullets[i];
				Bullets.erase(Bullets.begin() + i);
				--i;
				--j;
			}
			else //the bullet is moving, so update its position
			{
				Bullets[i]->Update();
			}
		}
	}
}

void EntityManager::RenderEntities()
{
	for( short i = 0; i < (short)EntityList.size(); ++i )
	{
		if( !EntityList[i]->alive && EntityList[i]->canRemove )
			continue;

		GameComponent3DAnimated* toRender = EntityList[i];
		if( light == 0 )
		{
			p_Renderer->Render3DComponent( &toRender );
		}else
		{
			p_Renderer->Render3DComponent( &toRender, light, true );
		}
	}

	//render the bullets
	for(unsigned j = 0; j < Bullets.size(); j++)
	{
		GameComponent3D* bulletGC3D = Bullets[j]->GetGC3D();
		p_Renderer->RenderPrimitive(&bulletGC3D, true);
	}

	p_Flamethrower->Render();
	p_Acid->Render();
	p_Freeze->Render();
	BloodEffect::GetEffect()->Render();
}


void EntityManager::ClearDeadEntities()
{
	hkpWorld* p_World = p_Havok->GetPhysicsWorld();

	for( int i = 0; i < (int)EntityList.size(); i++)
	{
		if(!EntityList[i]->alive)
		{
			if(EntityList[i]->entityType == ENTITY_PLAYER)
			{
				if(EntityList[i]->canRemove == true)
				{
					if(checkpointReached)//player reached checkpoint
					{
						//check if can remove
						{
							EntityList[i]->SetHealth(EntityList[i]->GetMaxHealth());
							EntityList[i]->alive = true;
							Havok::GetHavok()->setPosition(0,p_Platforms->GetCheckpoint());
							Havok::GetHavok()->setPosition(1,p_Platforms->GetCheckpoint());
							//clear any existing enemies
							ClearNonPlayers();
							//load lua script to re-add all the enemies.
							LuaObject::GetLua()->LoadLuaFile("Assets\\Scripts\\LevelOneEnemies.Lvl");
							UserInterface::GetInstance()->SetState(GAME_OVER_CONTINUE);
						}
					}
					else //player did not reach checkpoint
					{
						//check if canRemove
						UserInterface::GetInstance()->SetState(GAME_OVER);
					}
				}
			}
			else//enemies
			{				
				p_World->removeEntity(EntityList[i]->GetRigidBody());
				EntityList[i]->Shutdown();
				EntityList.erase(EntityList.begin() + i);
			}
		}
	}
}

void EntityManager::ClearNonPlayers()
{
	hkpWorld* p_World = p_Havok->GetPhysicsWorld();

	while( EntityList.size() > MAX_NUM_PLAYERS )
	{
		byte i = EntityList.size()-1;
		p_World->removeEntity(EntityList[i]->GetRigidBody());
		EntityList[i]->Shutdown();
		EntityList.pop_back();
	}
	Havok::GetHavok()->SetNumEnemies( 0 );
}

void EntityManager::ClearList()
{
	hkpWorld* p_World = p_Havok->GetPhysicsWorld();
	for(short i = 0; i < (short)EntityList.size(); ++i )
	{
		p_World->removeEntity(EntityList[i]->GetRigidBody());
		EntityList[i]->Shutdown();
	}
	Havok::GetHavok()->SetNumEnemies( 0 );
	EntityList.clear();
}

void EntityManager::ClearBullets()
{
	hkpWorld* p_World = p_Havok->GetPhysicsWorld();
	for(short i = 0; i < (short)Bullets.size(); ++i)
	{
		if(Bullets[i]->GetRigidBody()->getWorld() != HK_NULL)
			p_World->removeEntity(Bullets[i]->GetRigidBody());
		Bullets[i]->Shutdown();
	}

	Bullets.clear();
}

void EntityManager::CheckToUseStation()
{
	float dist = abs( p_Camera->pos().x - p_Platforms->GetCheckpoint().x );
	if (dist <= 5.0f) //They are within 5 distance from the checkpoint
	{
		//check if they press the XBUTTON_X
		if(p_Input->CheckXboxBufferedButton((BYTE)0, XBUTTON_X) ||
			p_Input->CheckXboxBufferedButton((BYTE)1, XBUTTON_X) ||
			p_Input->CheckKeyboardBufferedKey( DIK_V ) )
		{
			UserInterface::GetInstance()->SetState(WEAPON_SWITCH);
		}			
	}
}

Entity* EntityManager::GetPlayerUsingID(unsigned int playerRBodyID)
{
	for(short i = 0; i < MAX_NUM_PLAYERS; ++i)
	{
		unsigned int currentPlayerID = EntityList[i]->GetRigidBody()->getUid();
		if( currentPlayerID == playerRBodyID)
		{
			return EntityList[i];
		}
	}
	//should not reach here.
	return NULL;
}

Entity* EntityManager::GetEnemyUsingID(unsigned int collidingEnemyID)
{
	for(short j = MAX_NUM_PLAYERS; j < (short)EntityList.size(); ++j)//J should start after all the players
	{
		unsigned int thisEnemyID = EntityList[j]->GetRigidBody()->getUid();
		if(thisEnemyID == collidingEnemyID)
		{
			return EntityList[j];
		}
	}
	//should not reach here.
	return NULL;
}

Projectile* EntityManager::GetBulletUsingID(unsigned int collidingBulletID)
{
	unsigned int thisBulletID = -1;

	for(short b = 0; b < (short)Bullets.size(); ++b)
	{
		thisBulletID =	Bullets[b]->GetRigidBody()->getUid();
		if(thisBulletID == collidingBulletID)
		{
			return Bullets[b];
		}
	}
	//should not reach here.
	return NULL;
}

void EntityManager::Shutdown()
{
	if( instance )
	{
		ClearList();
		ClearBullets();
		p_Flamethrower->Shutdown();
		p_Acid->Shutdown();
		p_Freeze->Shutdown();
		BloodEffect::GetEffect()->Shutdown();

		delete instance;
	}

	instance = 0;
}

D3DXVECTOR3 EntityManager::GetPlayerOnePos()
{
	if( EntityList.size() > 0 )
		return EntityList[0]->pos;

	return D3DXVECTOR3( 0, 0, 0 );
}
