#include "Weapon.h"

void Weapon::Update( float dt )
{
	/*constantly update coolDownRemaining until it reaches (falls below) 0.
	So  the update needs to be checking for coolDownRemaining > 0.0f and doing nothing
	or, the update needs to be checking for coolDownRemaining <=0.0f and thats when you run the normal code
	The weapon now has clip = clipsize and can shoot again since there is no cooldown remaining.
	*/
	if( Clip < 1 )
		Reload();

	if(CoolDownRemaining > 0)
	{
		CoolDownRemaining -= dt;
	}
	if( false == CanShoot )
	{
		ROFTimer -= dt;

		if( ROFTimer < 0.0f && CoolDownRemaining <= 0)
			CanShoot = true; 
	}
}

void Weapon::Reload(void)
{
    CoolDownRemaining = CoolDown;	//triggers weapon::coolDown and sets coolDownRemaining to weapon::coolDown.
	Clip = Clipsize;
	CanShoot = false;
	/*
	More thoughts about reloading.
	If the weapon is a pistol, shotgun, or acidgun, then the weapon should only be cooling down and thus reloading if you are holding that weapon
	On the contrary, the flamegun, freezegun, and confusegun's very idea is that they have a long cooldown between uses
	So, it feels terrible if you MUST be holding these 3 weapons in order for them to cooldown. They should cooldown while you hold a pistol
	However, it is totally CHEAP if the player has no penalty when reloading a pistol, shotgun or acidgun and can simply swap weapons and when
	they come back, the weapon has been reloaded. Plus, thats not realistic that you reloaded your pistol/shotgun while you were shooting something else 
	the entire time.
	*/
}

void Weapon::Shoot()
{
	if(CoolDownRemaining > 0)
		return;//do nothing cause you are reloading
	if(ROFTimer > 0)
		return;//do nothing because you cant shoot faster than your rate of fire

	if(Clip > 0 && (true == CanShoot) )
	{
		ROFTimer = RateOfFire;
		CanShoot = false;

		if(GunType != PISTOL && GunType != FIREGUN && GunType != FREEZEGUN)
			Ammo--;	//and decreases total ammo count and clip(clip is the current bullets before a reload)
		Clip--;
	}
}