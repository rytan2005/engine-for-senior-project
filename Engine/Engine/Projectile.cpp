#include "Projectile.h"
#include "FreezeEffect.h"
#include "AcidEffect.h"

// Constructor
Projectile::Projectile(bool isPlayer, D3DXVECTOR3 bulletStart, Directions TheDir, ElementType bulletElement, short GunDamage, float GunRange)
{
	bulletStart.y += 0.5f; //make the starting point higher so it shoots from their arms' height
	Dir = TheDir;
	element = bulletElement;
	Damage = GunDamage;
	Range = GunRange;

	//make the graphical body
	GameComponent3D* temp = 0;
	DX9Render::GetRenderer()->LoadSphere(&temp, 0.025f, 2, 2);
	Graphic = temp;
	Graphic->pos = bulletStart;

	//make the hkpRigidBody
	if(TheDir == DIR_LEFT)
	{
		rBody = Havok::GetHavok()->AddBullet(isPlayer, bulletStart, D3DXVECTOR3(-1,0,0));
	}
	else //DIR_RIGHT
	{
		rBody = Havok::GetHavok()->AddBullet(isPlayer, bulletStart, D3DXVECTOR3(1,0,0));
	}
	DistTraveled = 0;	//check this in the manager. If its >= range then remove it
}

Projectile::~Projectile(void)
{}

void Projectile::Update()
{
	//update the graphical position to be what Havok tells it to be.
	Graphic->pos.x = rBody->getPosition()(0);//x
	Graphic->pos.y = rBody->getPosition()(1);//y (z not needed)

	DistTraveled += GameTimer::GetTimer()->GetDeltaTime();

	if( GetElement() == ElementType::FREEZE )
	{
		FreezeEffect::GetEffect()->SetOrigin( Graphic->pos );
		FreezeEffect::GetEffect()->AddParticles( 0, PARTICLE_DETAIL_LOW );
	}
	if( GetElement() == ElementType::POISON )
	{
		AcidEffect::GetEffect()->SetOrigin( Graphic->pos );
		AcidEffect::GetEffect()->AddParticles( 0, PARTICLE_DETAIL_LOW );
	}
}

void Projectile::Shutdown()
{
	//delete the rigidbody*
	if(rBody->getWorld() != HK_NULL)
	{
  		rBody->removeReference();//world delete rigid body instead?
		rBody = HK_NULL;
	}
	//shutdown the GameComponent3D
	Graphic->Shutdown();
}


short Projectile::GetDamage(void)
{ 
	return Damage; 
}
hkpRigidBody* Projectile::GetRigidBody(void)
{ 
	return rBody;
}
GameComponent3D* Projectile::GetGC3D(void)
{ 
	return Graphic;
}
float Projectile::GetDistTraveled(void)
{ 
	return DistTraveled;
}
float Projectile::GetRange(void)
{ 
	return Range;
}
ElementType Projectile::GetElement(void)
{
	return element;
}