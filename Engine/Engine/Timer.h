#pragma once

#include <Windows.h>
#pragma comment (lib, "winmm.lib")

class CTimer
{
private:
	static CTimer* timerObj;
	CTimer();
	CTimer(const CTimer& t){}
	void operator= (const CTimer& t){}

	short timer, prevTime, timeDiff;

	_int64 currTimeStamp, prevTimeStamp;
	float deltaTime, secsPerCnt;

	float fpsTimer;
	short fps, frameCounter;

public:
	static CTimer* GetTimer();

	void StartTimer();

	void Update();
	float GetDeltaTime();
	short GetElapsedMilliseconds();
	short GetFPS();

	void Shutdown();

	~CTimer(void){}
};

