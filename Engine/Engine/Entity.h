#pragma once

#include "Utilities.h"
#include "Havok.h"
#include "Weapon.h"
#include "Projectile.h"

//using namespace std;

enum MAX_ENTITY_HEALTH {PLAYER_HEALTH = 100,
						SMALL_SPIDER_HEALTH = 60,
						ROBOT_HEALTH = 80,
						GAURD_HEALTH = 100,
						COMMANDER_HEALTH = 500,
						JUGGERNAUT_HEALTH = 1000,
						BOSS_SPIDER_HEALTH = 2000,
						MECH_WARRIOR_HEALTH = 3000};

enum Status_Effect { NORMAL = 0, FROZEN, POISONED, BURNING};

class Entity : public GameComponent3DAnimated
{
public:
	Entity();
	short health;
	hkpRigidBody* rBody;
	unsigned short ID;
	EntityType entityType;
	Directions dir;
	Status_Effect Status;
	float StatusTimer;
	float PeriodicDmgTimer;
	//bool alive;

	bool canTakeDamage;
	
	void TakeDamage(short damage);
	void GainHealth(short amount);
	void SetHealth(short smount);
	short GetHealth();
	short GetMaxHealth();
	hkpRigidBody* GetRigidBody(void)
		{ return rBody; }

	void Update(float dt); //update the player's ammo count, cooldown remaining	
	void UpdateStatus(float dt);
	void SetStatus(Status_Effect newStatus, float duration);
	Status_Effect GetStatus()
		{ return Status; }
	void SetWeaponTo(GUNTYPE gt);
	void SwitchWeapon(void)		//swaps between the pistol and the large elemental gun
		{ IsPrimary = !IsPrimary; }
	Weapon* getActiveWeapon();
	GUNTYPE* getWeapons();
	Projectile* Shoot();		//Remove this after BETA. I plan to be able to shoot secondary with the Right Bumper on Controller without switching weapons.
	Projectile* ShootPrimary();	//checks if it can shoot, then spawns a Projectile* with correct stats and returns it (to be caught by entitymanager)
	Projectile* ShootSecondary();
	short GetGunDamage();
	float GetGunRange();
	float GetGunCoolDownRemaining();
	bool CanShoot();

private:
	Weapon Primary;
	Weapon Secondary;		 
	bool IsPrimary;			 //when true, player is using their primary large elemental weapon. when false, player is using their secondary pistol
};