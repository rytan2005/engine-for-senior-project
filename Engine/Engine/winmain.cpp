/*
	Marshall Johnson
	winmain for GSP420 engine
	9/17/2012
*/

#define VC_EXTRALEAN
#define WIN32_LEAN_AND_MEAN

#include <iostream>
#include <windows.h>

//#include <vld.h>

#include "GameObject.h"
#include "LuaObject.h"

char* WndClassName = "GSP 420 Engine";
HWND hwnd = NULL;
const short Width = 1366;
const short Height = 768;

CGameObject g_Game;


bool InitializeWindow( HINSTANCE hInstance, int showWnd, int width, int height, bool windowed );
int messageloop();

LRESULT CALLBACK WndProc( HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam );

int WINAPI WinMain( HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nShowCmd )
{
#if defined(DEBUG) | defined (_DEBUG)
	_CrtSetDbgFlag( _CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF );
#endif

	if( !InitializeWindow( hInstance, nShowCmd, Width, Height, true ) )
	{
		MessageBox( 0, "Window Initialization Failed", "Error", MB_OK );
		return 0;
	}

	if( !g_Game.Init( hwnd, hInstance ) )
	{
		MessageBox( 0, "Game Initialization Failed", "Error", MB_OK );
		return 0;
	}

	messageloop();

	g_Game.Shutdown();

	return 0;
}

bool InitializeWindow( HINSTANCE hInstance, int showWnd, int width, int height, bool windowed )
{
	WNDCLASSEX wc;
	wc.cbSize			= sizeof( WNDCLASSEX );
	wc.style			= CS_HREDRAW | CS_VREDRAW;
	wc.lpfnWndProc		= WndProc;
	wc.cbClsExtra		= NULL;
	wc.cbWndExtra		= NULL;
	wc.hInstance		= hInstance;
	wc.hIcon			= LoadIcon( NULL, IDI_APPLICATION );
	wc.hCursor			= LoadCursor( NULL, IDC_ARROW );
	wc.hbrBackground	= (HBRUSH)( COLOR_WINDOW + 2 );
	wc.lpszMenuName		= NULL;
	wc.lpszClassName	= WndClassName;
	wc.hIconSm			= LoadIcon( NULL, IDI_APPLICATION );

	if( !RegisterClassEx( &wc ) )
	{
		MessageBox( NULL, "Error Registering Class", "Error", MB_OK | MB_ICONERROR );
		return false;
	}

	hwnd = CreateWindowEx( NULL, WndClassName, WndClassName, WS_OVERLAPPEDWINDOW,
		0, 0, width, height, NULL, NULL, hInstance, NULL );

	if( !hwnd )
	{
		MessageBox( NULL, "Error Creating Window", "Error", MB_OK | MB_ICONERROR );
		return false;
	}

	ShowWindow( hwnd, showWnd );
	UpdateWindow( hwnd );

	return true;
}

int messageloop()
{
	MSG msg;

	//ZeroMemory( &msg, sizeof( MSG ) );

	while( true )
	{
		if( PeekMessage( &msg, NULL, 0, 0, PM_REMOVE ) )
		{
			if( msg.message == WM_QUIT )
				break;
			TranslateMessage( &msg );
			DispatchMessage( &msg );
		}

		g_Game.Update();
		g_Game.Render();
	}

	return msg.wParam;
}

LRESULT CALLBACK WndProc( HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam )
{
	switch( msg )
	{
	case WM_DESTROY:
		PostQuitMessage( 0 );
		return 0;
	}

	return DefWindowProc( hwnd, msg, wParam, lParam );
}
// Keycode
#include <Common/Base/keycode.cxx>

// Productfeatures
// We're using only physics - we undef products even if the keycode is present so
// that we don't get the usual initialization for these products.
#undef HK_FEATURE_PRODUCT_AI
#undef HK_FEATURE_PRODUCT_ANIMATION
#undef HK_FEATURE_PRODUCT_CLOTH
#undef HK_FEATURE_PRODUCT_DESTRUCTION
#undef HK_FEATURE_PRODUCT_BEHAVIOR
#undef HK_FEATURE_PRODUCT_MILSIM
#undef HK_FEATURE_PRODUCT_NEW_PHYSICS

#define HK_EXCLUDE_FEATURE_SerializeDeprecatedPre700
#define HK_EXCLUDE_FEATURE_RegisterVersionPatches
// Vdb needs the reflected classes
//#define HK_EXCLUDE_FEATURE_RegisterReflectedClasses
#define HK_EXCLUDE_FEATURE_MemoryTracker

#include <Common/Base/Config/hkProductFeatures.cxx>