#pragma once

#include <d3d9.h>
#include <d3dx9.h>
#pragma comment (lib, "d3d9.lib")
#pragma comment (lib, "d3dx9.lib")
#include <vector>
#include <string>
using namespace std;

#include "Renderer.h"
#include "Skybox.h"

#include "DX9Structs.h"

class DX9Render : public Render
{
private:
	int NumRenderCalls;

	static DX9Render* renderer;
	DX9Render(){}
	DX9Render(const DX9Render& t){}
	void operator= (const DX9Render& t){}


	IDirect3D9*				m_pD3DObject;
	IDirect3DDevice9*		m_pD3DDevice;
	D3DCAPS9				m_D3DCaps;
	D3DPRESENT_PARAMETERS	D3Dpp;
	HWND*					p_hWnd;
	bool					Initialized;

	ID3DXEffect*			m_pDefaultEffect;
	ID3DXEffect*			m_pAnimEffect;

	ID3DXMesh*				m_pCubeMesh;
	float					m_CubeBoundingSphere;

	ID3DXEffect*			m_pSkyboxEffect;
	ID3DXMesh*				m_pSkyboxMesh;
	Skybox*					m_pSkybox;

	ID3DXSprite*			m_pSprite;
	ID3DXFont*				m_pFont;

	D3DXMATRIX worldMat, transMat, scaleMat, rotMat;

	Camera* camera;

	AnimationList* AnimList;
	void ClearAnimationList( AnimationList* animList );

	bool DeviceLost();
	void OnLostDevice();
	void OnResetDevice();
	friend class ParticleEmitter;

public:
	~DX9Render(void){}
	static DX9Render* GetRenderer();

	IDirect3DDevice9* GetDevice(){return m_pD3DDevice;}
	Camera* GetActiveCamera(){return camera;}

	bool Init( HWND& hwnd );
	void SetCamera( Camera* cam );

	bool LoadSkybox( Skybox** skybox, char* filename );
	void LoadSphere( GameComponent3D** object, float radius, UINT slices, UINT stacks );
	void LoadCylinder( GameComponent3D** object, float radius1, float radius2, float length, UINT slices, UINT stacks );
	void LoadUntexturedCube( GameComponent3D** object, float width, float height, float depth );
	bool LoadTexturedCube( GameComponent3D** object, float width, float height, float depth, char* texfile, float TexUScale = 1.0f, float TexVScale = 1.0f );

	bool Load3DGameComponent( GameComponent3D** object, char* filename );	
	bool Load3DGameComponent( GameComponent3DAnimated** object, char* filename );
	bool Load2DGameComponent( GameComponent2D** object, char* filename );
	
	void SetSkybox( Skybox* skybox );

	bool BeginRender();
	void Begin2DRender();

	void RenderPrimitive( GameComponent3D** object, bool wireframe );

	void Render3DComponent( GameComponent3D** object, Light* light = 0, bool lit = false );
	void Render3DComponent( GameComponent3DAnimated** object, Light* light = 0, bool lit = false );
	void Render2DComponent( GameComponent2D** object );
	void RenderText( char* message, float xPos, float yPos );

	void End2DRender();
	void EndRender();

	void SetFullscreen( bool enable );
	void ToggleFullscreen();

	void Shutdown();
};

