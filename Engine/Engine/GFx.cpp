#include "GFx.h"


GFxClass::GFxClass(void)
{
}


GFxClass::~GFxClass(void)
{
}

bool GFxClass::InitGFx()
{
	// Initialize logging -- GFx will print errors to the log
	// stream. GFx log messages are very helpful when
	// debugging. To generate a simple error, comment out
	// gfxLoader.SetFileOpener(pfileOpener).
	gfxLoader.SetLog(Ptr<SF::Log>(*new GFxPlayerLog()));

	// Give the loader the default file opener
	Ptr<FileOpener> pfileOpener = *new FileOpener;
	gfxLoader.SetFileOpener(pfileOpener);

	// Create the render
	pRenderHAL = *new Render::D3D9::HAL();
	if (!(pRenderer = *new Render::Renderer2D(pRenderHAL.GetPtr())))
		return false;


	Ptr<ASSupport> pAS2Support = *new GFx::AS2Support();
	gfxLoader.SetAS2Support(pAS2Support);

	// Use EdgeAA to improve the appearance of the interface without the computational
	// expense of full AA through the video card.
	//pRenderConfig->SetRenderFlags(RenderConfig::RF_EdgeAA);

	// Load the movie and crate instance.
	pUIMovieDef = *(gfxLoader.CreateMovie(UIMOVIE_FILENAME, Loader::LoadWaitFrame1));			// Loader::LoadKeepBindData | Loader::LoadWaitFrame1 - keeps textures
	if(!pUIMovieDef)
		return false;

	// We pass 'true' argument to make sure that first frame is initialized.
	// We can use 'false' if we need to pass some information to the movie
	// before its first frame 1 ActionScript logic is executed.
	// If false is passed....
	// pUIMovie->Advance(0.0f, 0, true); // must be performed
	// Maintains state associated with a single running instance of a movie
	//  such as the current frame, time in the movie, states of buttons, and
	//  ActionScript variables.
	pUIMovie = *pUIMovieDef->CreateInstance(true, 0);
	if(!pUIMovie)
		return false;

	//For GFx 3.3 and above, the controller and mouse count must be set
	//otherwise GFx will assume that neither are present
	pUIMovie->SetMouseCursorCount(1);
	pUIMovie->SetControllerCount(1);

	// get the display handle
	hMovieDisplay = pUIMovie->GetDisplayHandle();

	// Advance the movie to the first frame
	pUIMovie->Advance(0.0f, 0);					// not necessary but done for reassurance.

	// Note the time to determine the amount of time elapsed between this frame and the next
	MovieLastTime = timeGetTime();

	// Set the background stage color to alpha blend with the underlying 3D environment
	pUIMovie->SetBackgroundAlpha(0.0f);

	return true;
}

void GFxClass::OnCreateDevice(IDirect3DDevice9* pd3dDevice)
{

}

void GFxClass::OnResetDevice(IDirect3DDevice9* pd3dDevice)
{
	HWND hWND = DXUTGetHWND();
	pRenderHAL->SetDependentVideoMode(pd3dDevice, &presentParams, 0, Render::D3D9::HAL::VMConfig_NoSceneCalls, hWND);

	// Use the window client rect size as the viewport.
	D3DVIEWPORT9 vp;
	if(pd3dDevice->GetViewport(&vp) == D3D_OK)
		pUIMovie->SetViewport(vp.Width, vp.Height, 0, 0, vp.Width, vp.Height);
}

void GFxClass::OnLostDevice()
{

}

void GFxClass::OnDestroyDevice()
{

}

void GFxClass::StorePresentParameters(D3DPRESENT_PARAMETERS p)
{

}

void GFxClass::AdvanceAndRender()
{

}

// Helper function that converts Windows VK keyCode values
// to GFxKeyEvents and routes them to GFxPlayer.
void GFxClass::HandleKeyEvent(unsigned keyCode, bool downFlag)
{
	Key::Code key(Key::None);

	if (keyCode >= 'A' && keyCode <= 'Z')
	{
		key = (Key::Code) ((keyCode - 'A') + Key::A);
	}
	else if (keyCode >= VK_F1 && keyCode <= VK_F15)
	{
		key = (Key::Code) ((keyCode - VK_F1) + Key::F1);
	}
	else if (keyCode >= '0' && keyCode <= '9')
	{
		key = (Key::Code) ((keyCode - '0') + 48);
	}
	else if (keyCode >= VK_NUMPAD0 && keyCode <= VK_NUMPAD9)
	{
		key = (Key::Code) ((keyCode - VK_NUMPAD0) + Key::KP_0);
	}
	else
	{
		// Use a look-up table for keys don't correlate in order,.
		static struct {
			int          vk;
			Key::Code gs;
		} table[] =
		{
			{ VK_SHIFT,     Key::Shift },
			{ VK_RSHIFT,    Key::Shift },
			{ VK_CONTROL,   Key::Control },
			{ VK_RCONTROL,  Key::Control },
			{ VK_MENU,      Key::Alt },
			{ VK_RMENU,     Key::Alt },
			{ VK_RETURN,    Key::Return },
			{ VK_ESCAPE,    Key::Escape },
			{ VK_LEFT,      Key::Left },
			{ VK_UP,        Key::Up },
			{ VK_RIGHT,     Key::Right },
			{ VK_DOWN,      Key::Down },
			{ VK_SPACE,     Key::Space },
			{ VK_BACK,      Key::Backspace },
			{ VK_DELETE,    Key::Delete },
			{ VK_INSERT,    Key::Insert },

			// TODO: fill this out some more
			{ 0, Key::None }
		};

		for (int i = 0; table[i].vk != 0; i++)
		{
			if (keyCode == (unsigned)table[i].vk)
			{
				key = table[i].gs;
				break;
			}
		}
	}

	if (key != Key::None)
	{
		if (pMovie)
		{
			// Pass Key events to the movie so that can be handled in ActionScript.
			KeyEvent event(downFlag ? GFx::Event::KeyDown : KeyEvent::KeyUp, key);
			pMovie->HandleEvent(event);
		}
	}
}