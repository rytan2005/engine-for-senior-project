#pragma once
#include "DX9Renderer.h"
#include "Havok.h"
#include "Actor.h"
#include "MovingActor.h"
#include <vector>

class Trigger;
class MovingActor;

class PlatformManager
{
private:
	static PlatformManager* p_Instance;
	PlatformManager(void);
	PlatformManager( const PlatformManager& I ){};

	std::vector<Actor*> platforms;
	std::vector<Trigger*> triggers;
	std::vector<MovingActor*> movingActors;

	Render* p_Renderer;
	Light* light;

	D3DXVECTOR3 checkpoint;
	D3DXVECTOR3 endpoint;

public:
	static PlatformManager* GetManager();
	void AddPlatform( float width, float height, 
		float depth, D3DXVECTOR3 pos, 
		char* texfile, float texU, float texV );
	
	void AddDoorTrigger( D3DXVECTOR3 posTrigger, 
		float scaleTrigger, D3DXVECTOR3 posDoor, D3DXVECTOR3 scaleDoor );

	void setCheckpoint(D3DXVECTOR3 checkpoint){ this->checkpoint = checkpoint;}
	D3DXVECTOR3 GetCheckpoint(){return checkpoint;}
	
	void setEndpoint(D3DXVECTOR3 endpoint){ this->endpoint = endpoint;}
	D3DXVECTOR3 GetEndpoint(){return endpoint;}

	void SetActiveLight( Light* pLight );
	void Update();
	void Render();
	void Shutdown();

	~PlatformManager(void);
};

