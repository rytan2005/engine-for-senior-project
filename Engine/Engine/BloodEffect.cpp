#include "BloodEffect.h"


BloodEffect* BloodEffect::instance;
BloodEffect* BloodEffect::GetEffect()
{
	if( !instance )
	{
		instance = new BloodEffect();
		instance->filename = "assets/shaders/flamethrower.hlsl";
		instance->techname = "tech1";
		instance->texname = "assets/textures/particles/blood1.png";
	}
	return instance;
}

void BloodEffect::InitParticle( Particle& out )
{
	out.initialPos = origin;
	out.initialPos.x += direction / 2.0f;

	out.initialTime = mTime;
	out.lifeTime = GetRandomFloat( 0.1f, 2.0f );
	out.initialColor = D3DXCOLOR( 1.0f, 1.0f, 1.0f, 1.0f );
	out.initialSize = GetRandomFloat( 0.5f, 2.0f );
	out.mass = GetRandomFloat( 0.8f, 1.2f );

	if( direction < 0.01f && direction > -0.01f )
	{
		out.initialVelocity.x = GetRandomFloat( -3.0f, 3.0f );
		out.initialVelocity.y = GetRandomFloat( 0.0f, 3.0f );
		out.initialVelocity.z = GetRandomFloat( -3.0f, 3.0f );
	}else
	{
		out.initialVelocity.x = direction * GetRandomFloat( 1.0f, 3.0f );
		out.initialVelocity.y = GetRandomFloat( 0.5f, 2.0f );
		out.initialVelocity.z = GetRandomFloat( -2.0f, 2.0f );
	}
}

void BloodEffect::AddParticles( short direction, 
		PARTICLE_DETAIL_LEVEL detailLevel )
{
	this->direction = direction;

	int numParticlesToInit = 0;
	switch( detailLevel )
	{
	case PARTICLE_DETAIL_LOW:
		numParticlesToInit = 10;
		break;

	case PARTICLE_DETAIL_MEDIUM:
		numParticlesToInit = 30;
		break;

	case PARTICLE_DETAIL_HIGH:
		numParticlesToInit = 50;
		break;

	default:
		numParticlesToInit = 5;
		break;
	};

	for( short i = 0; i < numParticlesToInit; ++i )
	{
		AddParticle();
	}
}