#pragma once

#include "Actor.h"
#include "Trigger.h"

class PlatformManager;
class Trigger;

class MovingActor : public Actor
{
private:
	Trigger* trigger;
	D3DXVECTOR3 StartPos;
	D3DXVECTOR3 Destination;
	float OpenSpeed;

public:
	MovingActor(char* filename, D3DXVECTOR3 position, Trigger* trigger,
		float TexU, float TexV,
		D3DXVECTOR3 Scale = D3DXVECTOR3(1,1,1),
		D3DXVECTOR3 rotation = D3DXVECTOR3(0,0,0));
	void Update();
};

