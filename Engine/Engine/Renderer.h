#pragma once

#define VC_EXTRALEAN
#define WIN32_LEAN_AND_MEAN
#include <windows.h>

class Skybox;
struct GameComponent3D;
struct GameComponent3DAnimated;
struct GameComponent2D;
class Camera;
struct Light;

class Render
{
public:
	Render(void){}
	~Render(void){}

	///	Init
	/*
		Initialized the Rendering Device for use with both 2D, and 3D
			Rendering, on parameter is the HWND from the winmain
	*/
	virtual bool Init( HWND& hwnd ) = 0;

	/// SetCamera
	/*
		Sets the camera to the camera that is specified by the program.
			The only parameter for this function is a pointer to the camera
			to be used.
	*/
	virtual void SetCamera( Camera* cam ) = 0;

	/// LoadSkybox
	/*
		This function loads a skybox and stores the required data for the 
			skybox into the given skybox object. The only 2 parameters to this
			function are a double pointer to the skybox object, and the file
			name of the texture to be used as a skybox.
	*/
	virtual bool LoadSkybox( Skybox** skybox, char* filename ) = 0;

	/// LoadSphere
	/*
		Loads a sphere into the specified 3d game component object. The parameters
			to this function are a double pointer to the game component that will
			hold the mesh, and the radius for the sphere, as well as the stacks
			and slices for the sphere.
	*/
	virtual void LoadSphere( GameComponent3D** object, float radius, UINT slices, UINT stacks ) = 0;

	/// LoadCylinder
	/*
		Loads a cylinder into the specified 3d game component object. The parameters
			to this function are the game component, 2 float radii, the length of
			the cylinder, and the number of slices and stacks that go into the cylinder.
	*/
	virtual void LoadCylinder( GameComponent3D** object, float radius1, float radius2, float length, UINT slices, UINT stacks ) = 0;

	/// LoadUntexturedCube
	/*
		Loads a cube into the specified 3d game component object without UV coords.
			The parameters to this function are a double pointer to the game component 
			objects, the width, height, and depth of the cube.
	*/
	virtual void LoadUntexturedCube( GameComponent3D** object, float width, float height, float depth ) = 0;

	/// LoadTexturedCube
	/*
		Loads a cube into the specified 3D game component object with UV coords.
			The parameters to this function are the double pointer to the game component,
			the width, height, and depth of the cube, and the texture filename
			of the texture to be used.
	*/
	virtual bool LoadTexturedCube( GameComponent3D** object, float width, float height, float depth, char* texfile, float TexUScale = 1.0f, float TexVScale = 1.0f ) = 0;

	/// Load3DGameComponent
	/*
		Loads a 3D game component from a file, and readies it for drawing
			with the rendering device. parameters are: a double pointer
			to the type of object created, and the file name of the 3d
			asset.
	*/
	virtual bool Load3DGameComponent( GameComponent3D** object, char* filename ) = 0;
	
	/// Load3DGameComponent
	/*
		THIS IS AN OVERLOADED METHOD OF THE LOAD FUCNTION, AND THIS ONE TAKES IN
			AND LOADS AN ANIMATED MODEL.
		Loads an animated 3D game component from a specified file and
			readies it for rendering. the parameters are the 3d game component,
			the file for the mesh, and the file for the texture of the mesh
	*/
	virtual bool Load3DGameComponent( GameComponent3DAnimated** object, char* filename ) = 0;

	/// Load2DGameComponet
	/*
		Loads a 2D game component, from a file, and readies it for renderiong.
			Only parameter is a double pointer to the object, and the 
			file name of the 3D asset.
	*/
	virtual bool Load2DGameComponent( GameComponent2D** object, char* filename ) = 0;

	/// BeginRender
	/*
		Begins the rendering process, must be called before rendering
			any 3D or 2D objects.
	*/
	virtual bool BeginRender() = 0;

	/// SetSkybox
	/*
		This function sets the skybox to be used when rendering. Thi only
			parameter to this function is a pointer to the skybox to be used.
	*/
	virtual void SetSkybox( Skybox* skybox ) = 0;

	/// Begin2DRender
	/*
		Begins the rendering process for 2D rendering, must be called
			before rendering any 2D objects.
	*/
	virtual void Begin2DRender() = 0;

	/// RenderPrimitive
	/*
		Performs the render of a specified primitive object.
			the only parameter to this function is a double pointer to the object
			that contains the primitive, this function does not take into account
			texture info, or any materials.
	*/
	virtual void RenderPrimitive( GameComponent3D** object, bool wireframe ) = 0;
	
	/// Render3DComponent
	/*
		Performs the Render of a 3D Component.
			parameters to this function, the 3D component
			that you wish to have rendered, and the camera.
	*/
	virtual void Render3DComponent( GameComponent3D** object, Light* light = 0, bool Lit = false ) = 0;

	/// Render3DComponent
	/*
		THIS IS AN OVERLOADED METHOD OF THE RENDER FUNCTION FOR THE 3D OBJECTS.
			THIS METHOD RENDERS THE 3D ANIMATED MODELS.
		Performs the render of the animated 3d component. the only parameter
			to this function is a double pointer to the game component to be 
			rendered.
	*/
	virtual void Render3DComponent( GameComponent3DAnimated** object, Light* light = 0, bool Lit = false ) = 0;

	/// Render2DComponent
	/*
		Performs the render of a 2D component
			The only parameter to this function, is the 2D component
			that you wish to have rendered.
	*/
	virtual void Render2DComponent( GameComponent2D** object ) = 0;

	/// Render Text
	/*
		Renders Text to the screen.
			the only parameters to this function are the message to
			be rendered, and the x and y position of the message in
			screen coordinates
		*/
	virtual void RenderText( char* message, float xPos, float yPos ) = 0;

	/// End2DRender
	/*
		Ends the 2D rendering process, this function is called when
			all the 2D components have been rendered to finalize
			the 2D rendering, so they can be present to the screen.
	*/
	virtual void End2DRender() = 0;

	/// EndRender
	/*
		This is called after all 3D and 2D rendering is finished,
			this function finalizes the rendering process, and 
			presents, the scene to the screen.
	*/
	virtual void EndRender() = 0;

	/// SetFullScreen
	/*
		This function sets the screen to either full screen, or windowd
			mode. The only parameter to this function is a boolean that
			determines whether full screen should be enabled or not.
	*/
	virtual void SetFullscreen( bool enable ) = 0;

	/// ToggleFullscreen
	/*
		This function toggles between fullscreen, and windowed mode,
			without the parameter required by the SetFullscreen() function.
	*/
	virtual void ToggleFullscreen() = 0;

	/// Shutdown
	/*
		Shuts down the rendering object, and cleans up all of the
			allocated memory used by this object.
	*/
	virtual void Shutdown() = 0;
};

