#pragma once

#ifndef H_ENTITYMANAGER
#define H_ENTITYMANAGER

#include <vector>
#include "Entity.h"
#include "Player.h"
#include "GameTimer.h"
#include "Utilities.h"
#include "DX9Renderer.h"
#include "InputManager.h"
#include "Projectile.h"
#include "Enemy.h"
#include "AIHandler.h"
#include "PlatformManager.h"

#include "FlamethrowerEffect.h"
#include "AcidEffect.h"
#include "FreezeEffect.h"

CONST BYTE MAX_NUM_PLAYERS = 2;
CONST BYTE MAX_NUM_ENEMIES = 15;

class EntityManager
{
public:

	vector<Entity*> EntityList;
	Light* light;
	bool checkpointReached;

	bool Init();
	void SetActiveCamera( Camera* cam );
	void AddEntity( char* fileName, D3DXVECTOR3 pos, D3DXVECTOR3 rot,
		float scale = 1, D3DXVECTOR3 offset = D3DXVECTOR3(0,0,0), float patrolRadius = 1.0f );
	void AddEntity(EntityType type, D3DXVECTOR3 pos);
	void UpdateEntities();
	void UpdateBullets();
	void RenderEntities();
	void ClearDeadEntities();
	void ClearNonPlayers();
	void ClearList();
	void ClearBullets();
	void CheckToUseStation();
	Entity* GetPlayerUsingID(unsigned int playerRBodyID);
	Entity* GetEnemyUsingID(unsigned int collidingEnemyID);
	Projectile* GetBulletUsingID(unsigned int collidingBulletID);
	Camera* GetActiveCamera(){return p_Camera;}
	static EntityManager* GetEntityManager();
	PlatformManager* p_Platforms;
	void Shutdown();

	D3DXVECTOR3 GetPlayerOnePos();

private:
	
	Render* p_Renderer;
	static EntityManager* instance;

	AIHandler* aiHandler;

	FlamethrowerEffect* p_Flamethrower;
	AcidEffect* p_Acid;
	FreezeEffect* p_Freeze;

	vector<Projectile*> Bullets;

	GameTimer* p_Timer;
	InputManager* p_Input;
	Havok* p_Havok;
	Camera* p_Camera;

	EntityManager(){}
	EntityManager(const EntityManager &r){}

	friend class Trigger;
};

#endif