#include "ParticleEffect.h"


IDirect3DVertexDeclaration9* Particle::decl = 0;

bool ParticleEmitter::Init( const D3DXVECTOR3& accel,
	int maxNumParticles, float timePerParticle )
{
	origin = D3DXVECTOR3( 0, 0, 0 );
	direction = 1.0f;
	Active = false;
	timeAccum = 0.0f;
	mTime = 0.0f;
	mAccel = accel;

	mTimePerParticle = timePerParticle;
	mMaxNumParticles = maxNumParticles;
	mParticles.resize( mMaxNumParticles );
	mAliveParticles.reserve( mMaxNumParticles );
	mDeadParticles.reserve( mMaxNumParticles );

	for( int i = 0; i < mMaxNumParticles; ++i )
	{
		mParticles[i].lifeTime = 0.0f;
		mParticles[i].initialTime = 0.0f;
	}

	D3DXMatrixIdentity( &mWorld );
	D3DXMatrixIdentity( &mInvWorld );

	IDirect3DDevice9* gd3dDevice = DX9Render::GetRenderer()->m_pD3DDevice;
	D3DXCreateTextureFromFile( gd3dDevice, texname.c_str(), &mTex );

	ID3DXBuffer* errors = 0;
	D3DXCreateEffectFromFile( gd3dDevice, filename.c_str(), 0, 0, D3DXSHADER_DEBUG, 0, &mFX, &errors );
	if( errors )
	{
		MessageBox( 0, (char*)errors->GetBufferPointer(), 0, 0 );
		return false;
	}

	mhTech    = mFX->GetTechniqueByName(techname.c_str());
	mhWVP     = mFX->GetParameterByName(0, "gWVP");
	mhEyePosL = mFX->GetParameterByName(0, "gEyePosL");
	mhTex     = mFX->GetParameterByName(0, "gTex");
	mhTime    = mFX->GetParameterByName(0, "gTime");
	mhAccel   = mFX->GetParameterByName(0, "gAccel");
	mhViewportHeight = mFX->GetParameterByName(0, "gViewportHeight");

	// We don't need to set these every frame since they do not change.
	(mFX->SetTechnique(mhTech));
	(mFX->SetValue(mhAccel, mAccel, sizeof(D3DXVECTOR3)));
	(mFX->SetTexture(mhTex, mTex));

	(gd3dDevice->CreateVertexBuffer(mMaxNumParticles*sizeof(Particle),
		D3DUSAGE_DYNAMIC|D3DUSAGE_WRITEONLY|D3DUSAGE_POINTS,
		0, D3DPOOL_DEFAULT, &mVB, 0));

	if( !Particle::decl )
	{
		D3DVERTEXELEMENT9 elements[] = 
		{
			{0, 0,  D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_POSITION, 0},
			{0, 12, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 0},
			{0, 24, D3DDECLTYPE_FLOAT1, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 1},
			{0, 28, D3DDECLTYPE_FLOAT1, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 2},
			{0, 32, D3DDECLTYPE_FLOAT1, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 3},
			{0, 36, D3DDECLTYPE_FLOAT1, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 4},
			{0, 40, D3DDECLTYPE_D3DCOLOR, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_COLOR, 0},
			D3DDECL_END()
		};
		gd3dDevice->CreateVertexDeclaration( elements, &Particle::decl );
	}
	return true;
}

void ParticleEmitter::Shutdown()
{
	SAFE_RELEASE( mFX );
	SAFE_RELEASE( mTex );
	SAFE_RELEASE( mVB );
}

float ParticleEmitter::GetTime()
{
	return mTime;
}
void ParticleEmitter::SetTime(float dt)
{
	mTime = dt;
}
const AABB& ParticleEmitter::GetAABB() const
{
	return mBox;
}

void ParticleEmitter::SetWorldMatrix( const D3DXMATRIX& world )
{
	mWorld = world;
	D3DXMatrixInverse( &mInvWorld, 0, &mWorld );
}

void ParticleEmitter::AddParticle()
{
	if( mDeadParticles.size() > 0 )
	{
		Particle* p = mDeadParticles.back();
		InitParticle( *p );

		mDeadParticles.pop_back();
		mAliveParticles.push_back( p );
	}
}

void ParticleEmitter::OnLostDevice()
{
	if( mFX )
	{
		mFX->OnLostDevice();
		SAFE_RELEASE( mVB );
	}
}
void ParticleEmitter::OnResetDevice()
{
	if( mFX )
	{
		mFX->OnResetDevice();
		if( mVB == 0 )
		{
			IDirect3DDevice9* device = DX9Render::GetRenderer()->GetDevice();
			device->CreateVertexBuffer( mMaxNumParticles*sizeof(Particle),
				D3DUSAGE_DYNAMIC|D3DUSAGE_WRITEONLY|D3DUSAGE_POINTS,
				0, D3DPOOL_DEFAULT, &mVB, 0 );
		}
	}
}

void ParticleEmitter::Update( float dt )
{
	mTime += dt;
	mDeadParticles.resize( 0 );
	mAliveParticles.resize( 0 );

	for( int i = 0; i < mMaxNumParticles; ++i )
	{
		if( (mTime - mParticles[i].initialTime) > mParticles[i].lifeTime )
		{
			mDeadParticles.push_back(&mParticles[i]);
		}else
		{
			mAliveParticles.push_back(&mParticles[i]);
		}
	}

	if( !Active )
		return;

	if( mTimePerParticle > 0.0f )
	{
		timeAccum += dt;
		while( timeAccum >= mTimePerParticle )
		{
			AddParticle();
			timeAccum -= mTimePerParticle;
		}
	}
}

void ParticleEmitter::Render()
{
	Camera* cam = DX9Render::GetRenderer()->GetActiveCamera();
	D3DXVECTOR3 eyePosW = cam->pos();
	D3DXVECTOR3 eyePosL;
	D3DXVec3TransformCoord( &eyePosL, &eyePosW, &mInvWorld );

	mFX->SetValue( mhEyePosL, &eyePosL, sizeof(D3DXVECTOR3));
	mFX->SetFloat( mhTime, mTime );
	mFX->SetMatrix( mhWVP, &(mWorld * cam->viewProj() ) );

	HWND hwnd = *DX9Render::GetRenderer()->p_hWnd;
	RECT clientRect;
	GetClientRect( hwnd, &clientRect );
	mFX->SetInt( mhViewportHeight, clientRect.bottom );

	UINT numPasses = 0;
	mFX->Begin( &numPasses, 0 );
	mFX->BeginPass( 0 );

	DX9Render::GetRenderer()->m_pD3DDevice->SetStreamSource( 0, mVB, 0, sizeof( Particle ) );
	DX9Render::GetRenderer()->m_pD3DDevice->SetVertexDeclaration( Particle::decl );

	Particle* p = 0;
	mVB->Lock( 0, 0, (void**)&p, D3DLOCK_DISCARD );
	int vbIndex = 0;

	for( UINT i = 0; i < mAliveParticles.size(); ++i )
	{
		p[vbIndex] = *mAliveParticles[i];
		++vbIndex;
	}
	mVB->Unlock();

	if( vbIndex > 0 )
	{
		mFX->CommitChanges();
		IDirect3DDevice9* device = DX9Render::GetRenderer()->GetDevice();
		device->DrawPrimitive( D3DPT_POINTLIST, 0, vbIndex );
	}

	mFX->EndPass();
	mFX->End();
}

void ParticleEmitter::EnableEffect( bool enable )
{
	Active = enable;
}

bool ParticleEmitter::IsActive()
{
	return Active;
}

void ParticleEmitter::SetOrigin( D3DXVECTOR3& pos )
{
	origin = pos;
}

void ParticleEmitter::SetDirection( float dir )
{
	direction = dir;
}

void ParticleEmitter::AddParticles( short direction, 
		PARTICLE_DETAIL_LEVEL detailLevel )
{
	this->direction = direction;

	int numParticlesToInit = 0;
	switch( detailLevel )
	{
	case PARTICLE_DETAIL_LOW:
		numParticlesToInit = 10;
		break;

	case PARTICLE_DETAIL_MEDIUM:
		numParticlesToInit = 25;
		break;

	case PARTICLE_DETAIL_HIGH:
		numParticlesToInit = 40;
		break;

	default:
		numParticlesToInit = 5;
		break;
	};

	for( short i = 0; i < numParticlesToInit; ++i )
	{
		AddParticle();
	}
}